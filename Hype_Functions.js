var trackList = {};
var activeList = document.location.href;
var currentTrack = 0;
var currentPlayerObj = Array();
var activeItem;
var currentUrl;
var prevUrl;
var is_logged_in;
var logged_in_username;
var playback_allowed;
var user_country;
var user_region;
var static_http_server;
var initial_ts;
var initial_ref = '';
var dragging_position = false;
var dragging_x;
var isReady = 0;
var playerStatus = '';
var playerDisplayed = 'normal';
var playback_event_timeout = 0;
var playback_event_count = 0;
var playback_manual = 0;
var player_position;
var player_duration;
var player_volume = 50;
var page_updater;
var skip_update_page_contents = 0;
var notificationTimeout = 0;
var updateSpy = 1;
var album_rs = Array();
var album_r_curr = Array();
var master_ord;
var master_passback;
var ad_feedback_code;
var ad_feedback_position;
var ua_info = {};
var first_load = 1;
(function ($) {
    window.get_cookie = function (name) {
        var start = document.cookie.indexOf(name + "=");
        var len = start + name.length + 1;
        if ((!start) && (name != document.cookie.substring(0, name.length))) {
            return null;
        }
        if (start == -1) {
            return null;
        }
        var end = document.cookie.indexOf(";", len);
        if (end == -1) {
            end = document.cookie.length;
        }
        return unescape(document.cookie.substring(len, end));
    }
    window.set_cookie = function (name, value, expires, path, domain, secure) {
        var today = new Date();
        today.setTime(today.getTime());
        if (expires) {
            expires = expires * 1000 * 60 * 60 * 24;
        }
        var expires_date = new Date(today.getTime() + (expires));
        document.cookie = name + "=" + escape(value) + ((expires) ? ";expires=" + expires_date.toGMTString() : '') + ((path) ? ";path=" + path : '') + ((domain) ? ";domain=" + domain : '') + ((secure) ? ";secure" : '');
    }
    window.debug = function (q, w, e, r, t, y) {
        if (!is_dev()) {
            return false;
        }
        if (typeof console != 'undefined') {
            try {
                console.log.apply(console, arguments);
            } catch (err) {
                console.log(q, w, e, r, t, y);
            }
        }
        return false;
    };
    window.is_dev = function () {
        if (document.location.href.match(/dev.hypem.com/i)) {
            return true;
        } else if (document.location.href.match(/debug_console/i)) {
            return true;
        } else {
            return false;
        }
    }
    window.set_ad_vars = function () {
        debug("set_ad_vars() called");
        window.master_ord = parseInt(Math.random() * 10000000000000000);
        var pv_addr;
        if (document.location.href.search(/\/#!?\//) != -1) {
            pv_addr = document.location.hash;
            pv_addr = pv_addr.replace(/#!?\//, '/');
        } else {
            pv_addr = document.location.pathname;
        };
        if (pv_addr == '/' || pv_addr.match(/latest/) || pv_addr.match(/subscription/)) {
            window.master_passback = 'homepage';
        } else if (pv_addr.match(/popular/)) {
            window.master_passback = 'popular';
        } else if (pv_addr.match(/twitter\/popular/)) {
            window.master_passback = 'twitter';
        } else if (pv_addr.match(/radio/)) {
            window.master_passback = 'radioshow';
        } else if (pv_addr.match(/spy/)) {
            window.master_passback = 'spy';
        } else if (pv_addr.match(/about/)) {
            window.master_passback = 'about';
        } else if (pv_addr.match(/contact/)) {
            window.master_passback = 'contact';
        }
        if (typeof takeovers != 'undefined') {
            debug("processing skin initialization for " + takeovers.length + " possible takeovers, we are in " + user_country);
            for (i = 0; i < takeovers.length; i++) {
                var takeover = takeovers[i];
                var offset = 3600 * (takeover.offset || 0);
                var now = server_time + offset;
                if (now < takeover.start || now > takeover.end) continue;
                debug("It's " + now + " in " + takeover.country_code + " which is greater than " + takeover.start + ", so that takeover is now live.");
                if (takeover.country_code != user_country || now < takeover.start || now > takeover.end) continue;
                takeover.match = new RegExp(takeover.match);
                takeover.href = '/takeovers/takeover-images.php?' + takeover.id;
                custom_css.push(takeover);
            }
        }
        var head = document.getElementsByTagName('head')[0];
        for (i = 0; i < custom_css.length; i++) {
            var page = custom_css[i];
            if (typeof (page) != "undefined" && document.location.href.match(page.match)) {
                if (!$('#' + page.id).length) {
                    var css_sheet = document.createElement('link');
                    css_sheet.type = "text/css";
                    css_sheet.rel = "stylesheet";
                    css_sheet.id = page.id;
                    css_sheet.href = page.href;
                    head.appendChild(css_sheet);
                }
                if ($('#takeover-link')) $('#takeover-link').remove();
                if (typeof page.target != 'undefined') {
                    var t = $('<div/>', {
                        id: 'takeover-link'
                    });
                    var a = $('<a/>', {
                        href: page.target,
                        target: '_blank'
                    });
                    var i = $('<img/>', {
                        src: page.pixel + '&cb=' + (+new Date() + Math.round(Math.random() * 1000))
                    });
                    a.append(i);
                    t.append(a);
                    $($('body').get(0)).append(t);
                }
            } else if (typeof (page) != "undefined" && !document.location.href.match(page.match) && $('#' + page.id).length) {
                $('#' + page.id).remove();
                if ($('#takeover-link')) $('#takeover-link').remove();
            } else {}
        }
    }
    $(window).bind('keypress', function (e) {
        var code;
        if (!e) var e = window.event;
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        var t_elt = $(e.target) || $(e.srcElement);
        if (!t_elt.is('input') && !t_elt.is('textarea')) {
            if (e.shiftKey || e.ctrlKey || e.altKey || e.metaKey) {} else {
                var character = String.fromCharCode(code);
                if (character == 'n' || character == 'j' || character == 'b' || code == '39') nextTrack(this);
                else if (character == 'p' || character == 'k' || character == 'z' || code == '37') prevTrack(this);
                else if (character == 'l' || character == 'h' || character == 'f') toggle_favorite('item', trackList[activeList][currentTrack].id);
                else if (character == 'x' || character == 'c' || character == 'z' || code == '37') togglePlaySimple();
                else if (character == 'v') stopTrack();
            }
        }
    });
    window.init_selectors = function () {
        debug("init_selectors called");
        window.player_elements = {
            time_position: $('#player-time-position'),
            time_total: $('#player-time-total'),
            progress_loading: $('#player-progress-loading'),
            progress_playing: $('#player-progress-playing'),
            progress_outer: $('#player-progress-outer'),
            volume_outer: $('#player-volume-outer'),
            volume_ctrl: $('#player-volume-ctrl'),
            volume_mute: $('#player-volume-mute'),
            prev: $('#playerPrev'),
            page: $('#player-page'),
            links: $('#player-links'),
            nowplaying: $('#player-nowplaying'),
            player_controls: $('#player-controls'),
            container: $('#player-container'),
            player_inner: $('#player-inner')
        }
    }
    window.load_user_menu = function () {
        debug("load_user_menu() called");
        $.ajax({
            url: '/inc/header_menu',
            cache: false,
            type: 'get',
            async: true,
            success: function (response) {
                $('#filter').html(response);
            }
        });
    }
    window.page_url_state_init = function () {
        debug("page_url_state_init() called, logged_in: " + is_logged_in + " logged_in_username: " + logged_in_username);
        initial_ts = get_unix_time();
        inital_ref = document.referrer;
        load_ad_code(user_country);
        if (document.location.href.search('\\?forgot=1&email=') != -1) {
            show_lightbox("reset", '/inc/lb_reset_pw.php?key=' + getQueryVariable('key') + '&email=' + getQueryVariable('email'));
        }
        if (is_html5_history_compat()) {
            if (document.location.href.match(/com\/?(\?.+)?$/) && is_logged_in) {
                update_page_contents();
            } else if (document.location.href.match(/com\/?(\?.+)?$/) && !is_logged_in) {
                update_page_contents();
            } else if (document.location.href.match(/\/#!?/)) {
                url = document.location.href.replace(/\/#!?\//, '/');
                history.replaceState(null, null, url);
                first_load = 0;
                update_page_contents();
            } else {
                update_page_contents();
            }
        } else {
            if (document.location.href.match(/\/#!\/$/) && is_logged_in) {
                update_page_contents();
            } else if (!document.location.href.match(/\/#/) && !document.location.href.match(/ax=1/) && !document.location.href.match(/_escaped_fragment_/)) {
                url = document.location.pathname + document.location.search;
                url = url.replace(/\?ax=1/, '');
                url = "/#!" + url;
                top.location = url;
                update_page_contents();
            } else if (!document.location.href.match(/\/#!/) && !document.location.href.match(/ax=1/) && document.location.href.match(/\/#\//)) {
                url = document.location.href.replace(/\/#\//, '/#!/');
                top.location = url;
                update_page_contents();
            } else {
                update_page_contents();
            }
        }
        if (!document.location.href.match(/_escaped_fragment_/)) {
            currentUrl = document.location.pathname + document.location.hash;
            prevUrl = currentUrl;
            setInterval('check_hash_change()', 300);
        }
    }
    window.load_ad_code = function (country) {
        debug('load_ad_code(' + country + ') called');
        if (country == "AU") {
            var elem = document.createElement('script');
            elem.src = "http://secure-au.imrworldwide.com/v60.js";
            elem.id = "au_nielsen";
            elem.async = false;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
        } else if (country != "GB" && country != "IE" && country != "AU" && (ua_info.is_mobile || ads_enabled)) {}
    }
    window.update_page_contents = function () {
        debug('update_page_contents() called');
        if (document.location.pathname == '/' || is_html5_history_compat()) {
            if (document.location.href.search(/_escaped_fragment_/) == -1) {
                $('#player-loading').show();
                if (is_html5_history_compat()) {
                    url = document.location.pathname + document.location.search;
                } else {
                    url = document.location.hash;
                }
                if (url == '') {
                    url = '/';
                } else if (url == '/play_queue') {
                    queueItems = get_site_queue();
                    url = '/set?items=' + queueItems.join(',');
                }
                url = url.replace(/\?ax=1/, '');
                url = url.replace(/^#!?/, '');
                debug("update_page_contents() about to load " + url);

                function post_update_page_contents() {
                    debug("post_update_page_contents() called");
                    $('#player-loading').hide();
                    if (prevUrl != currentUrl && !currentUrl.match(/^\/blogs\//)) {
                        $(window).scrollTop(0);
                    }
                    prevUrl = currentUrl;
                    if (document.location.href.match(/track\//) && typeof (FB) != "undefined" && typeof (FB.XFBML) != "undefined") {
                        FB.XFBML.parse();
                    }
                    if (document.location.href.match(/popular$/)) {
                        setTimeout(function () {
                            render_popular_sparklines();
                        }, 1000);
                    }
                    set_ad_vars();
                    setup_player_bar();
                    ga_pageview();
                    rewrite_links();
                    if (user_country != 'GB' && user_country != 'IE' && user_country != 'AU') {
                        update_buzz_page_state();
                    }
                }
                if (first_load && is_html5_history_compat() && !url.match(/^\/(\?.*)?$/) && !url.match(/latest/)) {
                    debug("update_page_contents loading skipped as page already inside");
                    post_update_page_contents();
                } else {
                    set_ad_vars();
                    page_updater = $.ajax({
                        url: url,
                        data: 'ax=1&ts=' + get_unix_time(),
                        dataType: 'html',
                        type: 'get',
                        async: true,
                        success: function (content) {
                            $('#content-wrapper').html(content);
                            post_update_page_contents();
                        },
                        error: function () {
                            $('#player-loading').hide();
                            setup_player_bar();
                        }
                    });
                }
                first_load = 0;
                if (playerStatus != "PLAYING" && getQueryVariable("autoplay")) {
                    setTimeout(function () {
                        togglePlay();
                    }, 1000);
                }
            }
        }
    }
    window.handle_click = function (event) {
        debug('handle_click(' + event + ') called');
        if (!event) var event = window.event;
        t_elt = event.target || event.srcElement;
        if (t_elt.tagName != 'A') {
            while (t_elt.tagName != 'A') {
                t_elt = t_elt.parentNode;
            }
            url = t_elt.href;
        } else {
            url = t_elt.href;
        }
        if (url.match(/random$/)) {
            load_random_track();
        } else if (url.match(/random_search$/)) {
            load_random_search();
        } else {
            load_url(url, null, event);
        }
        return false;
    }
    window.ga_pageview = function () {
        debug("ga_pageview() called");
        if (typeof (pSUPERFLY) !== "undefined") {
            if (trackList[document.location.href].page_name) {
                if (trackList[document.location.href].page_name == "profile") {
                    pSUPERFLY.virtualPage("/" + trackList[document.location.href].page_mode, trackList[document.location.href].page_name + ": " + trackList[document.location.href].page_mode);
                } else {
                    pSUPERFLY.virtualPage("/" + trackList[document.location.href].page_name, trackList[document.location.href].page_name);
                }
            }
        }
        var pv_addr;
        if (document.location.href.search(/\/#!?\//) != -1) {
            pv_addr = document.location.hash;
            pv_addr = pv_addr.replace(/#!?\//, '/');
        } else {
            pv_addr = document.location.pathname;
        }
        try {
            pageTracker = _gat._getTracker("UA-97430-1");
            pageTracker._trackPageview(pv_addr);
        } catch (err) {
            debug("GOOGLE ANALYTICS ERROR: " + err);
        }
        try {
            if (typeof (__qc) != 'undefined' && __qc.qpixelsent && __qc.qpixelsent.length) {
                __qc.qpixelsent = [];
            }
            if (user_country == 'GB' || user_country == 'IE') {
                _qevents.push([{
                    qacct: "p-f66q2dQu9xpU2"
                }, {
                    qacct: "p-70RhgKmunzjBs",
                    labels: "Music"
                }]);
            } else {
                _qevents.push({
                    qacct: "p-f66q2dQu9xpU2"
                });
            }
        } catch (err) {
            debug("QUANTCAST ERROR: " + err);
        }
        if (user_country == "AU") {
            update_nielsen(pv_addr);
        }
    }
    window.update_buzz_page_state = function () {
        debug("update_buzz_page_state() called");
        var buzz_url;
        try {
            if (is_html5_history_compat()) {
                buzz_url = document.location.pathname + document.location.search;
            } else {
                buzz_url = document.location.hash;
            }
            bmQuery.bmLib.geoLocation = user_country;
            bmQuery.bmLib.pageChange(buzz_url);
        } catch (err) {
            debug("bmQuery exception [retrying...] : " + err);
            setTimeout(function () {
                update_buzz_page_state();
            }, 500);
        }
    }
    window.update_nielsen = function (pv_addr) {
        debug("update_nielsen() called");
        try {
            var pvar = {
                cid: "inthemix",
                content: "0",
                server: "secure-au",
                page_url: "http://hypem.com" + pv_addr
            };
            var trac = nol_t(pvar);
            trac.record().post();
        } catch (err) {
            debug("update_nielsen exception [retrying...] : " + err);
            setTimeout(function () {
                update_nielsen();
            }, 1000);
        }
    }
    window.dfp_extras_var_passthru = function () {
        var appended_str = '';
        var dfpKeyword = getQueryVariable("dfpKeyword");
        if (dfpKeyword != '' && typeof (dfpKeyword) != 'undefined') {
            appended_str += "&dfpKeyword=" + dfpKeyword;
        }
        if (document.location.href.match(/ceelodistilled/)) {
            appended_str += "&dfpKeyval=campaign:absolut";
        } else if (document.location.href.match(/singer-songwriter/)) {
            appended_str += "&dfpKeyval=campaign:singersongwriter";
        } else {
            var dfpKeyval = getQueryVariable("dfpKeyval");
            if (dfpKeyval != '' && typeof (dfpKeyval) != 'undefined') {
                appended_str += "&dfpKeyval=" + dfpKeyval;
            }
        }
        return appended_str;
    }
    window.dfp_extras_passback = function (country) {
        var page_name, page_num, passback;
        if (trackList && trackList[document.location.href] && trackList[document.location.href]['page_name']) {
            page_name = trackList[document.location.href]['page_name'];
        }
        if (trackList && trackList[document.location.href] && trackList[document.location.href]['page_num']) {
            page_num = trackList[document.location.href]['page_num'];
        }
        if (country == 'AU' || country == 'GB' || country == 'IE') {
            if ((page_name == "index" || page_name == "profile") && (page_num == 1)) {
                passback = "homepage";
            } else if ((page_name == "index" || page_name == "profile") && (page_num > 1 && page_num < 7)) {
                passback = "page" + page_num;
            } else if (page_name == "popular") {
                passback = "popular";
            } else if (page_name == "tweets_popular") {
                passback = "twitter";
            } else if (page_name == "radio_show") {
                passback = "radioshow";
            } else if (page_name == "spy") {
                passback = "spy";
            } else if (page_name == "about") {
                passback = "about";
            } else if (page_name == "contact") {
                passback = "contact";
            } else {
                passback = "ros";
            }
        } else {
            if ((page_name == "index" || page_name == "profile") && page_num == 1) {
                passback = "homepage";
            } else if ((page_name == "index" || page_name == "profile") && (page_num > 1 && page_num < 7)) {
                passback = "page" + page_num;
            } else if (page_name == "radio_show") {
                passback = "radioshow";
            } else if (page_name == "umf2011") {
                passback = "umf2011";
            } else {
                passback = "ros";
            }
        }
        return passback;
    }
    window.resize_ad_element = function (pos, size) {
        debug("resize_ad_element(" + pos + ", " + size + ") called");
        if (pos.match(/ad-rectangle/)) {
            $('#' + pos).height(size.height);
        } else if (pos.match(/ad-leaderboard/)) {
            $('#' + pos).css({
                'width': size.width,
                'height': size.height
            });
            $('#' + pos).parent().css({
                'width': size.width,
                'height': size.height,
                'margin-top': '-3px',
                'margin-left': (Math.round(1051 - size.width) / 2) + 'px'
            });
        }
    }
    window.load_url = function (url, action_src, e) {
        debug("load_url(" + url + ", " + action_src + ") called");
        disable_infinite_page_scroll();
        if (ua_info.is_mobile && $('#menu-item-back')) {
            $('#menu-item-back').show();
        }
        if (page_updater && page_updater.readyState != 4) {
            debug("ignored click to " + url);
            return false;
        }
        if (action_src === '' && action_src != 'undefined') {
            action_src = trackList[activeList].page_name;
        }
        try {
            if (url.match(/amazon/i)) {
                pageTracker._trackEvent("Affiliate", "Amazon", action_src);
            } else if (url.match(/emusic/i)) {
                pageTracker._trackEvent("Affiliate", "Emusic", action_src);
            } else if (url.match(/itunes/i)) {
                pageTracker._trackEvent("Affiliate", "iTunes", action_src);
            } else if (url.match(/songkick/i)) {
                pageTracker._trackEvent("Affiliate", "Songkick", action_src);
            } else if ((url.match(/track\//i) || url.match(/item\//i) || url.match(/track\//i)) && url.match(/rec_ref=/)) {
                pageTracker._trackEvent("Track", "Suggestion", action_src);
            } else if (url.match(/search\//i) && url.match(/rec_ref=/)) {
                pageTracker._trackEvent("Search", "Suggestion", action_src);
            } else if (url.match(/search\//i)) {
                pageTracker._trackEvent("Search", "Normal", action_src);
            } else if (url.match(/artist\//i)) {
                pageTracker._trackEvent("Search", "Artist_click", action_src);
            } else if (url.match(/track\//i) || url.match(/item\//i) || url.match(/song\//i)) {
                pageTracker._trackEvent("Track", "Click", action_src);
            } else if (!url.match(/hypem.com/i)) {
                pageTracker._trackEvent("Blog", "Post_click", action_src);
            }
        } catch (err) {
            debug("GOOGLE ANALYTICS ERROR: " + err);
        }
        if ((url.indexOf('/') === 0 || url.search(/^https?:\/\/.*?hypem\.com\//) != -1) && url.search('/go/') == -1 && url.search('/fast-forward\/launch') == -1 && url.search('/download') == -1 && url.search('/admin/') == -1 && url.search('/share') == -1 && url.search('/feed\/.+?xml$') == -1 && url.search('/playlist') == -1 && url.search(/http:\/\/blog\./) == -1 && url.search(/http:\/\/stream\./) == -1 && url.search(/http:\/\/merch\./) == -1 && url.search(/api\/twitter/) == -1 && url.search('/labs\/ff/') == -1 && (typeof (e) == "undefined" || !e.metaKey)) {
            if (document.location.href.search(/_escaped_fragment_/) == -1) {
                url = url.replace(/^https?:\/\/.+?\//, '/');
                url = url.replace(/[\?&]ax=1/, '');
                if (url.match(/%2F/i) || url.match(/%3F/i) || url.match(/%252F/i) || url.match(/%2B/i) || url.match(/%25/i) || /MSIE/.test(navigator.userAgent)) {} else {
                    try {
                        url = decodeURIComponent(url);
                    } catch (err) {}
                }
                if (is_html5_history_compat()) {
                    currentUrl = url;
                    history.pushState(null, null, document.location.protocol + '//' + document.location.host + url);
                } else {
                    currentUrl = "/#!" + url;
                    top.location.href = "/#!" + url;
                }
                disable_notification_check();
                update_page_contents();
            } else {
                top.location.href = url;
            }
        } else {
            try {
                window.open(url, "_blank", '');
                return false;
            } catch (e) {
                window.open(url, '_blank', 'width=' + window.screen.availWidth + ',height=' + window.screen.availHeight + ',menubar=yes,toolbar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes');
                return false;
            }
        }
        return false;
    }
    window.check_hash_change = function () {
        if (currentUrl != document.location.pathname + document.location.hash) {
            debug("detected url change from " + currentUrl + " to " + document.location.pathname + document.location.hash);
            currentUrl = document.location.pathname + document.location.hash;
            if (skip_update_page_contents) {
                skip_update_page_contents = 0;
                debug("ignored page reload event due to autoscroll");
                return false;
            }
            if (page_updater && page_updater.readyState != 4) {
                debug("ignored page reload event due to loading in progress");
                return false;
            }
            update_page_contents();
        }
    }
    window.rewrite_links = function () {
        debug("rewrite_links() called");
        $("a[href^='/']").each(function (i, elt) {
            $(elt).unbind('click').bind('click', handle_click);
        });
        $("a[href^='http']").each(function (i, elt) {
            $(elt).unbind('click').bind('click', handle_click);
        })
    }
    window.get_visitorid = function () {
        var cookie_txt = decodeURIComponent(get_cookie("AUTH"));
        var cookie_arr = cookie_txt.split(":");
        return cookie_arr[1];
    }
    window.hide_notice = function (cookie_key) {
        jQuery('#top-notice').hide();
        jQuery('#player-container').css({
            top: ''
        });
        if (jQuery('body').css('background-image')) {
            jQuery('body').css({
                'background-position': 'center 77px'
            });
        }
        set_cookie(cookie_key, 'true', '30', '/', '', '');
    }
    window.set_site_queue = function (queueItems) {
        set_cookie("play_queue", JSON.stringify(queueItems), '30', '/', '', '');
    }
    window.get_site_queue = function () {
        var data = get_cookie("play_queue");
        return JSON.parse(data);
    }
    window.getQueryVariable = function (variable) {
        var query = window.location.href.substring(window.location.href.indexOf("?") + 1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
    }
    window.load_search = function () {
        var query = $('#q').val();
        if (query && query != "Artist or Track") {
            query = $.trim(query);
            query = urlencode_kinda(query);
            load_url("/search/" + query + "/1/", "search_box");
        } else {
            load_random_search(true);
        }
    }
    window.urlencode_kinda = function (str) {
        if (/MSIE/.test(navigator.userAgent)) {
            str = encodeURIComponent(str);
        } else {
            str = str.replace(/\+/g, '%2B');
            str = str.replace(/\%/g, '%25');
            str = str.replace(/\//g, '%2F');
            str = str.replace(/\?/g, '%3F');
            str = str.replace(/([^\s])&([^\s])/, '$1%2526$2');
        }
        return str;
    }
    window.load_random_search = function (forced) {
        var action_src;
        if (forced) {
            action_src = "search_box";
        } else {
            action_src = "search_shuffe_button";
        }
        $.ajax({
            url: '/random_search',
            data: 'rel_only=1',
            cache: false,
            type: 'get',
            async: true,
            success: function (transport) {
                var response = transport.responseText || "/random_search";
                load_url(response, action_src);
            },
            error: function () {
                load_url('/random_search', action_src);
            }
        });
    }
    window.load_random_track = function () {
        $.ajax({
            url: '/random',
            data: 'rel_only=1',
            cache: false,
            type: 'get',
            async: true,
            success: function (transport) {
                var response = transport.responseText || "/random";
                load_url(response, "item");
            },
            error: function () {
                load_url('/random', "item");
            }
        });
    }
    window.get_unix_time = function () {
        return parseInt(+new Date() / 1000);
    }
    window.is_int = function (value) {
        if ((parseFloat(value) == parseInt(value)) && !isNaN(value)) {
            return true;
        } else {
            return false;
        }
    }
    window.sec_to_str = function (nSec) {
        var min = Math.floor(nSec / 60);
        var sec = nSec - (min * 60);
        return min + ':' + (sec < 10 ? '0' + sec : sec);
    }
    window.getQueryVariable = function (variable) {
        var query = window.location.href.substring(window.location.href.indexOf("?") + 1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) return pair[1];
        }
    }
    window.sm_onload = function () {
        debug("sm_onload fired, readystate is " + currentPlayerObj[(currentPlayerObj.length - 1)].readyState + " duration:" + parseInt(currentPlayerObj[(currentPlayerObj.length - 1)].duration / 1000));
        if (this.readyState == 2 || (this.readyState == 3 && !ua_info.is_ios && !ua_info.is_android && (this.bytesLoaded < 50000 || this.duration == 0))) {
            if (this.url.match(/(hypem|archive)/) || this.final) {
                debug("FAILED, giving up reloading, running nextTrack (failed url: " + this.url + ' )');
                nextTrack();
            } else {
                debug("FAILED SM, retrying... " + this.url);
                retryLoadTrack();
            }
        } else {
            debug("sm_onload reports success, setting player_duration...?");
            if (activeItem.time) {
                player_duration = activeItem.time;
            }
        }
    }
    window.sm_onplay = function () {
        if (this.sID != ("currentPlayer" + currentTrack)) {
            return false;
        }
        playerStatus = "PLAYING";
        debug("sm_onplay fired, readyState: " + this.readyState + ", playState: " + this.playState);
        showPlaying();
        document.title = document.title.replace(/\u25FC/, '\u25BA');
    }
    window.sm_onresume = function () {
        playerStatus = "PLAYING";
        debug("sm_onresume fired");
        showPlaying();
        document.title = document.title.replace(/\u25FC/, '\u25BA');
    }
    window.showPlaying = function (playing) {
        if (typeof playing == 'undefined') playing = true;
        if (activeList == document.location.href) {
            if (playing && !playback_allowed) {
                show_lightbox('listen');
                stopTrack();
                return false;
            }
            player_elements.main_button = $('#player-main-button');
            if (player_elements.main_button.length) {
                if (playing) {
                    player_elements.main_button.addClass("pause");
                } else {
                    player_elements.main_button.removeClass("pause");
                }
            }
            player_elements.play = $('#playerPlay');
            if (player_elements.play.length) {
                if (playing) {
                    player_elements.play.addClass("pause");
                } else {
                    player_elements.play.removeClass("pause");
                }
            }
            if (playing) {
                update_current_play_ctrl("pause");
                set_track_bg(trackList[activeList][currentTrack].id, "green");
            } else {
                var play_track_current = $('#play_track_' + currentTrack);
                if (play_track_current.length) {
                    play_track_current.removeClass().addClass("play");
                }
            }
        }
    }
    window.sm_onpause = function () {
        debug('on_pause fired');
        if (this.sID != 'currentPlayer' + currentTrack) return false;
        playerStatus = 'PAUSED';
        showPlaying(false);
        document.title = document.title.replace(/\u25BA/, '\u25FC');
    }
    window.sm_onfinish = function () {
        debug("sm_onfinish fired");
        playerStatus = "COMPLETED";
        stopTrack();
        nextTrack();
    }
    window.sm_whileplaying = function () {
        if (this.sID != ("currentPlayer" + currentTrack)) return false;
        if (dragging_position) return;
        if (player_position != parseInt(this.position / 1000)) {
            player_position = parseInt(this.position / 1000);
            $('#player-loading').hide();
            if (is_fade_enabled() && (player_duration - player_position) == 15 && playback_allowed) {
                loadNextTrack();
            } else if (is_fade_enabled() && (player_duration - player_position) == 3 && currentPlayerObj[1].bytesLoaded && playback_allowed) {
                beginFadeTransition();
            }
            player_elements.time_position.html(sec_to_str(player_position));
            player_elements.time_total.html(sec_to_str(player_duration));
            player_elements.progress_playing.width((Math.round(player_position / player_duration * 100 * 100) / 100) + '%');
        }
    }
    window.sm_whileloading = function () {
        if (this.sID != "currentPlayer" + currentTrack) {
            return false;
        }
        var percent_loaded = (Math.round((this.bytesLoaded / this.bytesTotal) * 100) * 100 / 100) + '%';
        player_elements.progress_loading.animate({
            width: percent_loaded
        }, 50);
        if (!activeItem.time) {
            player_duration = parseInt(currentPlayerObj[0].duration / 1000);
            player_elements.time_total.html(sec_to_str(parseInt(player_duration)));
        }
    }
    window.sm_start_drag = function (evt) {
        if (!evt) var evt = window.event;
        t_elt = evt.target || evt.srcElement;
        if (t_elt.id.match(/progress/)) {
            dragging_position = true;
            $(window).unbind('mousemove').bind('mousemove', sm_follow_progress_drag).unbind('mouseup').bind('mouseup', sm_end_drag);
        }
        if (t_elt.id.match(/volume/)) {
            dragging_position = true;
            $(window).unbind('mousemove').bind('mousemove', sm_follow_volume_drag).unbind('mouseup').bind('mouseup', sm_end_drag);
        }
        return false;
    }
    window.sm_follow_volume_drag = function (evt) {
        if (!evt) var evt = window.event;
        t_elt = evt.target || evt.srcElement;
        sm_update_volume(evt, t_elt, false);
    }
    window.sm_follow_progress_drag = function (evt) {
        if (!evt) var evt = window.event;
        t_elt = evt.target || evt.srcElement;
        var x = parseInt(evt.clientX);
        var offset = player_elements.progress_loading.offset();
        var nMsecOffset = Math.floor((x - offset.left - 4) / (player_elements.progress_outer.width()) * (activeItem.time * 1000));
        if (!isNaN(nMsecOffset)) nMsecOffset = Math.min(nMsecOffset, currentPlayerObj[0].duration);
        player_elements.time_position.html(sec_to_str(player_position));
        player_elements.progress_playing.width((Math.round(player_position / player_duration * 100 * 100) / 100) + '%')
        sm_update_progress(evt, t_elt);
        if (player_position >= player_duration) sm_end_drag();
    }
    window.sm_end_drag = function (evt) {
        if (!evt) var evt = window.event;
        t_elt = evt.target || evt.srcElement;
        debug('END DRAGGING');
        dragging_position = false;
        $(window).unbind('mousemove').unbind('mouseup');
        if (t_elt.id.match(/progress/)) {
            sm_update_progress(evt, t_elt);
        }
        if (t_elt.id.match(/volume/)) {
            sm_update_volume(evt, t_elt, true);
            var x = parseInt(evt.clientX);
            var offset = player_elements.volume_outer.offset();
            var volume_pc = parseInt(Math.floor((x - offset.left - 2) / (player_elements.volume_outer.width()) * 100));
            $.ajax({
                url: '/inc/user_action.php',
                data: 'act=volume&value=' + volume_pc + '&session=' + get_visitorid(),
                cache: false,
                type: 'post',
                async: true
            });
        }
        return false;
    }
    window.sm_update_volume = function (evt, t_elt, morph) {
        debug(t_elt.tagName + "clicked X:" + evt.clientX);
        var x = parseInt(evt.clientX);
        var offset = player_elements.volume_outer.offset();
        var volume_pc = parseInt(Math.floor((x - offset.left - 2) / (player_elements.volume_outer.width()) * 100));
        if (!isNaN(volume_pc)) volume_pc = Math.min(volume_pc, 100);
        if (!isNaN(volume_pc) && currentPlayerObj[0]) currentPlayerObj[0].setVolume(volume_pc);
        if (!isNaN(volume_pc)) player_volume = volume_pc;
        if (morph) {
            player_elements.volume_ctrl.animate({
                width: volume_pc + '%'
            }, 50);
        } else {
            player_elements.volume_ctrl.width(volume_pc + '%');
        }
    }
    window.sm_update_progress = function (evt, t_elt) {
        debug(t_elt.tagName + " " + t_elt.id + " clicked X:" + evt.clientX);
        var x = parseInt(evt.clientX);
        var offset = player_elements.progress_loading.offset();
        var nMsecOffset = Math.floor((x - offset.left - 4) / (player_elements.progress_outer.width()) * (player_duration * 1000));
        if (!isNaN(nMsecOffset)) nMsecOffset = Math.min(nMsecOffset, (player_duration * 1000));
        if (!isNaN(nMsecOffset)) currentPlayerObj[0].setPosition(nMsecOffset);
        player_position = parseInt(nMsecOffset / 1000);
        if (currentPlayerObj[1]) {
            clearInterval(currentPlayerObj[0].fadeIn);
            currentPlayerObj[1].destruct();
            clearInterval(currentPlayerObj[0].fadeOut);
            currentPlayerObj[0].setVolume(player_volume);
        }
    }
    window.sm_toggle_mute = function () {
        if (currentPlayerObj[0] && currentPlayerObj[0].muted) {
            player_elements.volume_mute.removeClass('player-volume-mute-active').addClass('player-volume-mute-inactive');
            player_elements.volume_ctrl.animate({
                width: player_volume
            }, 500);
            currentPlayerObj[0].unmute();
        } else if (currentPlayerObj[0]) {
            player_elements.volume_mute.removeClass('player-volume-mute-inactive').addClass('player-volume-mute-active');
            player_elements.volume_ctrl.animate({
                width: 0 + '%'
            }, 500);
            currentPlayerObj[0].mute();
        }
    }
    window.loadNextTrack = function (skip) {
        debug("loadNextTrack(" + skip + ") called");
        if (typeof (skip) === "undefined") {
            skip = 1;
        }
        var track = currentTrack + skip;
        if (trackList[activeList][track]) {
            if (trackList[activeList][track].type != '' && trackList[activeList][track].key != '') {
                var r = $.ajax({
                    url: '/serve/source/' + trackList[activeList][track].id + '/' + trackList[activeList][track].key,
                    type: 'get',
                    async: false,
                    cache: false,
                    dataType: 'json',
                    error: function () {
                        debug('loadNext /source/ request FAILED');
                    }
                });
                try {
                    response = jQuery.parseJSON(r.responseText);
                } catch (err) {
                    debug("FAILED to parse JSON data");
                    nextTrack();
                    return false;
                }
                try {
                    currentPlayerObj[1] = soundManager.createSound({
                        id: 'currentPlayer' + track,
                        url: response.url,
                        autoLoad: true,
                        autoPlay: false,
                        onload: sm_onload,
                        onplay: sm_onplay,
                        onresume: sm_onresume,
                        onpause: sm_onpause,
                        whileplaying: sm_whileplaying,
                        whileloading: sm_whileloading,
                        onfinish: sm_onfinish,
                        volume: 0
                    });
                    currentPlayerObj[1].final = response.final;
                } catch (err) {
                    debug('Can\'t create sound! :(' + err.description);
                }
                return true;
            } else {
                skip++;
                loadNextTrack(skip);
            }
        } else {
            return false;
        }
    }
    window.retryLoadTrack = function () {
        debug("retryLoadTrack() called");
        if (currentPlayerObj[1]) {
            var prev_url_data = {};
            prev_url_data.url = currentPlayerObj[1].url;
            prev_url_data.readyState = currentPlayerObj[1].readyState;
            prev_url_data.bytesLoaded = currentPlayerObj[1].bytesLoaded;
            prev_url_data.duration = currentPlayerObj[1].duration;
            var track = currentTrack + skip;
            currentPlayerObj[1].destruct();
            var r = $.ajax({
                url: '/serve/source/' + trackList[activeList][track].id + '/' + trackList[activeList][track].key,
                data: 'retry=1&readyState=' + prev_url_data.readyState + '&bytesLoaded=' + prev_url_data.bytesLoaded + '&duration=' + prev_url_data.duration + '&prev_url=' + Base64.encode(prev_url_data.url),
                type: 'get',
                async: false,
                cache: false,
                dataType: 'json',
                error: function () {
                    debug('retryLoadTrack /source/ request FAILED');
                }
            });
            try {
                response = jQuery.parseJSON(r.responseText);
            } catch (err) {
                debug("FAILED to parse JSON data");
                nextTrack();
                return false;
            }
            try {
                currentPlayerObj[1] = soundManager.createSound({
                    id: 'currentPlayer' + track,
                    url: response.url,
                    autoLoad: true,
                    autoPlay: false,
                    onload: sm_onload,
                    onplay: sm_onplay,
                    onresume: sm_onresume,
                    onpause: sm_onpause,
                    whileplaying: sm_whileplaying,
                    whileloading: sm_whileloading,
                    onfinish: sm_onfinish,
                    volume: 0
                });
                currentPlayerObj[1].final = response.final;
            } catch (err) {
                debug('Can\'t create sound! :(' + err.description);
            }
        } else {
            var prev_url_data = {};
            prev_url_data.url = currentPlayerObj[0].url;
            prev_url_data.readyState = currentPlayerObj[0].readyState;
            prev_url_data.bytesLoaded = currentPlayerObj[0].bytesLoaded;
            prev_url_data.duration = currentPlayerObj[0].duration;
            if (currentPlayerObj[0]) {
                currentPlayerObj[0].destruct();
            }
            var r = $.ajax({
                url: '/serve/source/' + trackList[activeList][(currentTrack)].id + '/' + trackList[activeList][(currentTrack)].key,
                data: 'retry=1&readyState=' + prev_url_data.readyState + '&bytesLoaded=' + prev_url_data.bytesLoaded + '&duration=' + prev_url_data.duration + '&prev_url=' + Base64.encode(prev_url_data.url),
                type: 'get',
                async: false,
                cache: false,
                dataType: 'json',
                error: function () {
                    debug('retryLoadTrack /source/ request FAILED');
                }
            });
            try {
                response = jQuery.parseJSON(r.responseText);
            } catch (err) {
                debug("FAILED to parse JSON data");
                nextTrack();
                return false;
            }
            try {
                currentPlayerObj[0] = soundManager.createSound({
                    id: 'currentPlayer' + currentTrack,
                    url: response.url,
                    autoLoad: true,
                    autoPlay: true,
                    onload: sm_onload,
                    onplay: sm_onplay,
                    onresume: sm_onresume,
                    onpause: sm_onpause,
                    whileplaying: sm_whileplaying,
                    whileloading: sm_whileloading,
                    onfinish: sm_onfinish,
                    volume: player_volume
                });
                currentPlayerObj[0].final = response.final;
            } catch (err) {
                debug('Can\'t create sound! :(' + err.description);
            }
        }
    }
    window.beginFadeTransition = function () {
        debug("beginFadeTransition() called");
        currentPlayerObj[1].play();
        fadeOutSound(currentPlayerObj[0], -10, 300);
        fadeInSound(currentPlayerObj[1], 10, 300);
    }
    window.fadeInSound = function (soundObj, amount, ms_delay) {
        if (soundObj) {
            var vol = soundObj.volume;
            debug("fadeInSound, current: " + vol + ", " + amount + ", " + ms_delay)
            if (vol >= player_volume) return false;
            soundObj.setVolume(Math.min(player_volume, vol + amount));
            soundObj.fadeIn = setTimeout(function () {
                fadeInSound(soundObj, amount, ms_delay)
            }, ms_delay);
        }
    }
    window.fadeOutSound = function (soundObj, amount, ms_delay) {
        if (soundObj) {
            var vol = soundObj.volume;
            debug("fadeOutSound, current: " + vol + ", " + amount + ", " + ms_delay);
            if (vol == 0) return false;
            soundObj.setVolume(Math.max(0, vol + amount));
            soundObj.fadeOut = setTimeout(function () {
                fadeOutSound(soundObj, amount, ms_delay)
            }, ms_delay);
        }
    }
    window.is_fade_enabled = function () {
        if (!ua_info.is_ios && !ua_info.is_android && !ua_info.is_playbook && trackList[activeList].length > 1 && !is_spy_page()) {
            return true;
        } else {
            return false;
        }
    }
    window.is_html5_history_compat = function () {
        return !!(window.history && history.pushState);
    }
    window.update_current_play_ctrl = function (mode) {
        if (activeItem) {
            if ($('#play_ctrl_' + activeItem.id).length) {
                $('#play_ctrl_' + activeItem.id).removeClass().addClass('play-ctrl ' + mode);
            }
        }
    }
    window.togglePlayByItemid = function (itemid, evt) {
        debug("togglePlayByItemid(" + itemid + ", " + evt + ") called");
        for (var i = 0; i < trackList[document.location.href].length; i++) {
            debug(' checking ' + trackList[document.location.href][i]['id'] + ' vs ' + itemid);
            if (trackList[document.location.href][i]['id'] == itemid) {
                togglePlay(i, evt);
                return false;
            }
        }
    }
    window.is_spy_page = function () {
        if (document.location.href.match(/\/spy$/) || document.location.href.match(/\/obsessedspy$/) || document.location.href.match(/\/lovespy$/)) {
            return true;
        } else {
            return false;
        }
    }
    window.is_shuffle_page = function () {
        if (document.location.href.match(/\/shuffle/)) {
            return true;
        } else {
            return false;
        }
    }
    window.togglePlaySimple = function () {
        if (playerStatus == 'PAUSED') {
            if (trackList[activeList][currentTrack].type == 'normal') {
                update_current_play_ctrl('pause');
                currentPlayerObj[0].resume();
            }
        } else if (playerStatus == 'PLAYING') {
            if (trackList[activeList][currentTrack].type == 'normal') {
                update_current_play_ctrl('play');
                currentPlayerObj[0].pause();
                if (currentPlayerObj[1]) {
                    currentPlayerObj[1].pause();
                    nextTrack();
                }
            }
        } else if (playerStatus === '') {
            togglePlay();
        }
    }
    window.togglePlay = function (id, evt) {
        debug("togglePlay(" + id + ", " + evt + ") called");
        if ((id == currentTrack || typeof id == 'undefined') && (trackList[activeList][currentTrack].id == activeItem.id || typeof id == 'undefined') && typeof currentTrack != 'undefined' && playerStatus != '' && activeList == document.location.href && (playerStatus == 'PAUSED' || playerStatus == 'PLAYING')) {
            togglePlaySimple();
        } else {
            stopTrack();
            if (activeList != document.location.href) {
                trackList[activeList] = Array();
                activeList = document.location.href;
                player_elements.page.css('visibility', 'hidden');
            }
            if (evt && evt.shiftKey) {
                var queueItems = get_site_queue();
                if (typeof queueItems == 'undefined' || queueItems == null) {
                    queueItems = Array();
                }
                for (var i = 0; i < queueItems.length;) {
                    if (this[i] === trackList[document.location.href][id].id) {
                        queueItems.splice(i, 1);
                        set_site_queue(queueItems);
                        return false;
                    } else {
                        ++i;
                    }
                }
                if (typeof id == 'undefined') id = 0;
                queueItems.push(trackList[document.location.href][id].id);
                set_site_queue(queueItems);
            } else {
                if (typeof id == 'undefined') id = 0;
                currentTrack = id;
                playback_manual = 1;
                playTrack();
            }
        }
        return false;
    }
    window.stopTrack = function () {
        debug("stopTrack called");
        if (currentTrack === undefined) {
            return false;
        }
        if (trackList[activeList] === undefined) {
            return false;
        }
        if (trackList[activeList][currentTrack] === undefined) {
            return false;
        }
        if (activeList == document.location.href) {
            if (activeItem) {
                set_track_bg(activeItem.id, '');
            }
            update_current_play_ctrl('play');
            if ($('#player-main-button').length) {
                $('#player-main-button').removeClass();
            }
            if ($('#playerPlay').length) {
                $('#playerPlay').removeClass();
            }
        }
        disable_playback_check();
        if (trackList[activeList][currentTrack].type == 'normal') {
            if (currentPlayerObj[0] && currentPlayerObj[0].playState != 0) {
                debug("executing .stop();");
                currentPlayerObj[0].stop();
            }
            document.title = trackList[activeList].title;
        } else {
            return false;
        }
    }
    window.playTrack = function (skip_prompts) {
        debug("playTrack called");
        if (!playback_allowed) {
            show_lightbox("listen");
            return false;
        } else if (trackList[activeList] && trackList[activeList][currentTrack].buy_prompt == 1 && skip_prompts != 1) {
            show_lightbox("buy_track");
            return false;
        } else if (!trackList[activeList]) {
            return false;
        }
        $('#player-loading').show();
        if (trackList[activeList].length > 1) {
            player_elements.prev.removeClass("playerPrev-inactive").addClass("playerPrev-active").removeClass("playerNext-inactive").addClass("playerNext-active");
            playback_event_count = 0;
        }
        playback_event_count = 0;
        activeItem = trackList[activeList][currentTrack];
        enable_playback_check();
        show_player_bar();
        set_now_playing_info(currentTrack);
        if (activeList == document.location.href) {
            set_track_bg(trackList[activeList][currentTrack].id, "green");
            update_current_play_ctrl("pause");
            if ($('#player').length) {
                $('#player-main-button').removeClass().addClass("pause");
            }
            player_elements.play = $('#playerPlay');
            if (player_elements.play.length) {
                player_elements.play.removeClass().addClass("pause");
            }
        }
        if (activeItem.artist) {
            now_playing_title = "\u25BA " + activeItem.artist + ' - ' + activeItem.song + ' / The Hype Machine';
        } else {
            now_playing_title = "\u25BA " + activeItem.song + ' / The Hype Machine';
        }
        document.title = now_playing_title;
        if (activeItem.type == "normal" || activeItem.type == "sc_special") {
            if (currentPlayerObj[0]) {
                currentPlayerObj[0].destruct();
            }
            if (currentPlayerObj[1] && currentPlayerObj[1].sID == "currentPlayer" + currentTrack) {
                tmp = currentPlayerObj.shift();
                if (activeItem.time) {
                    player_duration = activeItem.time;
                }
                if (playerStatus == "COMPLETED") {
                    playerStatus = "PLAYING";
                }
            } else {
                if (currentPlayerObj[1]) {
                    tmp = currentPlayerObj.shift();
                }
                if (currentPlayerObj[0]) {
                    currentPlayerObj[0].destruct();
                }
                var source_data;
                if (activeItem.type == "sc_special") {
                    var response = {};
                    response.url = activeItem.sc_stream;
                } else {
                    var r = $.ajax({
                        url: '/serve/source/' + activeItem.id + '/' + activeItem.key,
                        type: 'get',
                        async: false,
                        cache: false,
                        dataType: 'json',
                        error: function () {
                            debug('playTrack /source/ request FAILED');
                        }
                    });
                    try {
                        response = jQuery.parseJSON(r.responseText);
                    } catch (err) {
                        debug("FAILED to parse JSON data");
                        nextTrack();
                        return false;
                    }
                }
                try {
                    currentPlayerObj[0] = soundManager.createSound({
                        id: 'currentPlayer' + currentTrack,
                        url: response.url,
                        autoLoad: true,
                        autoPlay: true,
                        onload: sm_onload,
                        onplay: sm_onplay,
                        onresume: sm_onresume,
                        onpause: sm_onpause,
                        whileplaying: sm_whileplaying,
                        whileloading: sm_whileloading,
                        onfinish: sm_onfinish,
                        volume: player_volume
                    });
                    currentPlayerObj[0].final = response.final;
                    if (activeItem.time) {
                        player_duration = activeItem.time;
                    }
                    setTimeout(function () {
                        if (currentPlayerObj[0].bytesLoaded < 1000 && currentPlayerObj[0].duration < 5 && !currentPlayerObj[0].final) {
                            debug("Loading appears to have failed");
                            disable_playback_check();
                            retryLoadTrack();
                        }
                    }, 5000);
                } catch (err) {
                    debug('wtf, cant create sound! :(', err);
                }
            }
        }
        if ($('player-volume-mute').className == 'player-volume-mute-active') {
            currentPlayerObj[0].mute();
        }
    }
    window.nextTrack = function (clicked_obj) {
        debug("nextTrack, from: " + clicked_obj);
        playback_manual = (typeof clicked_obj != 'undefined') ? 1 : 0;
        while ((trackList[activeList].length - 1) > currentTrack && typeof trackList[activeList][(currentTrack + 1)] != "undefined") {
            if (trackList[activeList][(currentTrack + 1)].type != '') {
                stopTrack();
                currentTrack++;
                debug('Current track: ' + currentTrack);
                playTrack();
                return false;
            } else {
                currentTrack++;
            }
        }
        stopTrack();
    }
    window.prevTrack = function (clicked_obj) {
        debug("PREV from: " + clicked_obj);
        playback_manual = (typeof clicked_obj != 'undefined') ? 1 : 0;
        while (currentTrack > 0 && typeof trackList[activeList][(currentTrack - 1)] != "undefined") {
            if (trackList[activeList][(currentTrack - 1)].type != '') {
                stopTrack();
                currentTrack--;
                playTrack();
                return false;
            } else {
                currentTrack--;
            }
        }
        return false;
    }
    window.set_track_bg = function (itemid, color) {
        debug("set_track_bg(" + itemid + "," + color + ") called");
        if (document.location.href.match(/\/sxsw2009/) || is_spy_page()) {
            return false;
        }
        var track_obj = $('#section-track-' + itemid);
        if (color == "green" && typeof track_obj != 'undefined') {
            track_obj.addClass('active-playing-green');
            return true;
        } else if (typeof track_obj != 'undefined') {
            track_obj.removeClass("active-playing-green");
            return true;
        } else {
            debug("Could not find section-track-" + itemid);
            return false;
        }
    }
    window.set_now_playing_info = function () {
        debug("set_now_playing_info() called");
        if (typeof (player_elements) == "undefined") {
            init_selectors();
        }
        player_elements.time_total.html(sec_to_str(activeItem.time));
        var song = $('<a/>', {
            href: '/track/' + activeItem.id,
            text: activeItem.song
        }).click(function () {
            e.preventDefault();
            load_url($(this).attr('href'), 'player_bar');
        });
        if (activeItem.artist) {
            var artist = $('<a/>', {
                href: '/artist/' + escape(activeItem.artist) + '/1/',
                text: activeItem.artist
            }).click(function (e) {
                e.preventDefault();
                load_url($(this).attr('href'), 'player_bar');
            });
            now_playing = artist.add('<span> - </span>').add(song);
        } else {
            now_playing = song;
        }
        $('#playerFav').remove();
        var fav_link = $('<a href="#" id="playerFav" class="fav_item_' + activeItem.id + '"></a>');
        $('#playerPlay').after(fav_link);
        fav_link.click(function (e) {
            e.preventDefault();
            toggle_favorite('item', activeItem.id);
        });
        if (activeItem.fav == 1) {
            fav_link.addClass('fav-on');
        } else {
            fav_link.addClass('fav-off');
        }
        var blog_link = $('<a/>', {
            "class": 'store read',
            href: activeItem.posturl,
            html: 'Read Post &raquo;'
        }).bind('click', function (e) {
            e.preventDefault();
            load_url($(this).attr('href'), 'player_bar');
        });
        var share_link = $('<div class="share" id="player-share-button"></div>');
        player_elements.nowplaying.empty().append(now_playing.add(share_link).add(blog_link));
        player_elements.links.hide();
        share_link.unbind('click').bind('click', function () {
            player_elements.links.css('left', share_link.offset().left - Math.floor(player_elements.links.width() / 2) + (share_link.width() / 2));
            share_link.css('opacity', 0.5);
            swap_share_links();
        });
        player_elements.links.unbind('mouseout').bind('mouseout', function () {
            window.swap_share_timeout = setTimeout(function () {
                swap_share_links();
            }, 500);
        });
        player_elements.links.unbind('mouseover').bind('mouseover', function () {
            clearInterval(window.swap_share_timeout);
        });
        buy_txt = '';
        if (activeItem.emusic) {
            buy_txt += ' <a onclick="load_url(this.href, \'player_bar\');return false;" class="store emusic" href="' + activeItem.emusic + '?player=1" target="_new">eMusic<span></span></a>';
        }
        if (activeItem.amazon) {
            buy_txt += ' <a onclick="load_url(this.href, \'player_bar\');return false;" class="store amazon" href="' + activeItem.amazon + '?player=1" target="_new">Amazon<span></span></a>';
        }
        if (activeItem.itunes) {
            buy_txt += ' <a onclick="load_url(this.href, \'player_bar\');return false;" class="store itunes" href="' + activeItem.itunes + '?player=1" target="_new">iTunes<span></span></a>';
        }
        var share_els = getShareLinks(activeItem, player_elements.links.find('.content').empty());
        page_playing_txt = '<a href="' + activeList.replace('/#!', '') + '" onclick="load_url(this.href, \'player_bar_prev_elt\');return false;"><img src="/images/songplaying.gif" /></a>';
        player_elements.page.empty().append($(page_playing_txt));
    }
    window.getShareLinks = function (item, target) {
        share_txt = '<a class="twitter-share" target="_new" href="/share.php?s_provider=awesm&share_type=twitter&create_type=hypem-player&url=http%3A%2F%2Fhypem.com%2Fitem%2F' + item.id + '&text=' + encodeURI(item.artist.replace(/&/, '')) + ' - ' + encodeURI(item.song.replace(/&/, '')) + '%20on%20@hypem&via=hypem"></a> ';
        share_txt += '<a class="facebook-share" target="_new" href="/share.php?s_provider=awesm&share_type=facebook&create_type=hypem-player&url=http%3A%2F%2Fhypem.com%2Fitem%2F' + item.id + '&title=' + encodeURI(item.artist.replace(/&/, '')) + ' - ' + encodeURI(item.song.replace(/&/, '')) + '">Share</a> ';
        share_txt += '<!--<a class="tumblr-share" target="_new" href="http://www.tumblr.com/share/audio?externally_hosted_url=http%3A%2F%2Fhypem.com%2Fserve%2Fpublic%2F' + item.id + '&name=' + encodeURI(item.artist.replace(/&/, '')) + ' - ' + encodeURI(item.song.replace(/&/, '')) + '&url=http%3A%2F%2Fhypem.com%2Fitem%2F' + item.id + '" title="Share on Tumblr" style="display:inline-block; text-indent:-9999px; overflow:hidden; width:62px; height:20px; background:url(http://platform.tumblr.com/v1/share_2.png) top left no-repeat transparent;">Share on Tumblr</a>-->';
        target.append($(share_txt));
        target.find('a').each(function (i, el) {
            $(el).bind('click', function () {
                var center = {
                    x: ($(window).width() / 2) - (620 / 2),
                    y: ($(window).height() / 2) - (415 / 2)
                };
                var new_window = window.open($(this).attr('href'), $(this).attr('rel'), 'height=415,width=620,location=false,toolbar=false,menubar=false,screenX=' + center.x + ',screenY=' + center.y + ',left' + center.x + ',top' + center.y);
                if (window.focus) new_window.focus();
                return false;
            });
        });
    }
    window.swap_share_links = function () {
        if (player_elements.links.css('display') == 'none') {
            $('#player-share-button').animate({
                opacity: 0.5
            }, 250);
            player_elements.links.fadeIn(250);
            $('#player-timebar').fadeOut(250);
        } else {
            $('#player-share-button').animate({
                opacity: 1
            }, 250);
            player_elements.links.hide();
            $('#player-timebar').css('display', '');
        }
    }, window.toggle_favorite = function (type, id, gray, skip_prompt) {
        debug("toggle_favorite(" + type + ", " + id + ", " + gray + ", " + skip_prompt + ") called");
        if (is_logged_in == 0 && !get_cookie("dontprompt") && !skip_prompt) {
            show_lightbox('listen', '/inc/lb_signup_info.php?type=' + type + '&val=' + id);
            return false;
        }
        if (type == "query") {
            id_enc = Base64.encode(id);
            id_enc = id_enc.replace(/=+/, '');
        }
        var via_data = '';
        if (trackList[document.location.href]) {
            if (trackList[document.location.href]['page_name'] == "profile" && (trackList[document.location.href]['page_mode'] == "loved" || trackList[document.location.href]['page_mode'] == "history") && logged_in_username != trackList[document.location.href]['profile_user']) {
                via_data = "&via_user=" + trackList[document.location.href]['profile_user'];
            } else if (trackList[document.location.href]['page_name'] == "profile" && trackList[document.location.href]['page_mode'] == "feed" && logged_in_username != trackList[document.location.href]["item_" + id]['via_user']) {
                via_data = "&via_user=" + trackList[document.location.href]["item_" + id]['via_user'];
            }
        }
        var request = $.ajax({
            url: '/inc/user_action.php',
            data: 'act=toggle_favorite&ts=' + get_unix_time() + '&ts_offset=' + (get_unix_time() - initial_ts) + '&initial_ref=' + initial_ref + '&session=' + get_visitorid() + '&type=' + type + '&val=' + encodeURIComponent(id) + via_data + '&current_page_name=' + trackList[document.location.href]['page_name'] + '&current_page_mode=' + trackList[document.location.href]['page_mode'] + '&current_profile_user=' + trackList[document.location.href]['profile_user'] + '&current_url=' + Base64.encode(location.href),
            type: 'post',
            async: true,
            cache: false,
            success: function (response) {
                var response = response || "no response text";
            },
            error: function () {
                alert('We couldn\'t add your favorite! :( One of our systems is unavailable and will return shortly.');
            }
        });
        activeclass = (type == "item") ? "fav-on" : "unfollow";
        inactiveclass = (type == "item") ? "fav-off" : "follow";
        activecolor = '';
        inactivecolor = '#ccc';
        activeclass_favcount = "favcount-on";
        active_favcount_change = 1;
        inactiveclass_favcount = "favcount-off";
        inactive_favcount_change = -1;
        active_text = '<em></em> Unfollow';
        inactive_text = '<em></em> Follow';
        if (type == "query") {
            $(".fav_" + type + "_" + id_enc).each(function (index, elt) {
                if ($(elt).hasClass(activeclass)) {
                    $(elt).removeClass(activeclass).addClass(inactiveclass);
                    $(elt).html(inactive_text);
                } else {
                    $(elt).removeClass(inactiveclass).addClass(activeclass);
                    $(elt).html(active_text);
                    try {
                        pageTracker._trackEvent("Search", "Favorite", trackList[activeList]['page_name']);
                    } catch (err) {
                        debug("GOOGLE ANALYTICS ERROR: " + err);
                    }
                }
            });
        } else {
            $('.fav_' + type + "_" + id).each(function (index, elt) {
                if ($(elt).hasClass(activeclass)) {
                    $(elt).removeClass(activeclass).addClass(inactiveclass);
                    if (type != 'item') {
                        $(elt).html(inactive_text);
                    }
                } else {
                    $(elt).removeClass(inactiveclass).addClass(activeclass);
                    if (type != 'item') {
                        $(elt).html(active_text);
                    }
                    try {
                        if (type == "item") {
                            pageTracker._trackEvent("Track", "Favorite", trackList[activeList]['page_name']);
                        } else if (type == "site") {
                            pageTracker._trackEvent("Blog", "Favorite", trackList[activeList]['page_name']);
                        } else if (type == "user") {
                            pageTracker._trackEvent("Friend", "Favorite", trackList[activeList]['page_name']);
                        }
                    } catch (err) {
                        debug("GOOGLE ANALYTICS ERROR: " + err);
                    }
                }
            });
            $('.favcount_' + id).each(function (index, elt) {
                if ($(elt).hasClass(activeclass_favcount)) {
                    $(elt).removeClass(activeclass_favcount).addClass(inactiveclass_favcount);
                    if (type == 'item' && is_int($(elt).html())) {
                        $(elt).html((parseInt($.trim($(elt).html())) + inactive_favcount_change));
                    }
                } else {
                    $(elt).removeClass(inactiveclass_favcount).addClass(activeclass_favcount);
                    if (type == 'item' && is_int($(elt).html())) {
                        $(elt).html((parseInt($.trim($(elt).html())) + active_favcount_change));
                    }
                }
            });
            if (type == "item" && (currentUrl.match(/popular/) || currentUrl.match(/search/) || currentUrl.match(/artist/) || currentUrl.match(/spy/) || currentUrl.match(/list/))) {
                toggle_item_activity('favorites', id, 0, 20);
            }
        }
    }
    window.show_all_tracks = function (elt) {
        var elementList = elt.parent().children();
        elt.parent().children().each(function (index, elti) {
            if ($(elti).attr('class') !== '' && $(elti).attr('class').match(/same-post/)) {
                $(elti).hide();
            }
        });
    }
    window.show_buy = function (pos) {
        $('#buy' + pos).slideDown(250);
        if ($('#meta' + pos)) $('#meta' + pos).hide();
    }
    window.expand_hyped = function (list_parent) {
        var elementList = $('#list_parent').find('li');
        var tmp = '';
        var i = 0;
        $.each(elementList, function (index, elt) {
            if ($(elt).hasClass('hyped-6') && $(elt).css('display', 'none')) {
                $(elt).hide();
            } else if ($(elt).hasClass('hyped-6') && $(elt).css('display', '')) {
                $(elt).show();
            }
        });
        if ($('#mbx').length) {
            if ($('#mbx').html().indexOf("EXPAND") !== -1) {
                $('#mbx').html("HIDE &uarr;");
            } else {
                $('#mbx').html("EXPAND &darr;");
            }
        }
    }
    window.enable_notification_check = function () {
        debug("enable_notification_check() called");
        if (notificationTimeout != 0) {
            debug("Disabling autoupdater in enable_notification_check");
            clearInterval(notificationTimeout);
            notificationTimeout = 0;
        }
        if (trackList[document.location.href] !== undefined) {
            if (is_spy_page()) {
                if (trackList[document.location.href][0].ts !== undefined) {
                    debug("Enabling autoupdater for: " + document.location.href);
                    notificationTimeout = window.setInterval(check_notification, 2000);
                }
                $('#spy-status').html('LIVE');
                $('#spy-status').removeClass().addClass('spy-live');
            } else {
                setTimeout(function () {
                    if (trackList[document.location.href][0].ts !== undefined) {
                        debug("Enabling autoupdater for: " + document.location.href);
                        notificationTimeout = window.setInterval(check_notification, 180000);
                    }
                }, 180000);
            }
        } else {
            return false;
        }
    }
    window.check_notification = function () {
        if (is_spy_page()) {
            var request = $.ajax({
                url: '/inc/serve_track_notification.php',
                data: 'page=' + escape(document.location.href) + '&latest=' + trackList[document.location.href][0].ts + '&ts=' + get_unix_time(),
                type: 'get',
                async: true,
                success: function (response) {
                    $(response).insertBefore($('.section-track')[0]);
                    if ($($('div.section-track').get(1)).attr('class').match(/even/)) {
                        $($('.section-track').get(0)).removeClass('even').addClass('odd');
                    }
                }
            });
        } else {}
    }
    window.disable_notification_check = function () {
        if (notificationTimeout !== 0) {
            debug("Disabling autoupdater in disable_notification_check");
            clearInterval(notificationTimeout);
            notificationTimeout = 0;
            if (is_spy_page()) {
                $('#spy-status').html('PAUSED');
                $('#spy-status').removeClass().addClass('spy-paused');
            }
            return true;
        } else {
            return false;
        }
    }
    window.check_section_updated = function (page_name, latest) {
        debug('check_section_updated(' + page_name + ',' + latest + ') called');
        var request = $.ajax({
            url: '/inc/serve_track_notification.php',
            data: 'page_name=' + escape(page_name) + '&latest=' + latest + '&ts=' + get_unix_time(),
            type: 'get',
            dataType: 'json',
            async: true,
            success: function (response) {
                if (response.count > 0) {
                    if (response.page_name == "feed") {
                        jQuery('#submenu li.my-feed').append("<span class='count'>" + response.count + "</span>");
                    }
                }
            }
        });
    }
    window.enable_playback_check = function () {
        if (playback_event_timeout !== 0) {
            debug("playback_check already running!");
            return false;
        } else {
            playback_event_timeout = window.setInterval(playback_check, 5000);
            return true;
        }
    }

    function playback_check() {
        debug('playback_check() called');
        if (activeList.search(/\/radio\/live$/) != -1) {
            return false;
        }
        if (playerStatus == "PLAYING") {
            debug("playback_check: " + player_position + "/" + player_duration);
            var via_data = '';
            if (trackList[activeList]['page_name'] == "profile" && (trackList[activeList]['page_mode'] == "loved" || trackList[activeList]['page_mode'] == "history") && logged_in_username != trackList[activeList]['profile_user']) {
                via_data = "&via_user=" + trackList[activeList]['profile_user'];
            }
            if (playback_event_count == 0 && player_position >= 30 && player_position < 45) {
                try {
                    pageTracker._trackEvent("Track", "Play_30", trackList[activeList]['page_name']);
                } catch (err) {
                    debug("GOOGLE ANALYTICS ERROR: " + err);
                }
                var request = $.ajax({
                    url: '/inc/user_action.php',
                    data: 'act=log_action&type=listen&ts=' + get_unix_time() + '&ts_offset=' + (get_unix_time() - initial_ts) + '&initial_ref=' + initial_ref + '&session=' + get_visitorid() + '&val=' + activeItem.id + '&pos=' + player_position + '&rem=' + (player_duration - player_position) + '&vendor=' + playerDisplayed + '&playback_manual=' + playback_manual + via_data + '&current_page_name=' + trackList[activeList]['page_name'] + '&current_page_mode=' + trackList[activeList]['page_mode'] + '&current_url=' + Base64.encode(activeList),
                    type: 'get',
                    success: function (response) {
                        if (response == 'NOPLAYS') {
                            playback_allowed = 0;
                        } else if (response == 'BUYPROMPT') {
                            trackList[activeList][currentTrack].buy_prompt = 1;
                        }
                    }
                });
                playback_event_count++;
            } else if (playback_event_count == 1 && (player_position / player_duration) > (2 / 3)) {
                try {
                    pageTracker._trackEvent("Track", "Play_Full", trackList[activeList]['page_name']);
                } catch (err) {
                    debug("GOOGLE ANALYTICS ERROR: " + err);
                }
                var request = $.ajax({
                    url: '/inc/user_action.php',
                    data: 'act=log_action&type=listen&ts=' + get_unix_time() + '&ts_offset=' + (get_unix_time() - initial_ts) + '&initial_ref=' + initial_ref + '&session=' + get_visitorid() + '&val=' + activeItem.id + '&pos=' + player_position + '&rem=' + (player_duration - player_position) + '&vendor=' + playerDisplayed + '&playback_manual=' + playback_manual + via_data + '&current_page_name=' + trackList[activeList]['page_name'] + '&current_page_mode=' + trackList[activeList]['page_mode'] + '&current_url=' + Base64.encode(activeList),
                    type: 'get'
                });
                playback_event_count++;
            }
        }
    }
    window.disable_playback_check = function () {
        if (playback_event_timeout != undefined) {
            clearInterval(playback_event_timeout);
            playback_event_timeout = 0;
            return true;
        } else {
            return false;
        }
    }
    window.share_item = function (item) {
        var share_button = $('#section-track-' + item + ' .share');
        var offset = share_button.offset();
        window.share_links_el = $('#share-links');
        offset.left -= (share_links_el.width() / 2) - (share_button.width() / 2);
        offset.top -= 7;
        var share_els = getShareLinks(trackList[document.location.href]['item_' + item], share_links_el.find('.content').empty());
        share_links_el.css(offset).show();
        share_links_el.unbind('mouseout').bind('mouseout', function () {
            window.swap_item_share_timeout = setTimeout(function () {
                share_links_el.fadeOut(250);
            }, 500);
        });
        share_links_el.unbind('mouseover').bind('mouseover', function () {
            clearInterval(window.swap_item_share_timeout);
        });
    }
    window.toggle_item_activity = function (type, itemid, skip, limit) {
        debug.call(this, 'toggle_item_activity', arguments);
        var elt_name;
        if (type == 'favorites' || type == "favorites-friends") {
            elt_name = 'favcountlist';
        } else if (type == 'reposts') {
            elt_name = 'reposts';
        }
        var act_info = $('#section-track-' + itemid + ' .act_info')
        if (act_info.css('display') !== "none" && $(act_info.children()[0]).hasClass(elt_name)) {
            act_info.slideUp(300);
        } else if (act_info.html() !== '' && $(act_info.children()[0]).hasClass(elt_name)) {
            act_info.slideDown(300);
        } else {
            load_item_activity(type, itemid, skip, limit);
        }
    }
    window.update_item_activity = function (type, itemid, skip, limit) {
        debug.call(this, 'update_item_activity', arguments);
        load_item_activity(type, itemid, skip, limit);
    }
    window.load_item_activity = function (type, id, skip, limit) {
        debug.call(this, 'load_item_activity', arguments);
        var extra_args;
        var target_elt;
        if (type == 'reposts') {
            extra_args = {
                "skip_post": trackList[document.location.href]['item_' + id].postid
            };
        }
        if (type == 'tweets') {
            target_elt = $('#section-track-' + id + ' .act_info_tweets');
        } else {
            target_elt = $('#section-track-' + id + ' .act_info');
        }
        var request = $.ajax({
            url: '/inc/serve_activity_info.php',
            data: 'type=' + type + '&id=' + id + '&skip=' + skip + '&limit=' + limit + '&extra_args=' + JSON.stringify(extra_args) + '&ts=' + get_unix_time(),
            type: 'get',
            async: true,
            success: function (response) {
                target_elt.html(response);
                target_elt.show();
            }
        });
    }
    window.toggle_item_graph = function (id, force) {
        debug('toggle_item_graph() called');
        var target_elt = $('#flot-' + id + '-container');
        if (!$('#flot-' + id).length && !force) {
            load_item_graph(id);
            debug('a');
        } else {
            if (target_elt.css('display') == 'none') {
                target_elt.slideDown(200);
            } else {
                target_elt.slideUp(200);
            }
        }
    }
    window.load_item_graph = function (id) {
        var target_elt = $('#flot-' + id + '-container');
        var request = $.ajax({
            url: '/inc/serve_track_graph.php',
            data: '&id=' + id,
            type: 'get',
            async: true,
            success: function (response) {
                target_elt.html(response);
                target_elt.show();
            }
        });
    }
    window.show_sidebar_info = function (uid, method, section) {
        var element = $('#' + section);
        if (element.css('display') != "none") {
            element.slideUp(300, function () {
                element.hide();
            });
            $('#show_' + method).html('Show all &darr;');
        } else {
            var request = $.ajax({
                url: '../inc/serve_sidebar_profile_items.php',
                data: 'uid=' + uid + '&method=' + method,
                type: 'get',
                async: true,
                success: function (response) {
                    element.html(response);
                    $('#loading_' + method).hide();
                    $('#show_' + method).html('Collapse &uarr;');
                    element.slideDown(200);
                }
            });
            $('#loading_' + method).show();
        }
    }
    window.set_nav_item_active = function (eltid) {
        var eltm = $('#' + eltid);
        var menu = $('#menu-item-username');
        $.each($("li.active"), function (index, elt) {
            $(elt).removeClass('active');
        });
        if (menu.length) {
            menu.removeClass('active');
        }
        if (eltm.length) {
            eltm.addClass('active');
        }
    }
    window.setup_player_bar = function () {
        if (typeof (player_elements) == "undefined") {
            init_selectors();
        }
        var playableCount = 0;
        if (typeof (trackList[document.location.href]) != 'undefined') {
            for (var i = 0; typeof (trackList[document.location.href][i]) != 'undefined'; i++) {
                if (trackList[document.location.href][i].type != '') {
                    playableCount++;
                }
            }
        }
        var window_el = $(window);
        if (player_elements.container.hasClass('mobile')) {
            window_el.bind('scroll', function () {
                player_elements.container.css('top', window.pageYOffset + window.innerHeight - 34);
            });
        }
        debug("setup_player_bar() detected " + playableCount + " tracks on this page: " + document.location.href);
        var end = /\/[0-9]+$/g;
        var same_page = activeList.replace(end, '') == document.location.href.replace(end, '');
        if (playerStatus != "PLAYING" && playerStatus != "PAUSED") {
            if (trackList[document.location.href] === undefined || trackList[document.location.href].length === 0 || playableCount == 0) {
                if ($('#player').length) {
                    $('#player').css('visibility', 'hidden');
                }
                hide_player_bar();
                return false;
            } else {
                show_player_bar();
            }
            if (activeList != document.location.href) {
                trackList[activeList] = Array();
                activeList = document.location.href;
            }
            if (trackList[document.location.href][0].type != '') {
                currentTrack = 0;
            } else {
                for (var i = 0; trackList[document.location.href].length > i; i++) {
                    if (trackList[document.location.href][i].type != '') {
                        currentTrack = i;
                        break;
                    }
                }
            }
            activeItem = trackList[activeList][currentTrack];
            set_now_playing_info(currentTrack);
            player_elements.volume_ctrl.width(player_volume + "%");
            document.title = trackList[activeList].title;
            player_elements.page.css('visibility', 'hidden');
            if (trackList[activeList].length == 1) {
                player_elements.prev.removeClass("playerPrev-active").addClass("playerPrev-inactive").removeClass("playerNext-active").addClass("playerNext-inactive");
            }
        } else if (playerStatus == "PLAYING" && same_page) {
            if (trackList[activeList][currentTrack].id == trackList[document.location.href][currentTrack].id && !is_shuffle_page()) {
                set_track_bg(trackList[activeList][currentTrack].id, "green");
                if ($('#play_track_' + currentTrack)) {
                    $('#play_track_' + currentTrack).attr('class', "pause");
                }
            } else if (!is_spy_page()) {
                for (var i = 0; typeof (trackList[document.location.href][i]) != 'undefined'; i++) {
                    if (trackList[document.location.href][i].id == trackList[activeList][currentTrack].id) {
                        set_track_bg(i, "green");
                        if ($('#play_track_' + i).length) {
                            $('#play_track_' + i).attr('class', 'pause');
                        }
                    }
                }
            }
            if ($('#player').length) {
                $('#player-main-button').attr('class', "pause");
            }
            player_elements.play = $('#playerPlay')
            if (player_elements.play.length) {
                player_elements.play.attr('class', "pause");
            }
            player_elements.page.css('visibility', "hidden");
        } else if (playerStatus == "PLAYING" && !same_page) {
            player_elements.page.css('visibility', 'visible');
            if (trackList[document.location.href] === undefined || trackList[document.location.href].length === 0 || playableCount == 0) {
                if ($('#player').length) {
                    $('#player').css('visibility', 'hidden');
                }
            }
        } else {
            if (trackList[document.location.href] === undefined || trackList[document.location.href].length === 0 || playableCount == 0) {
                if ($('#player').length) {
                    $('#player').css('visibility', "hidden");
                }
            }
        }
        player_elements.player_inner.show();
    }
    window.hide_player_bar = function () {
        debug('hide_player_bar() called');
        if (typeof (player_elements) == "undefined") {
            init_selectors();
        }
        player_elements.player_controls.addClass("disabled");
    }
    window.show_player_bar = function () {
        debug('show_player_bar() called');
        if (typeof (player_elements) == "undefined") {
            init_selectors();
        }
        player_elements.player_controls.removeClass("disabled");
    }
    window.blog_search = function (auto) {
        clearInterval(window.autosearch_blogs);
        if ((auto && $('#blog-directory-search').val().length >= 3) || !auto) {
            var request = $.ajax({
                url: '/inc/serve_sites.php',
                data: 'q=' + $('#blog-directory-search').val(),
                type: 'get',
                async: true,
                success: function (response) {
                    $('#directory-blogs').html(response);
                }
            });
        }
    }
    window.infinite_blog_scroll = function (pages, expand_el, GET) {
        window.blog_scroll = {
            infinite: false,
            pages: pages,
            page: 1,
            loading: false,
            GET: GET,
            complete: false
        };
        var w_el = $(window);
        var dir_el = $('#directory');
        expand_el.bind('click', function (e) {
            window.blog_scroll.infinite = true;
            loadNextBlogPage(true);
            $(this).remove();
        });
        w_el.bind('scroll', function (e) {
            var offset = $('#directory-blogs-inject').offset();
            if (offset) {
                var point = offset.top - (320 * 2);
                if (w_el.scrollTop() >= point) {
                    loadNextBlogPage();
                }
            }
            fadeInImages();
        });

        function fadeInImages(force) {
            if (typeof force == 'undefined') force = false;
            $('.fadein').each(function (i, el) {
                var el = $(el);
                var offset = el.offset();
                if (force || w_el.scrollTop() + w_el.height() >= offset.top) {
                    el.removeClass('fadein');
                    el.hide().fadeIn(500);
                }
            });
        }

        function loadNextBlogPage(force) {
            if (typeof force == 'undefined') force = false;
            if (!window.blog_scroll.loading && window.blog_scroll.infinite && !window.blog_scroll.complete) {
                window.blog_scroll.loading = true;
                window.blog_scroll.GET.page++;
                $('#directory-blogs-inject').before($('<img/>', {
                    src: '/images/ajax-loader-directory.gif',
                    id: 'ajax-loader'
                }));
                $.ajax({
                    url: '/inc/serve_sites.php',
                    data: window.blog_scroll.GET,
                    aync: true,
                    type: 'get',
                    dataType: 'html',
                    success: function (response) {
                        if (response == '') {
                            window.blog_scroll.complete = true;
                        }
                        $('#directory-blogs-inject').before(response);
                        $('#ajax-loader').remove();
                        window.blog_scroll.loading = false;
                        $('.fadein').each(function (i, el) {
                            $(el).hide();
                        });
                        fadeInImages(force);
                    }
                });
            }
        }
    }
    window.insert_next_page = function (callback) {
        debug("insert_next_page() called");
        var current_page = trackList[document.location.href].page_num;
        url = trackList[document.location.href].page_next;
        url = url.replace(/\?ax=1&frag=1&ts=\d+/, '');
        url = url.replace(/\&/, '?');
        url = url.replace(/^#!?/, '');
        if (!skip_update_page_contents && (trackList[document.location.href].loaded_page_next != url || url.match(/shuffle/i))) {
            trackList[document.location.href].loaded_page_next = url;
            debug("insert_next_page() going to process " + url);
            $('#player-loading').show();
            if (is_html5_history_compat()) {
                skip_update_page_contents = 1;
                trackList[document.location.protocol + "//" + document.location.host + url] = trackList[document.location.href];
                trackList[document.location.host] = null;
                history.replaceState(null, null, document.location.protocol + '//' + document.location.host + url);
                if (playerStatus == "PLAYING") {
                    activeList = document.location.href;
                }
            }

            function post_update_page_contents() {
                $('#player-loading').hide();
                rewrite_links();
            }
            page_updater = $.ajax({
                url: url,
                data: 'ax=1&frag=1&ts=' + get_unix_time(),
                dataType: 'html',
                type: 'get',
                async: true,
                success: function (content) {
                    $('#content-wrapper').append(content);
                    post_update_page_contents();
                    if (typeof callback == 'function') {
                        callback(content);
                    } else {
                        debug('insert_next_page callback not a function');
                    }
                    if (trackList[document.location.href].page_next == url && !infinite_page_scroll_settings.hit_end && !(trackList[document.location.href].page_name == 'profile' && trackList[document.location.href].page_mode == 'shuffle')) {
                        insert_end_content();
                        disable_infinite_page_scroll();
                        infinite_page_scroll_settings.hit_end = true;
                    }
                    r = setTimeout(function () {
                        skip_update_page_contents = 0;
                    }, 500);
                },
                error: function () {
                    $('#player-loading').hide();
                    setup_player_bar();
                }
            });
        } else {
            debug(" * ignored due to timing or end of pages");
        }
    }
    window.insert_end_content = function () {
        debug('insert_end_content() called');
        page_updater = $.ajax({
            url: '/inc/serve_end_of_infinity.php',
            dataType: 'html',
            type: 'get',
            async: true,
            success: function (content) {
                $('#content-wrapper').append(content);
            }
        });
    }
    window.enable_infinite_page_scroll = function () {
        debug("enable_infinite_page_scroll() called");
        var w_el = $(window);
        more_el = $('#infinite-tracks-button');
        if (more_el.length) {
            infinite_page_scroll_settings.view_more_exists = true;
            more_el.bind('click', function () {
                more_el.parent().slideUp(250);
                infinite_page_scroll_settings.view_more_clicked = true;
                insert_next_page(function () {
                    more_el.hide();
                });
                w_el.bind('scroll', infinite_page_scroll);
                return false;
            });
        } else {
            w_el.bind('scroll', infinite_page_scroll);
        }
    }
    window.disable_infinite_page_scroll = function () {
        debug("disable_infinite_page_scroll() called");
        var w_el = $(window);
        w_el.unbind('scroll', infinite_page_scroll);
        infinite_page_scroll_settings.view_more_clicked = false;
        infinite_page_scroll_settings.view_more_exists = false;
        infinite_page_scroll_settings.hit_end = false;
    }
    window.infinite_page_scroll_settings = {
        jump_ahead: 200,
        view_more_exists: false,
        view_more_clicked: false,
        hit_end: false
    };
    window.infinite_page_scroll = function (e) {
        var cw = $('#content-wrapper');
        var offset = cw.offset();
        if (offset) {
            var point = offset.top + cw.height();
            if ($(window).scrollTop() + window.innerHeight + infinite_page_scroll_settings.jump_ahead >= point) {
                insert_next_page();
            }
        }
    }
    window.blog_search_keyup = function (e) {
        clearInterval(window.autosearch_blogs);
        window.autosearch_blogs = setTimeout(function () {
            blog_search(true);
        }, 500);
    }
    window.blog_directory_switch = function (tab) {
        $('#directory-tag-navigation, #directory-search, #directory-tags, #directory-alpha').hide();
        switch (tab) {
        case 'tags':
            $('#directory-tag-navigation').show();
            $('#directory-tags').show();
            break;
        case 'search':
            $('#directory-search').show();
            break;
        case 'alpha':
            $('#directory-alpha').show();
            break;
        }
    }
    window.next_review = function (pos) {
        if (album_r_curr[pos] + 1 > (album_rs[pos].length - 1)) {
            album_r_curr[pos] = 0;
            show_review(pos);
        } else if (album_r_curr[pos] === 0 || album_r_curr[pos] === undefined) {
            album_r_curr[pos] = 1;
            show_review(pos);
        } else {
            album_r_curr[pos]++;
            show_review(pos);
        }
    }
    window.prev_review = function (pos) {
        if (album_r_curr[pos] - 1 < 0 || album_r_curr[pos] === undefined) {
            album_r_curr[pos] = album_rs[pos].length - 1;
            show_review(pos);
        } else {
            album_r_curr[pos]--;
            show_review(pos);
        }
    }
    window.show_review = function (pos) {
        $('#album-review' + pos).hide()
        $('#album-review-text' + pos).html(album_rs[pos][album_r_curr[pos]].rbody);
        $('#album-review' + pos).fadeIn(500);
    }
    window.load_rdio = function (pos, c_id) {
        new_iframe = $('<iframe/>', {
            id: "rdio_" + pos,
            width: "360",
            height: "360",
            scrolling: "no",
            frameborder: "no",
            src: 'http://rd.io/i/' + c_id + '?maxwidth=360&maxheight=360&autoplay=1&linkshare_id=DkrYZ0xe5n0&linkshare_subid=&linkshare_offerid=221756.1&linkshare_type=10&linkshare_tmpid=8599'
        });
        $('#album-player' + pos).append(new_iframe);
        $('#album-art' + pos).hide();
        $('#album-player-link' + pos).hide();
    }
    window.updateUrl = function (value) {
        if ($('#username_url').length) {
            $('#username_url').html(value);
        }
    }
    window.watchInput = function (el, options) {
        var duration = options.duration || 1000;
        var callback = (typeof options.callback == 'function') ? options.callback : function () {
                debug('No callback defined for ' + el.attr('id') + '_interval');
            };
        var bind = (typeof options.onblur != 'undefined') ? 'blur' : 'keyup';
        if (bind == 'blur') duration = 0;
        el.bind(bind, function () {
            clearInterval(window[el.attr('id') + '_interval']);
            window[el.attr('id') + '_interval'] = setTimeout(function () {
                if (typeof options.url != 'undefined') {
                    $.ajax({
                        url: '/inc/user_action.php',
                        async: true,
                        type: 'post',
                        data: options.url + encodeURIComponent(el.val()),
                        dataType: 'json',
                        success: function (response) {
                            callback(response);
                        }
                    });
                } else {
                    callback();
                }
            }, duration);
        });
    }
    window.process_lightbox_input = function (form_name) {
        debug("process_lightbox_input(" + form_name + ") called");
        var form_data = $("#defaultform").serialize();
        var extra_data = "";
        if (form_name == "create_account" && $('#form_type').val() == "normal") {
            var recaptcha_challenge;
            var recaptcha_response;
            if (typeof Recaptcha != 'undefined') {
                recaptcha_challenge = Recaptcha.get_challenge();
                recaptcha_response = Recaptcha.get_response();
            }
            extra_data = '&recaptcha_challenge=' + recaptcha_challenge + '&recaptcha_response=' + recaptcha_response;
        }

        function on_response_failure(response, form_name) {
            debug("process_lightbox_input->on_response_failure(%o, %o, %o) called", arguments[0], arguments[1], arguments[2]);
            if (!response || !response.error_msg) {
                $("#formmsg").html("Sorry! :(<br/>An unexpected error occurred: '" + response + "'<br/>Please <a href='/contact'>write to us</a> if this persists.");
            } else {
                $('#formmsg').html(response.error_msg);
            }
            $("#formmsg").css('color', "red");
            $("#formmsg").show();
            $("#submitlogin").attr('disabled', false);
            if (form_name == "login") {
                $("#submitlogin").val("Sign in");
            } else if (form_name == "forgot") {
                $("#submitlogin").val("Reset Password");
            } else if (form_name == "create_account") {
                $("#submitlogin").val("Create Account");
            } else if (form_name == "account" || form_name == "sharing" || form_name == "change_password" || form_name == "change_username" || form_name == "change_email") {
                $("#submitlogin").val("Save");
            }
        }

        function on_response_success(response, form_name) {
            debug("process_lightbox_input->on_response_success(%o, %o, %o) called", arguments[0], arguments[1], arguments[2]);
            if (response.status === "ok") {
                if (form_name == "login" || form_name == "create_account") {
                    if ($('#form_type').val() == "mobile") {
                        document.location.href = '/special/mobile_signup_success';
                    }
                    if (typeof ($('#favorite_type').val()) != "undefined" && typeof ($('#favorite_value').val()) != "undefined") {
                        if (is_ssl_parent()) {
                            post_login($('#favorite_type').val(), $('#favorite_value').val());
                        } else {
                            document.location.href = 'http://' + document.location.host + '/inc/lb_login.php?post_login=1&type=' + $('#favorite_type').val() + '&val=' + $('#favorite_value').val();
                        }
                    } else {
                        if (is_ssl_parent()) {
                            post_login();
                        } else {
                            document.location.href = 'http://' + document.location.host + '/inc/lb_login.php?post_login=1';
                        }
                    }
                } else if (form_name == "forgot" || form_name == "account" || form_name == "sharing" || form_name == "change_password" || form_name == "change_username" || form_name == "change_email") {
                    if (form_name == "account") {
                        $('#user_avatar').hide();
                    }
                    $("#defaultform").hide();
                    $("#formmsg").css('color', "green");
                    $("#formmsg").html(response.msg);
                    $("#formmsg").show();
                }
            } else if (response.status === "error") {
                on_response_failure(response, form_name);
            } else {
                on_response_failure(response, form_name);
            }
        }
        var request = $.ajax({
            url: '/inc/user_action.php',
            data: 'act=' + form_name + '&session=' + get_visitorid() + '&' + form_data + extra_data,
            type: 'post',
            dataType: 'json',
            success: function (data, response_txt, xhr_obj) {
                on_response_success(data, form_name);
            },
            error: function (xhr_obj, error_txt, http_error) {
                on_response_failure(error_txt, form_name);
            }
        });
        return false;
    }
    window.process_user_action = function (action_type) {
        debug("process_user_action(" + action_type + ") called");

        function on_response_failure(response, action_type) {
            debug("process_lightbox_input->on_response_failure(%o, %o, %o) called", arguments[0], arguments[1], arguments[2]);
            if (!response || !response.error_msg) {
                if (action_type == "user_logout") {
                    alert("We encountered an error logging you out.  It was: '" + response + "'.  Please try again later");
                } else {
                    $("#formmsg").html("Sorry! :(<br/>An unexpected error occurred: '" + response + "'<br/>Please <a href='/contact'>write to us</a> if this persists.");
                }
            } else {
                if (action_type == "user_logout") {
                    alert(response.error_msg);
                } else {
                    $('#formmsg').html(response.error_msg);
                }
            }
            $("#formmsg").css('color', "red");
            $("#formmsg").show();
            $("#submitlogin").attr('disabled', false);
        }

        function on_response_success(response, action_type) {
            debug("process_lightbox_input->on_response_success(%o, %o, %o) called", arguments[0], arguments[1], arguments[2]);
            if (response.status === "ok") {
                if (action_type == "user_logout") {
                    load_user_menu();
                    update_page_contents();
                } else {
                    $("#defaultform").hide();
                    $("#formmsg").css('color', "green");
                    $("#formmsg").html(response.msg);
                    $("#formmsg").show();
                }
            } else if (response.status === "error") {
                on_response_failure(response, action_type);
            } else {
                on_response_failure(response, action_type);
            }
        }
        var request = $.ajax({
            url: '/inc/user_action.php',
            data: 'act=' + action_type + '&session=' + get_visitorid(),
            type: 'post',
            dataType: 'json',
            success: function (data, response_txt, xhr_obj) {
                on_response_success(data, action_type);
            },
            error: function (xhr_obj, error_txt, http_error) {
                on_response_failure(error_txt, action_type);
            }
        });
        return false;
    }
    window.post_login = function (type, id) {
        debug("post_login(" + type + "," + id + ") called");
        load_user_menu();
        update_page_contents();
        Lightbox.hideBox();
        if (type && id && type != "listen") {
            setTimeout(function () {
                toggle_favorite(type, id);
            }, 1500);
        }
    }
    window.post_logout = function () {
        debug("post_logout() called");
        load_user_menu();
    }
    window.post_username_change = function () {
        debug("post_username_change() called");
        load_url('/popular');
        load_user_menu();
        Lightbox.hideBox();
    }
    window.cancel_iframe_dialog = function (redir_to) {
        debug('cancel_iframe_dialog(' + redir_to + ') called');
        if (!/lb_/.test(document.location.href)) {
            if (typeof (redir_to) != 'undefined' && redir_to != '') {
                load_url(redir_to);
            } else if (document.location.href.match(/forgot=1/)) {
                load_url("/");
            } else {
                update_page_contents();
            }
            load_user_menu();
            Lightbox.hideBox();
        } else {
            document.location.href = 'http://' + document.location.host + '/inc/lb_cancel.php';
        }
    }
    window.lightbox_close_handler = function (lightbox_url) {
        debug('lightbox_close_handler() called');
        if (lightbox_url.match(/lb_account/)) {
            load_user_menu();
            if (trackList[document.location.href] && trackList[document.location.href]['page_name'] == 'profile') {
                update_page_contents();
            }
        }
        Lightbox.hideBox();
    }
    window.is_ssl_parent = function () {
        if (!/lb_/.test(document.location)) {
            return true;
        } else {
            return false;
        }
    }
    window.switch_lightbox = function (type, url, reload) {
        debug('lightbox_close_handler(' + type + ',' + url + ') called');
        if (is_ssl_parent()) {
            show_lightbox(type, url);
        } else if (!is_ssl_parent() && reload) {
            document.location.href = 'http://' + document.location.host + '/inc/lb_cancel.php?switch_lightbox=1&type=' + type + '&url=' + url;
        } else {
            document.location.href = url + "?ts=" + get_unix_time();
        }
    }
    window.load_url_from_lightbox = function (url) {
        if (is_ssl_parent()) {
            load_url(url);
            cancel_iframe_dialog();
        } else {
            document.location.href = 'http://' + document.location.host + '/inc/lb_cancel.php?redir_to=' + url;
        }
    }
    window.show_lightbox = function (type, url, arg1) {
        if (!$('#box').length) {
            Lightbox.init();
        }
        if (typeof (url) === "undefined") {
            url = false;
        }
        if (type == 'login') {
            if (url) {
                Lightbox.showBoxByAJAX(url, 370, 270);
            } else {
                Lightbox.showBoxByAJAX('/inc/lb_login.php', 370, 270);
            }
        } else if (type == 'signup') {
            if (url) {
                Lightbox.showBoxByAJAX(url, 330, 540);
            } else {
                Lightbox.showBoxByAJAX('/inc/lb_signup.php', 330, 540);
            }
        } else if (type == 'listen') {
            if (url) {
                Lightbox.showBoxByAJAX(url, 360, 210);
            } else {
                Lightbox.showBoxByAJAX('/inc/lb_signup_info.php?type=listen&val=' + trackList[activeList][currentTrack].id, 360, 210);
            }
        } else if (type == 'confirm_new') {
            if (url) {
                Lightbox.showBoxByAJAX(url, 360, 210);
            } else {
                Lightbox.showBoxByAJAX('/inc/lb_confirm_new.php', 360, 210);
            }
        } else if (type == 'forgot') {
            Lightbox.showBoxByAJAX('/inc/lb_forgot.php', 370, 180);
        } else if (type == 'reset') {
            Lightbox.showBoxByAJAX(url, 350, 250);
        } else if (type == 'change_password') {
            Lightbox.showBoxByAJAX('/inc/lb_change_pw.php', 340, 520);
        } else if (type == 'change_username') {
            Lightbox.showBoxByAJAX('/inc/lb_change_username.php', 340, 520);
        } else if (type == 'change_email') {
            Lightbox.showBoxByAJAX('/inc/lb_change_email.php', 340, 520);
        } else if (type == 'account') {
            Lightbox.showBoxByAJAX('/inc/lb_account.php', 340, 575);
        } else if (type == 'sharing') {
            Lightbox.showBoxByAJAX('/inc/lb_sharing.php', 340, 520);
        } else if (type == 'postcard') {
            if (url) {
                Lightbox.showBoxByAJAX(url, 700, 450);
            }
        }
        return false;
    }
    window.checkPw = function () {
        if ($("#user_password_confirmation").val() !== $("#user_password").val() || $("#user_password").val() == '' || $("#user_password_confirmation").val() == '') {
            $("#nomatch").css('display', "inline");
            $("#submitlogin").attr('disabled', true);
            return 1;
        } else {
            $("#nomatch").css('display', "none");
            $("#submitlogin").attr('disabled', false);
            return false;
        }
    }
    window.display_twitter_score = function () {
        $('#twitter_score_calculate').val('Calculating...');
        $('#twitter_score_name').attr('disabled', true);
        $('#twitter_score_calculate').attr('disabled', true);
        var request = $.ajax({
            url: '/inc/user_action.php',
            data: 'act=check_twitter_score&session=' + get_visitorid() + '&arg=' + escape($('#twitter_score_name').val()),
            type: 'get',
            dataType: 'json',
            success: function (response) {
                resp_data = response;
                if (resp_data != '') {
                    $('#twitter_score_dyn').html('<b>' + Math.round(resp_data.tweet_points) + ' points</b> will be added to a song\'s score when <a href="http://twitter.com/' + resp_data.twitter_name + '">' + resp_data.twitter_name_long + '</a> links to it on Twitter.');
                    $('#twitter_score_dyn').show();
                    $('#twitter_score_calculate').val('Calculate!');
                    $('#twitter_score_calculate').attr('disabled', false);
                    $('#twitter_score_name').attr('disabled', false);
                } else {
                    $('#twitter_score_dyn').html('<b>N/A</b> points / tweet');
                    $('#twitter_score_dyn').show();
                    $('#twitter_score_calculate').val('Calculate!');
                    $('#twitter_score_calculate').attr('disabled', false);
                    $('#twitter_score_name').attr('disabled', false);
                }
            },
            error: function (transport) {
                $('#twitter_score_dyn').html('<b>N/A</b> points / tweet');
                $('#twitter_score_dyn').show();
                $('#twitter_score_calculate').val('Calculate!');
                $('#twitter_score_calculate').attr('disabled', false);
                $('#twitter_score_name').attr('disabled', false);
            }
        });
    }
    window.UploadToS3 = function () {
        if ($('#picture_upload').val()) {
            $("#s3_upload_form").attr('target', "fileframe");
            $("#s3_upload_form").attr('action', "/inc/img_upload.php");
            $('#current_img_elt').hide();
            $("#img_frame").show();
            $("#s3_upload_form").submit();
        } else {
            return false;
        }
    }
    window.contact_show_tips = function () {
        if ($('#contact_subject').val() === "All blog questions") {
            var request = $.ajax({
                url: 'inc/serve_all_blogs_listbox.php',
                success: function (response) {
                    $('#blogs_list').html(response);
                }
            });
            $('#blog_name').show();
            $('#blogs_list').html('<option>Please wait...</option>');
            $('#contact_tips').html('<p><strong>Recommending a new blog?</strong><br />Use the <a href=\"/add\">add blog form</a>; otherwise, pick the blog you are contacting us about below.  If tracks you posted are not appearing correctly, please also provide the link to the missing/incorrect post.</p>');
            $('#contact_tips').show();
        } else if ($('#contact_subject').val() === "Ad feedback") {
            $('#contact_tips').html("<p><strong>Reporting an ad problem?</strong><br />Clicking on the ad and giving us the URL it sends you to, will help greatly!</p>");
            $('#contact_tips').show();
            $('#blog_name').hide();
        } else if ($('#contact_subject').val() === "Business stuff") {
            $('#contact_tips').html("<p>Please be brief about how you see us working together.</p>");
            $('#contact_tips').show();
            $('#blog_name').hide();
        } else if ($('#contact_subject').val() === "Report bugs") {
            $('#contact_tips').html('<p>Please include what were you doing when the error happened, what browser you use, etc.<br /><br />If you use AdBlock, Flashblock, NoScript, or any other ad-blocking tools or Internet security software, please try disabling them if you encounter problems using the site</p>');
            $('#contact_tips').show();
            $('#blog_name').hide();
        } else if ($('#contact_subject').val() === "Everything else") {
            $('#contact_tips').html("<p><a href='http://hypem.com/about'>Our FAQ</a> can answer most common questions, <a href='http://hypem.com/about'>please look there first</a> - you'll get your answer faster!</p>");
            $('#contact_tips').show();
            $('#blog_name').hide();
        } else {
            $('#blog_name').hide();
            $('#contact_tips').html('');
            $('#contact_tips').show();
        }
    }
    window.render_popular_sparklines = function () {
        var head = jQuery('head');
        if (head.length) {
            if (!jQuery('#js_flot').length) {
                var elem = document.createElement('script');
                elem.src = '/js/flot/jquery.flot.js';
                elem.async = false;
                elem.type = "text/javascript";
                elem.id = 'js_jquery-flot';
                var scpt = document.getElementsByTagName('script')[0];
                scpt.parentNode.insertBefore(elem, scpt);
            }
            if (!jQuery('#js_sparkline').length) {
                var elem = document.createElement('script');
                elem.src = '/js/jquery.sparkline.min.js';
                elem.async = false;
                elem.type = "text/javascript";
                elem.id = 'js_sparkline';
                var scpt = document.getElementsByTagName('script')[0];
                scpt.parentNode.insertBefore(elem, scpt);
            }
        }
        Array.max = function (array) {
            return Math.max.apply(Math, array);
        };
        var maxes = new Array();
        $('.sparkdiv').each(function (i) {
            var id = $(this).attr('rel');
            if (day_points[id] != undefined) {
                maxes[i] = Array.max(day_points[id]);
            }
        });
        var max_page = Array.max(maxes);
        $('.sparkdiv').each(function (i) {
            var id = $(this).attr('rel');
            if (day_points[id] != undefined) {
                $("#sparkline-" + id).sparkline(day_points[id], {
                    type: 'bar',
                    barWidth: 3,
                    barColor: '#63902F',
                    barSpacing: 1,
                    width: '43px',
                    height: '20px',
                    spotColor: false,
                    minSpotColor: false,
                    maxSpotColor: false,
                    chartRangeMax: max_page
                });
            }
        });
    }
    window.positionMap = function (artist) {
        if (ua_info.is_mobile) {
            return false;
        }
        map.setZoom(4);
        loc = positions[artist.toLowerCase()];
        map.setCenter(new google.maps.LatLng((loc.lat() + 9), loc.lng()));
    }
    window.init_soundmanager = function () {
        if (document.location.href.match(/lb_/)) {
            return false;
        }
        if (typeof (soundManager) != 'undefined') {
            if (ua_info.is_ios || ua_info.is_android || ua_info.is_ie9 || ua_info.is_playbook) {
                soundManager.useHTML5Audio = true;
            } else if (!ua_info.is_android_old_flash) {
                soundManager.flashVersion = 9;
            }
            soundManager.debugMode = false;
            soundManager.url = '/';
            soundManager.flashLoadTimeout = 5000;
            debug("soundManager settings loaded OK");
            soundManager.onready(function () {
                if (soundManager.supported()) {
                    isReady = 1;
                    var group = $('#player-progress-loading, #player-progress-playing, #player-volume-outer');
                    group.bind('mousedown', sm_start_drag).bind('mouseup', sm_end_drag);
                    player_elements.volume_mute.bind('mouseup', sm_toggle_mute);
                } else {
                    debug("soundManager reported it's unsupported?");
                }
            });
        } else {
            debug("Attempted to set some soundManager logic, but SM not loaded yet");
            setTimeout(function () {
                init_soundmanager();
            }, 1000);
        }
    }
    $(document).ready(function () {
        debug("document.ready has fired");
        init_selectors();
        load_user_menu();
        init_soundmanager();
        $(window).mousemove(function (e) {
            if (!$('#takeover-link').length) return false;
            var p = {
                x: e.pageX - $('#header-inner').offset().left,
                y: e.pageY - $('#header-inner').offset().top
            };
            if ((p.x < 0 || p.x > 1056) & p.y > 74) {
                p.x += $('#header-inner').offset().left;
                p.y += $('#header-inner').offset().top;
            } else {
                p = {
                    x: 0,
                    y: 0
                };
            }
            $('#takeover-link').css({
                'left': p.x - 5,
                'top': p.y - 5
            });
        });
    });
})(jQuery);
