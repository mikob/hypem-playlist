var _gaq = _gaq || [];
window.displayList = {};
window.playList = {};
var currentTrack = 0;
var currentPlayerObj = [];
var currentUrl;
var prevUrl;
var activeList = null;
var is_logged_in;
var logged_in_username;
var playback_allowed;
var user_country;
var user_region;
var static_http_server;
var initial_ts;
var initial_ref = '';
var dragging_position = false;
var dragging_x;
var isReady = 0;
var playerStatus = '';
var playerDisplayed = 'normal';
var playback_event_timeout = 0;
var playback_event_count = 0;
var playback_manual = 0;
var player_position;
var player_duration;
var player_volume = 50;
var page_updater;
var skip_update_page_contents = 0;
var notificationTimeout = 0;
var spyTimeout = 0;
var updateSpy = 1;
var album_rs = [];
var album_r_curr = [];
var master_ord;
var master_passback;
var ad_feedback_code;
var ad_feedback_position;
var ua_info = {};
var vice_regions = ["DE"];
var toast_prompt_shown = false;
var first_load = 1;
(function($) {
    window.get_cookie = function(name) {
        var start = document.cookie.indexOf(name + "=");
        var len = start + name.length + 1;
        if ((!start) && (name !== document.cookie.substring(0, name.length))) {
            return null;
        }
        if (start === -1) {
            return null;
        }
        var end = document.cookie.indexOf(";", len);
        if (end === -1) {
            end = document.cookie.length;
        }
        return unescape(document.cookie.substring(len, end));
    }
    window.set_cookie = function(name, value, expires, path, domain, secure) {
        var today = new Date();
        today.setTime(today.getTime());
        if (expires) {
            expires = expires * 1000 * 60 * 60 * 24;
        }
        var expires_date = new Date(today.getTime() + (expires));
        document.cookie = name + "=" + escape(value) + ((expires) ? ";expires=" + expires_date.toGMTString() : '') + ((path) ? ";path=" + path : '') + ((domain) ? ";domain=" + domain : '') + ((secure) ? ";secure" : '');
    }
    window.debug = function(q, w, e, r, t, y) {
        if (!is_dev()) {
            return false;
        }
        if (typeof console != 'undefined') {
            try {
                console.log.apply(console, arguments);
            } catch (err) {
                console.log(q, w, e, r, t, y);
            }
        }
        return false;
    };
    window.is_dev = function() {
        if (document.location.href.match(/dev.hypem.com/i)) {
            return true;
        } else if (document.location.href.match(/debug_console/i)) {
            return true;
        } else {
            return false;
        }
    }
    window.set_ad_vars = function() {
        log("set_ad_vars() called");
        window.master_ord = parseInt(Math.random() * 10000000000000000);
        var pv_addr;
        if (document.location.href.search(/\/#!?\//) != -1) {
            pv_addr = document.location.hash;
            pv_addr = pv_addr.replace(/#!?\//, '/');
        } else {
            pv_addr = document.location.pathname;
        }
        ;
        if (pv_addr == '/' || pv_addr.match(/latest/) || pv_addr.match(/subscription/)) {
            window.master_passback = 'homepage';
        } else if (pv_addr.match(/popular/)) {
            window.master_passback = 'popular';
        } else if (pv_addr.match(/twitter\/popular/)) {
            window.master_passback = 'twitter';
        } else if (pv_addr.match(/radio/)) {
            window.master_passback = 'radioshow';
        } else if (pv_addr.match(/spy/)) {
            window.master_passback = 'spy';
        } else if (pv_addr.match(/about/)) {
            window.master_passback = 'about';
        } else if (pv_addr.match(/contact/)) {
            window.master_passback = 'contact';
        }
        if (typeof takeovers != 'undefined') {
            log("processing skin initialization for " + takeovers.length + " possible takeovers, we are in " + user_country);
            for (var i = 0; i < takeovers.length; i++) {
                var takeover = takeovers[i];
                var offset = 3600 * (takeover.offset || 0);
                var now = server_time + offset;
                if (now < takeover.start || now > takeover.end)
                    continue;
                log("It's " + now + " in " + takeover.country_code + " which is greater than " + takeover.start + ", so that takeover is now live.");
                if (takeover.country_code != user_country || now < takeover.start || now > takeover.end)
                    continue;
                takeover.match = new RegExp(takeover.match);
                takeover.href = '/takeovers/takeover-images.php?' + takeover.id;
                custom_css.push(takeover);
            }
        }
        var head = document.getElementsByTagName('head')[0];
        for (var i = 0; i < custom_css.length; i++) {
            var page = custom_css[i];
            if (typeof (page) != "undefined" && pv_addr.match(page.match)) {
                if (!$('#' + page.id).length) {
                    var css_sheet = document.createElement('link');
                    css_sheet.type = "text/css";
                    css_sheet.rel = "stylesheet";
                    css_sheet.id = page.id;
                    css_sheet.href = page.href;
                    head.appendChild(css_sheet);
                }
                if ($('#takeover-link'))
                    $('#takeover-link').remove();
                if (typeof page.target != 'undefined') {
                    var t = $('<div/>', {id: 'takeover-link'});
                    var a = $('<a/>', {href: page.target,target: '_blank'});
                    var i = $('<img/>', {src: page.pixel + '&cb=' + (+new Date() + Math.round(Math.random() * 1000))});
                    a.append(i);
                    t.append(a);
                    $('body').eq(0).append(t);
                }
            } else if (typeof (page) != "undefined" && !document.location.href.match(page.match) && $('#' + page.id).length) {
                $('#' + page.id).remove();
                if ($('#takeover-link'))
                    $('#takeover-link').remove();
            } else {
            }
        }
    }
    $(window).bind('keypress', function(e) {
        var code;
        if (!e)
            var e = window.event;
        if (e.keyCode)
            code = e.keyCode;
        else if (e.which)
            code = e.which;
        var t_elt = $(e.target) || $(e.srcElement);
        if (!t_elt.is('input') && !t_elt.is('textarea')) {
            if (e.shiftKey || e.ctrlKey || e.altKey || e.metaKey) {
            } else {
                var character = String.fromCharCode(code);
                if (character == 'n' || character == 'j' || character == 'b' || code == '39')
                    nextTrack(this);
                else if (character == 'p' || character == 'k' || character == 'z' || code == '37')
                    prevTrack(this);
                else if (character == 'l' || character == 'h' || character == 'f')
                    toggle_favorite('item', window.displayList['tracks'][currentTrack].id);
                else if (character == 'x' || character == 'c' || character == 'z' || code == '37')
                    togglePlaySimple();
                else if (character == 'v')
                    stopTrack();
            }
        }
    });
    window.init_selectors = function() {
        log("init_selectors called");
        window.player_elements = {time_position: $('#player-time-position'),time_total: $('#player-time-total'),progress_loading: $('#player-progress-loading'),progress_playing: $('#player-progress-playing'),progress_outer: $('#player-progress-outer'),volume_outer: $('#player-volume-outer'),volume_ctrl: $('#player-volume-ctrl'),volume_mute: $('#player-volume-mute'),prev: $('#playerPrev'),next: $('#playerNext'),page: $('#player-page'),links: $('#player-links'),nowplaying: $('#player-nowplaying'),player_controls: $('#player-controls'),container: $('#player-container'),player_inner: $('#player-inner')}
    }
    window.load_user_menu = function() {
        log("load_user_menu() called");
        $.ajax({url: '/inc/header_menu',cache: false,type: 'get',async: true,success: function(response) {
                $('#filter').html(response);
            }});
    }
    window.page_url_state_init = function() {
        log("page_url_state_init() called, logged_in: " + is_logged_in + " logged_in_username: " + logged_in_username);
        initial_ts = get_unix_time();
        inital_ref = document.referrer;
        load_ad_code(user_country);
        if (document.location.href.search('\\?forgot=1&email=') != -1) {
            show_lightbox("reset", '/inc/lb_reset_pw.php?key=' + getQueryVariable('key') + '&email=' + getQueryVariable('email'));
        }
        if (is_html5_history_compat()) {
            if (document.location.href.match(/com\/?(\?.+)?$/) && is_logged_in) {
                update_page_contents();
            } else if (document.location.href.match(/com\/?(\?.+)?$/) && !is_logged_in) {
                update_page_contents();
            } else if (document.location.href.match(/\/#!?/)) {
                url = document.location.href.replace(/\/#!?\//, '/');
                history.replaceState(null, null, url);
                first_load = 0;
                update_page_contents();
            } else {
                update_page_contents();
            }
        } else {
            if (document.location.href.match(/\/#!\/$/) && is_logged_in) {
                update_page_contents();
            } else if (!document.location.href.match(/\/#/) && !document.location.href.match(/ax=1/) && !document.location.href.match(/_escaped_fragment_/)) {
                url = document.location.pathname + document.location.search;
                url = url.replace(/\?ax=1/, '');
                url = "/#!" + url;
                top.location = url;
                update_page_contents();
            } else if (!document.location.href.match(/\/#!/) && !document.location.href.match(/ax=1/) && document.location.href.match(/\/#\//)) {
                url = document.location.href.replace(/\/#\//, '/#!/');
                top.location = url;
                update_page_contents();
            } else {
                update_page_contents();
            }
        }
        if (!document.location.href.match(/_escaped_fragment_/)) {
            currentUrl = get_current_rel_url();
            prevUrl = currentUrl;
            setInterval('check_hash_change()', 300);
        }
    }
    window.load_ad_code = function(country) {
        if (ads_enabled) {
            log('load_ad_code(' + country + ') called');
        } else {
            log('load_ad_code(' + country + ') called but ads are disabled');
            return false;
        }
        if (country == "AU") {
            var elem = document.createElement('script');
            elem.src = "http://secure-au.imrworldwide.com/v60.js";
            elem.id = "au_nielsen";
            elem.async = false;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
        } else if (jQuery.inArray(user_country, vice_regions) !== -1) {
        } else if (country != "GB" && country != "IE" && country != "AU" && (ua_info.is_mobile || ads_enabled)) {
        }
    }
    window.draw_ads = function() {
        var elt;
        if (ads_enabled && !ua_info.is_mobile && !ua_info.is_ios) {
            log("[" + get_unix_time() + "] draw_ads() called");
        } else {
            log("draw_ads() called, but ads are disabled or mobile");
            return false;
        }
        elt = $('.ad-leaderboard')[0];
        if (typeof (elt) == "undefined" || $(elt).children('iframe').length) {
            elt = $('.ad-leaderboard-midpage')[($('.ad-leaderboard-midpage').length - 1)];
        }
        if (elt && !$(elt).children('iframe').length) {
            var id_iframe = $(elt).attr('id') + "-frame";
            if (jQuery.inArray(user_country, vice_regions) !== -1) {
                new_iframe = $('<iframe/>', {'id': id_iframe,'class': "ad-leaderboard-frame",scrolling: "no",frameborder: "no",src: "/ads/vice-728x90-orig.html?pos=" + id_iframe + "&ord=" + master_ord + dfp_extras_var_passthru() + "&passback=" + dfp_extras_passback(user_country)});
                $(elt).append(new_iframe);
            } else if (user_country == "GB" || user_country == "IE") {
                new_iframe = $('<iframe/>', {'id': id_iframe,'class': "ad-leaderboard-frame",scrolling: "no",frameborder: "no",src: "/ads/uk-728x90.html?pos=" + id_iframe + "&ord=" + master_ord + dfp_extras_var_passthru() + "&passback=" + dfp_extras_passback(user_country)});
                $(elt).append(new_iframe);
            } else if (user_country == "AU") {
                new_iframe = $('<iframe/>', {'id': id_iframe,'class': "ad-leaderboard-frame",scrolling: "no",frameborder: "no",src: "/ads/au2-728x90.html?pos=" + id_iframe + "&ord=" + master_ord + dfp_extras_var_passthru() + "&passback=" + dfp_extras_passback(user_country)});
                $(elt).append(new_iframe);
            } else {
                new_iframe = $('<iframe/>', {'id': id_iframe,'class': "ad-leaderboard-frame",scrolling: "no",frameborder: "no",src: "/ads/dyn-dart-728x90.html?pos=" + id_iframe + "&ord=" + master_ord + dfp_extras_var_passthru() + "&passback=" + dfp_extras_passback(user_country)});
                $(elt).append(new_iframe);
            }
        }
        elt = $('.ad-rectangleA')[0];
        if (typeof (elt) == "undefined" || $(elt).children('iframe').length) {
            elt = $('.ad-rectangleA-midpage')[($('.ad-rectangleA-midpage').length - 1)];
        }
        if (elt && !$(elt).children('iframe').length) {
            var id_iframe = $(elt).attr('id') + "-frame";
            if (jQuery.inArray(user_country, vice_regions) !== -1) {
                new_iframe = $('<iframe/>', {'id': id_iframe,'class': "ad-rectangle-frame",scrolling: "no",frameborder: "no",src: "/ads/vice-300x250-1-orig.html?pos=" + id_iframe + "&ord=" + master_ord + dfp_extras_var_passthru() + "&passback=" + dfp_extras_passback(user_country)});
                $(elt).append(new_iframe);
            } else if (user_country == "GB" || user_country == "IE") {
                new_iframe = $('<iframe/>', {'id': id_iframe,'class': "ad-rectangle-frame",scrolling: "no",frameborder: "no",src: "/ads/uk-300x250-1.html?pos=" + id_iframe + "&ord=" + master_ord + dfp_extras_var_passthru() + "&passback=" + dfp_extras_passback(user_country)});
                $(elt).append(new_iframe);
            } else if (user_country == "AU") {
                new_iframe = $('<iframe/>', {'id': id_iframe,'class': "ad-rectangle-frame",scrolling: "no",frameborder: "no",src: "/ads/au2-300x250-1.html?pos=" + id_iframe + "&ord=" + master_ord + dfp_extras_var_passthru() + "&passback=" + dfp_extras_passback(user_country)});
                $(elt).append(new_iframe);
            } else {
                new_iframe = $('<iframe/>', {'id': id_iframe,'class': "ad-rectangle-frame",scrolling: "no",frameborder: "no",src: "/ads/dyn-dart-300x250-1.html?pos=" + id_iframe + "&ord=" + master_ord + dfp_extras_var_passthru() + "&passback=" + dfp_extras_passback(user_country)});
                $(elt).append(new_iframe);
            }
        }
        elt = $('.ad-rectangleB')[0];
        if (typeof (elt) == "undefined" || $(elt).children('iframe').length) {
            elt = $('.ad-rectangleB-midpage')[($('.ad-rectangleB-midpage').length - 1)];
        }
        if (elt && !$(elt).children('iframe').length) {
            var id_iframe = $(elt).attr('id') + "-frame";
            if (jQuery.inArray(user_country, vice_regions) !== -1) {
                new_iframe = $('<iframe/>', {'id': id_iframe,'class': "ad-rectangle-frame",scrolling: "no",frameborder: "no",src: "/ads/vice-300x250-2-orig.html?pos=" + id_iframe + "&ord=" + master_ord + dfp_extras_var_passthru() + "&passback=" + dfp_extras_passback(user_country)});
                $(elt).append(new_iframe);
            } else if (user_country == "GB" || user_country == "IE") {
                new_iframe = $('<iframe/>', {'id': id_iframe,'class': "ad-rectangle-frame",scrolling: "no",frameborder: "no",src: "/ads/uk-300x250-2.html?pos=" + id_iframe + "&ord=" + master_ord + dfp_extras_var_passthru() + "&passback=" + dfp_extras_passback(user_country)});
                $(elt).append(new_iframe);
            } else if (user_country == "AU") {
                new_iframe = $('<iframe/>', {'id': id_iframe,'class': "ad-rectangle-frame",scrolling: "no",frameborder: "no",src: "/ads/au2-300x250-2.html?pos=" + id_iframe + "&ord=" + master_ord + dfp_extras_var_passthru() + "&passback=" + dfp_extras_passback(user_country)});
                $(elt).append(new_iframe);
            } else {
                new_iframe = $('<iframe/>', {'id': id_iframe,'class': "ad-rectangle-frame",scrolling: "no",frameborder: "no",src: "/ads/dyn-dart-300x250-2.html?pos=" + id_iframe + "&ord=" + master_ord + dfp_extras_var_passthru() + "&passback=" + dfp_extras_passback(user_country)});
                $(elt).append(new_iframe);
            }
        }
    }
    window.load_list = function(list) {
    }
    window.update_page_contents = function() {
        log('update_page_contents() called');
        if (document.location.pathname == '/' || is_html5_history_compat()) {
            if (document.location.href.search(/_escaped_fragment_/) == -1) {
                $('#player-loading').show();
                if (is_html5_history_compat()) {
                    url = document.location.pathname + document.location.search;
                } else {
                    url = document.location.hash;
                }
                if (url == '') {
                    url = '/';
                } else if (url == '/play_queue') {
                    queueItems = get_site_queue();
                    url = '/set/' + queueItems.join(',');
                }
                url = url.replace(/\?ax=1/, '');
                url = url.replace(/^#!?/, '');
                log("update_page_contents() about to load " + url);
                function post_update_page_contents() {
                    log("post_update_page_contents() called on " + url);
                    $('#player-loading').hide();
                    window.displayList = JSON.parse($('#displayList-data').remove().html()) || {};
                    window.displayList['url'] = get_current_rel_url();
                    if (typeof window.playList !== 'undefined') {
                        window.playList.gc = true;
                    }
                    if (prevUrl != currentUrl && !currentUrl.match(/^\/blogs\//)) {
                        $(window).scrollTop(0);
                    }
                    prevUrl = currentUrl;
                    if (document.location.href.match(/track\//) && typeof (FB) != "undefined" && typeof (FB.XFBML) != "undefined") {
                        FB.XFBML.parse();
                    }
                    if (window.displayList['show_favorites_friends']) {
                        $('.section-track').each(function(i, el) {
                            load_item_activity('favorites-friends', $(el), 0, 20);
                        });
                    }
                    if (document.location.href.match(/popular\/?\d?/)) {
                        setTimeout(function() {
                            render_popular_sparklines();
                        }, 1000);
                    }
                    set_ad_vars();
                    draw_ads();
                    setup_player_bar();
                    show_push_message();
                    ga_pageview();
                    if (window.displayList.page_name === "spy") {
                        enable_spy_check();
                        set_nav_item_active('menu-item-spy');
                    }
                    if (ua_info.is_mobile) {
                        $('#content-left').insertBefore('#content-right');
                    }
                    if (user_country != 'GB' && user_country != 'IE' && user_country != 'AU') {
                        update_buzz_page_state();
                    }
                }
                if (first_load && is_html5_history_compat() && !url.match(/^\/(\?.*)?$/) && !url.match(/latest/)) {
                    log("update_page_contents loading skipped as page already inside");
                    post_update_page_contents();
                } else {
                    page_updater = $.ajax({url: url,data: 'ax=1&ts=' + get_unix_time(),dataType: 'html',type: 'get',async: true,success: function(content) {
                            $('#content-wrapper').html(content);
                            post_update_page_contents();
                        },error: function() {
                            $('#player-loading').hide();
                            setup_player_bar();
                            _gaq.push(['_trackEvent', 'Error', 'post_update_page_contents']);
                        }});
                }
                first_load = 0;
                if (playerStatus != "PLAYING" && getQueryVariable("autoplay")) {
                    setTimeout(function() {
                        togglePlay();
                    }, 1000);
                }
            }
        }
    }
    window.ga_pageview = function() {
        log("ga_pageview() called");
        var pv_addr;
        if (document.location.href.search(/\/#!?\//) != -1) {
            pv_addr = document.location.hash;
            pv_addr = pv_addr.replace(/#!?\//, '/');
        } else {
            pv_addr = document.location.pathname;
        }
        if (typeof acct_type !== "undefined") {
            _gaq.push(['_setVar', 'acct_' + acct_type]);
        }
        _gaq.push(['_trackPageview', pv_addr]);
        try {
            if (typeof (__qc) != 'undefined' && __qc.qpixelsent && __qc.qpixelsent.length) {
                __qc.qpixelsent = [];
            }
            if (user_country == 'GB' || user_country == 'IE') {
                _qevents.push([{qacct: "p-f66q2dQu9xpU2"}, {qacct: "p-70RhgKmunzjBs",labels: "Music"}]);
            } else {
                _qevents.push({qacct: "p-f66q2dQu9xpU2"});
            }
        } catch (err) {
            log("QUANTCAST ERROR: " + err);
            _gaq.push(['_trackEvent', 'Error', 'quantcast', err]);
        }
        if (jQuery.inArray(user_country, vice_regions) && typeof _comscore != "undefined") {
            _comscore.push({c1: "2",c2: "8568956"});
        }
        if (user_country == "AU") {
            update_nielsen(pv_addr);
        }
    }
    window.show_push_message = function() {
        log("show_push_message() called");
        var message_intro = $('<div id="message-intro" />');
        if (typeof (displayList) !== "undefined" && typeof (displayList.page_name) !== "undefined" && !displayList.page_name.match("/(email_manage|contact|iphoneapp|cmj2011|local|radio_show|sxsw2012_|gball2012|HypeTV)/") && (!is_logged_in || (is_logged_in && displayList.page_name == "profile" && (ua_info.is_mobile || ua_info.is_ios)))) {
            if (ua_info.is_android) {
                message_intro.html('<p><strong>Blog-powered radio? Favorites in your pocket?<br/><strong>Get <a href="http://app.net/uberhype">uberHype</a> or <a href="http://bit.ly/PbCc7k">Hype!</a> (unofficial apps) in the Android Market</strong><br /></p>');
            } else if (ua_info.is_ios) {
                message_intro.html('<p><strong style="font-size:28px;"><a href="http://click.linksynergy.com/fs-bin/click?id=DkrYZ0xe5n0&subid=&offerid=146261.1&type=10&tmpid=5573&RD_PARM1=http%3A%2F%2Fitunes.apple.com%2Fus%2Fapp%2Fhype-radio%2Fid414315986">Get Hype Machine for iPhone</a></strong><br /><span style="font-size:23px;">Blog-powered radio. Favorites in your pocket.</span><br/></p>');
            } else if (!ua_info.ua.match('/mypad/')) {
                message_intro.html('<p>Every day, thousands of people around the world write about music they love â and it all ends up here. <br/><strong><a onclick="show_lightbox(\'signup\');return false;" href="">Join us</a></strong> and find music worth listening to. Also on the <strong><A href="http://click.linksynergy.com/fs-bin/click?id=DkrYZ0xe5n0&subid=&offerid=146261.1&type=10&tmpid=5573&RD_PARM1=http%3A%2F%2Fitunes.apple.com%2Fus%2Fapp%2Fhype-radio%2Fid414315986">iPhone</a></strong>, <A href="http://bit.ly/PbCc7k"><strong>Android</strong> <span style="font-size:10px">(unoff.)</span></a> &amp; <a href="http://www.windowsphone.com/en-US/apps/3d299cde-5093-e011-986b-78e7d1fa76f8"><strong>Windows Phone</strong> <span style="font-size:10px">(unoff.)</span></a> <br/></p>');
            }
            $('#content').prepend(message_intro);
        }
    }
    window.update_buzz_page_state = function(count) {
        log("update_buzz_page_state() called");
        if (typeof (count) == "undefined") {
            count = 0;
        } 
        else if (count >= 9) {
            return false;
        } 
        else {
            count++;
        }
        var buzz_url;
        try {
            if (is_html5_history_compat()) {
                buzz_url = document.location.pathname + document.location.search;
            } else {
                buzz_url = document.location.hash;
            }
            if (ua_info.is_mobile || ua_info.is_ios) {
                bmQuery.bmLib.geoLocation = "XX";
            } else {
                bmQuery.bmLib.geoLocation = user_country;
            }
            bmQuery.bmLib.pageChange(buzz_url);
        } catch (err) {
            log("bmQuery exception [retrying " + count + "...] : " + err);
            setTimeout(function() {
                update_buzz_page_state(count);
            }, 500);
        }
    }
    window.update_nielsen = function(pv_addr) {
        log("update_nielsen() called");
        try {
            var pvar = {cid: "inthemix",content: "0",server: "secure-au",page_url: "http://hypem.com" + pv_addr};
            var trac = nol_t(pvar);
            trac.record().post();
        } catch (err) {
            log("update_nielsen exception [retrying...] : " + err);
            setTimeout(function() {
                update_nielsen();
            }, 1000);
        }
    }
    window.dfp_extras_var_passthru = function() {
        var appended_str = '';
        var dfpKeyword = getQueryVariable("dfpKeyword");
        if (dfpKeyword != '' && typeof (dfpKeyword) != 'undefined') {
            appended_str += "&dfpKeyword=" + dfpKeyword;
        }
        if (document.location.href.match(/ceelodistilled/)) {
            appended_str += "&dfpKeyval=campaign:absolut";
        } else if (document.location.href.match(/gball2012/)) {
            appended_str += "&dfpKeyval=page:gball2012";
        } else if (document.location.href.match(/popular\/remix/)) {
            appended_str += "&dfpKeyval=page:trident";
        } else if (document.location.href.match(/prius/)) {
            appended_str += "&dfpKeyval=page:priusc2012";
        } else if (document.location.href.match(/singer-songwriter/)) {
            appended_str += "&dfpKeyval=campaign:singersongwriter";
        } else {
            var dfpKeyval = getQueryVariable("dfpKeyval");
            if (dfpKeyval != '' && typeof (dfpKeyval) != 'undefined') {
                appended_str += "&dfpKeyval=" + dfpKeyval;
            }
        }
        return appended_str;
    }
    window.dfp_extras_passback = function(country) {
        var page_name, page_num, passback;
        if (window.displayList && window.displayList && window.displayList['page_name']) {
            page_name = window.displayList['page_name'];
        }
        if (window.displayList && window.displayList && window.displayList['page_num']) {
            page_num = window.displayList['page_num'];
        }
        if (country == 'AU' || country == 'GB' || country == 'IE') {
            if (page_name == "latest" && page_num == 1) {
                passback = "homepage";
            } else if ((page_name == "index" || page_name == "profile") && (page_num > 1 && page_num < 7)) {
                passback = "page" + page_num;
            } else if (page_name == "popular") {
                passback = "popular";
            } else if (page_name == "tweets_popular") {
                passback = "twitter";
            } else if (page_name == "radio_show") {
                passback = "radioshow";
            } else if (page_name == "spy") {
                passback = "spy";
            } else if (page_name == "about") {
                passback = "about";
            } else if (page_name == "contact") {
                passback = "contact";
            } else {
                passback = "ros";
            }
        } else {
            if ((page_name == "index" || page_name == "profile") && page_num == 1) {
                passback = "homepage";
            } else if ((page_name == "index" || page_name == "profile") && (page_num > 1 && page_num < 7)) {
                passback = "page" + page_num;
            } else if (page_name == "radio_show") {
                passback = "radioshow";
            } else if (page_name == "umf2011") {
                passback = "umf2011";
            } else {
                passback = "ros";
            }
        }
        return passback;
    }
    window.resize_ad_element = function(pos, size) {
        log("resize_ad_element(" + pos + ", " + size + ") called");
        $('#' + pos).css({'width': size.width,'height': size.height});
        $('#' + pos).parent().css({'width': size.width,'height': size.height});
    }
    window.load_url = function(url, action_src, e) {
        log("load_url(" + url + ", " + action_src + ") called");
        disable_infinite_page_scroll();
        if (page_updater && page_updater.readyState != 4) {
            log("ignored click to " + url);
            return false;
        }
        if (action_src === '' && action_src != 'undefined') {
            action_src = window.playList.page_name;
        }
        if (url.match(/amazon/i)) {
            _gaq.push(['_trackEvent', 'Affiliate', 'Amazon', action_src]);
        } else if (url.match(/emusic/i)) {
            _gaq.push(['_trackEvent', 'Affiliate', 'Emusic', action_src]);
        } else if (url.match(/itunes/i)) {
            _gaq.push(['_trackEvent', 'Affiliate', 'iTunes', action_src]);
        } else if (url.match(/songkick/i)) {
            _gaq.push(['_trackEvent', 'Affiliate', 'Songkick', action_src]);
        } else if ((url.match(/track\//i) || url.match(/item\//i) || url.match(/track\//i)) && url.match(/rec_ref=/)) {
            _gaq.push(['_trackEvent', 'Track', 'Suggestion', action_src]);
        } else if (url.match(/search\//i) && url.match(/rec_ref=/)) {
            _gaq.push(['._trackEvent', 'Search', 'Suggestion', action_src]);
        } else if (url.match(/search\//i)) {
            _gaq.push(['_trackEvent', 'Search', 'Normal', action_src]);
        } else if (url.match(/artist\//i)) {
            _gaq.push(['_trackEvent', 'Search', 'Artist_click', action_src]);
        } else if (url.match(/track\//i) || url.match(/item\//i) || url.match(/song\//i)) {
            _gaq.push(['_trackEvent', 'Track', 'Click', action_src]);
        } else if (!url.match(/hypem.com/i)) {
            _gaq.push(['_trackEvent', 'Blog', 'Post_click', action_src]);
        }
        if ((url.indexOf('/') === 0 || url.search(/^https?:\/\/.*?hypem\.com\//) != -1) && url.search('/go/') == -1 && url.search('/fast-forward\/launch') == -1 && url.search('/download') == -1 && url.search('/admin/') == -1 && url.search('/share') == -1 && url.search('/feed\/.+?xml$') == -1 && url.search('/playlist') == -1 && url.search(/http:\/\/blog\./) == -1 && url.search(/http:\/\/stream\./) == -1 && url.search(/http:\/\/merch\./) == -1 && url.search(/api\/twitter/) == -1 && url.search('/labs\/ff/') == -1 && (typeof (e) == "undefined" || !e.metaKey)) {
            if (document.location.href.search(/_escaped_fragment_/) == -1) {
                url = url.replace(/^https?:\/\/.+?\//, '/');
                url = url.replace(/[\?&]ax=1/, '');
                if (url.match(/%2F/i) || url.match(/%3F/i) || url.match(/%252F/i) || url.match(/%2B/i) || url.match(/%25/i) || /MSIE/.test(navigator.userAgent)) {
                } else {
                    try {
                        url = decodeURIComponent(url);
                    } catch (err) {
                    }
                }
                if (is_html5_history_compat()) {
                    currentUrl = url;
                    history.pushState(null, null, document.location.protocol + '//' + document.location.host + url);
                } else {
                    currentUrl = "/#!" + url;
                    top.location.href = "/#!" + url;
                }
                disable_notification_check();
                disable_spy_check();
                update_page_contents();
            } else {
                top.location.href = url;
            }
        } else {
            try {
                if (url.search(/twitter.com\/intent/) != -1) {
                    var center = {x: ($(window).width() / 2) - (550 / 2),y: ($(window).height() / 2) - (460 / 2)};
                    var new_window = window.open(url, 'twitter', 'height=460,width=550,location=false,toolbar=false,menubar=false,screenX=' + center.x + ',screenY=' + center.y + ',left' + center.x + ',top' + center.y);
                    return false;
                    if (window.focus)
                        new_window.focus();
                } else {
                    window.open(url, "_blank", '');
                }
                return false;
            } catch (e) {
                window.open(url, '_blank', 'width=' + window.screen.availWidth + ',height=' + window.screen.availHeight + ',menubar=yes,toolbar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes');
                return false;
            }
        }
        return false;
    }
    window.check_hash_change = function() {
        url = get_current_rel_url();
        if (currentUrl != url) {
            log("detected url change from " + currentUrl + " to " + url);
            currentUrl = url;
            if (skip_update_page_contents) {
                skip_update_page_contents = 0;
                log("ignored page reload event due to autoscroll");
                return false;
            }
            if (page_updater && page_updater.readyState != 4) {
                log("ignored page reload event due to loading in progress");
                return false;
            }
            update_page_contents();
        }
    }
    window.get_current_rel_url = function() {
        url = document.location.href;
        url = url.replace(/^https?:\/\/.+?\//, '/');
        url = url.replace(/[\?&]ax=1/, '');
        url = url.replace(/^#!?/, '');
        if (url.match(/%2F/i) || url.match(/%3F/i) || url.match(/%252F/i) || url.match(/%2B/i) || url.match(/%25/i) || /MSIE/.test(navigator.userAgent)) {
        } else {
            try {
                url = decodeURIComponent(url);
            } catch (err) {
            }
        }
        return url;
    }
    window.get_visitorid = function() {
        var cookie_txt = decodeURIComponent(get_cookie("AUTH"));
        var cookie_arr = cookie_txt.split(":");
        return cookie_arr[1];
    }
    window.hide_notice = function(cookie_key) {
        jQuery('#top-notice').hide();
        jQuery('#player-container').css({top: ''});
        set_cookie(cookie_key, 'true', '30', '/', '', '');
    }
    window.set_site_queue = function(queueItems) {
        set_cookie("play_queue", JSON.stringify(queueItems), '30', '/', '', '');
    }
    window.get_site_queue = function() {
        var data = get_cookie("play_queue");
        return JSON.parse(data);
    }
    window.getQueryVariable = function(variable) {
        var query = window.location.href.substring(window.location.href.indexOf("?") + 1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
    }
    window.load_search = function() {
        var query = $('#q').val();
        if (query && query != "Artist or Track") {
            query = $.trim(query);
            query = urlencode_kinda(query);
            load_url("/search/" + query + "/1/", "search_box");
        } else {
            load_random_search(true);
        }
    }
    window.urlencode_kinda = function(str) {
        if (/MSIE/.test(navigator.userAgent)) {
            str = encodeURIComponent(str);
        } else {
            str = str.replace(/\+/g, '%2B');
            str = str.replace(/\%/g, '%25');
            str = str.replace(/\//g, '%2F');
            str = str.replace(/\?/g, '%3F');
            str = str.replace(/([^\s])&([^\s])/, '$1%2526$2');
        }
        return str;
    }
    window.load_random_search = function(forced) {
        var action_src;
        if (forced) {
            action_src = "search_box";
        } else {
            action_src = "search_shuffe_button";
        }
        $.ajax({url: '/random_search',data: 'rel_only=1',cache: false,type: 'get',async: true,success: function(transport) {
                var response = transport.responseText || "/random_search";
                load_url(response, action_src);
            },error: function(error) {
                load_url('/random_search', action_src);
                _gaq.push(['_trackEvent', 'Error', 'random_search']);
            }});
    }
    window.load_random_track = function() {
        $.ajax({url: '/random',data: 'rel_only=1',cache: false,type: 'get',async: true,success: function(transport) {
                var response = transport.responseText || "/random";
                load_url(response, "item");
            },error: function() {
                _gaq.push(['_trackEvent', 'Error', 'load_random_track']);
                load_url('/random', "item");
            }});
    }
    window.get_unix_time = function() {
        return parseInt(+new Date() / 1000);
    }
    window.is_int = function(value) {
        if ((parseFloat(value) == parseInt(value)) && !isNaN(value)) {
            return true;
        } else {
            return false;
        }
    }
    window.sec_to_str = function(nSec) {
        var min = Math.floor(nSec / 60);
        var sec = nSec - (min * 60);
        return min + ':' + (sec < 10 ? '0' + sec : sec);
    }
    window.getQueryVariable = function(variable) {
        var query = window.location.href.substring(window.location.href.indexOf("?") + 1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable)
                return pair[1];
        }
    }
    window.sm_onload = function() {
        log("sm_onload fired, readystate is " + currentPlayerObj[(currentPlayerObj.length - 1)].readyState + " duration:" + parseInt(currentPlayerObj[(currentPlayerObj.length - 1)].duration / 1000));
        if (this.readyState == 2 || (this.readyState == 3 && !ua_info.is_ios && !ua_info.is_android && (this.bytesLoaded < 50000 || this.duration == 0))) {
            if (this.url.match(/(hypem|archive)/) || this['final']) {
                log("FAILED, giving up reloading, running nextTrack (failed url: " + this.url + ' )');
                nextTrack();
            } else {
                log("FAILED SM, retrying... " + this.url);
                loadTrack(0, 1, 0);
            }
        } else {
            log("sm_onload reports success, setting player_duration if not provided");
            if (typeof (window.playList['tracks'][currentTrack].time) === "undefined") {
                player_duration = parseInt(currentPlayerObj[0].duration / 1000);
                window.playList['tracks'][currentTrack]['time'] = player_duration;
                player_elements.time_total.html(sec_to_str(player_duration));
                log("duration set from loaded data");
            }
            $('#player-loading').hide();
        }
    }
    window.sm_onplay = function() {
        log("sm_onplay fired, readyState: " + this.readyState + ", playState: " + this.playState);
        if (this.sID != ("currentPlayer" + currentTrack)) {
            return false;
        }
        playerStatus = "PLAYING";
        setPlaying();
        document.title = document.title.replace(/\u25FC/, '\u25BA');
    }
    window.sm_onresume = function() {
        playerStatus = "PLAYING";
        log("sm_onresume fired");
        setPlaying();
        document.title = document.title.replace(/\u25FC/, '\u25BA');
    }
    window.setPlaying = function(playing) {
        log('setPlaying called');
        if (typeof playing == 'undefined')
            playing = true;
        player_elements.state = $('.play-state');
        if (player_elements.state.length) {
            if (playing) {
                player_elements.state.removeClass("play").addClass("pause");
            } else {
                player_elements.state.removeClass("pause").addClass("play");
            }
        }
    }
    window.sm_onpause = function() {
        log('on_pause fired');
        if (this.sID != 'currentPlayer' + currentTrack)
            return false;
        playerStatus = 'PAUSED';
        setPlaying(false);
        document.title = document.title.replace(/\u25BA/, '\u25FC');
    }
    window.sm_onfinish = function() {
        log("sm_onfinish fired");
        playerStatus = "COMPLETED";
        stopTrack();
        nextTrack();
    }
    window.sm_onstop = function() {
        log("sm_onstop fired");
        playerStatus = "COMPLETED";
    }
    window.sm_whileplaying = function() {
        if (this.sID != ("currentPlayer" + currentTrack))
            return false;
        if (dragging_position)
            return;
        if (player_position != parseInt(this.position / 1000)) {
            player_position = parseInt(this.position / 1000);
            $('#player-loading').hide();
            if (is_fade_enabled() && (player_duration - player_position) == 20 && playback_allowed) {
                loadTrack(0, 0, 1);
            } else if (is_fade_enabled() && (player_duration - player_position) == 7 && currentPlayerObj[1].bytesLoaded && playback_allowed) {
                beginFadeTransition();
            }
            player_elements.time_position.html(sec_to_str(player_position));
            player_elements.progress_playing.width((Math.round(player_position / player_duration * 100 * 100) / 100) + '%');
        }
    }
    window.sm_whileloading = function() {
        if (this.sID != "currentPlayer" + currentTrack) {
            return false;
        }
        var percent_loaded = (Math.round((this.bytesLoaded / this.bytesTotal) * 100) * 100 / 100) + '%';
        player_elements.progress_loading.animate({width: percent_loaded}, 50);
        if (typeof (window.playList['tracks'][currentTrack].time) === "undefined") {
            player_duration = parseInt(currentPlayerObj[0].duration / 1000);
            player_elements.time_total.html(sec_to_str(parseInt(player_duration)));
        }
    }
    window.sm_start_drag = function(evt) {
        if (!evt)
            var evt = window.event;
        var t_elt = evt.target || evt.srcElement;
        if (t_elt.id.match(/progress/)) {
            dragging_position = true;
            $(window).unbind('mousemove').bind('mousemove', sm_follow_progress_drag).unbind('mouseup').bind('mouseup', sm_end_drag);
        }
        if (t_elt.id.match(/volume/)) {
            dragging_position = true;
            $(window).unbind('mousemove').bind('mousemove', sm_follow_volume_drag).unbind('mouseup').bind('mouseup', sm_end_drag);
        }
        return false;
    }
    window.sm_follow_volume_drag = function(evt) {
        if (!evt)
            var evt = window.event;
        var t_elt = evt.target || evt.srcElement;
        sm_update_volume(evt, t_elt, false);
    }
    window.sm_follow_progress_drag = function(evt) {
        if (!evt)
            var evt = window.event;
        var t_elt = evt.target || evt.srcElement;
        var x = parseInt(evt.clientX);
        var offset = player_elements.progress_loading.offset();
        var nMsecOffset = Math.floor((x - offset.left - 4) / (player_elements.progress_outer.width()) * (window.playList['tracks'][currentTrack].time * 1000));
        if (!isNaN(nMsecOffset))
            nMsecOffset = Math.min(nMsecOffset, currentPlayerObj[0].duration);
        player_position = parseInt(nMsecOffset / 1000);
        player_elements.time_position.html(sec_to_str(player_position));
        player_elements.progress_playing.width((Math.round(player_position / player_duration * 100 * 100) / 100) + '%')
        sm_update_progress(evt, t_elt);
        if (player_position >= player_duration)
            sm_end_drag();
    }
    window.sm_end_drag = function(evt) {
        if (!evt)
            var evt = window.event;
        var t_elt = evt.target || evt.srcElement;
        log('END DRAGGING');
        dragging_position = false;
        $(window).unbind('mousemove').unbind('mouseup');
        if (t_elt.id.match(/progress/)) {
            sm_update_progress(evt, t_elt);
        }
        if (t_elt.id.match(/volume/)) {
            sm_update_volume(evt, t_elt, true);
            var x = parseInt(evt.clientX);
            var offset = player_elements.volume_outer.offset();
            var volume_pc = parseInt(Math.floor((x - offset.left - 2) / (player_elements.volume_outer.width()) * 100));
            $.ajax({url: '/inc/user_action.php',data: 'act=volume&value=' + volume_pc + '&session=' + get_visitorid(),cache: false,type: 'post',async: true});
        }
        return false;
    }
    window.sm_update_volume = function(evt, t_elt, morph) {
        log(t_elt.tagName + "clicked X:" + evt.clientX);
        var x = parseInt(evt.clientX);
        var offset = player_elements.volume_outer.offset();
        var volume_pc = parseInt(Math.floor((x - offset.left - 2) / (player_elements.volume_outer.width()) * 100));
        if (!isNaN(volume_pc))
            volume_pc = Math.min(volume_pc, 100);
        if (!isNaN(volume_pc) && currentPlayerObj[0])
            currentPlayerObj[0].setVolume(volume_pc);
        if (!isNaN(volume_pc))
            player_volume = volume_pc;
        if (morph) {
            player_elements.volume_ctrl.animate({width: volume_pc + '%'}, 50);
        } else {
            player_elements.volume_ctrl.width(volume_pc + '%');
        }
    }
    window.sm_update_progress = function(evt, t_elt) {
        log(t_elt.tagName + " " + t_elt.id + " clicked X:" + evt.clientX);
        var x = parseInt(evt.clientX);
        var offset = player_elements.progress_loading.offset();
        var nMsecOffset = Math.floor((x - offset.left - 4) / (player_elements.progress_outer.width()) * (player_duration * 1000));
        if (!isNaN(nMsecOffset))
            nMsecOffset = Math.min(nMsecOffset, (player_duration * 1000));
        if (!isNaN(nMsecOffset))
            currentPlayerObj[0].setPosition(nMsecOffset);
        player_position = parseInt(nMsecOffset / 1000);
        if (currentPlayerObj[1]) {
            clearInterval(currentPlayerObj[0].fadeIn);
            currentPlayerObj[1].destruct();
            clearInterval(currentPlayerObj[0].fadeOut);
            currentPlayerObj[0].setVolume(player_volume);
        }
    }
    window.sm_toggle_mute = function() {
        if (currentPlayerObj[0] && currentPlayerObj[0].muted) {
            player_elements.volume_mute.removeClass('player-volume-mute-active').addClass('player-volume-mute-inactive');
            player_elements.volume_ctrl.animate({width: player_volume + '%'}, 500);
            currentPlayerObj[0].unmute();
        } else if (currentPlayerObj[0]) {
            player_elements.volume_mute.removeClass('player-volume-mute-inactive').addClass('player-volume-mute-active');
            player_elements.volume_ctrl.animate({width: 0 + '%'}, 500);
            currentPlayerObj[0].mute();
        }
        return false;
    }
    window.beginFadeTransition = function() {
        log("beginFadeTransition() called");
        currentPlayerObj[1].play();
        fadeOutSound(currentPlayerObj[0], -5, 300);
        fadeInSound(currentPlayerObj[1], 5, 300);
    }
    window.fadeInSound = function(soundObj, amount, ms_delay) {
        if (soundObj) {
            var vol = soundObj.volume;
            log("fadeInSound, current: " + vol + ", change: " + amount + ", delay:" + ms_delay)
            if (vol >= player_volume)
                return false;
            soundObj.setVolume(Math.min(player_volume, vol + amount));
            soundObj.fadeIn = setTimeout(function() {
                fadeInSound(soundObj, amount, ms_delay)
            }, ms_delay);
        }
    }
    window.fadeOutSound = function(soundObj, amount, ms_delay) {
        if (soundObj) {
            var vol = soundObj.volume;
            log("fadeOutSound, current: " + vol + ", change: " + amount + ", delay:" + ms_delay)
            if (vol == 0)
                return false;
            soundObj.setVolume(Math.max(0, vol + amount));
            soundObj.fadeOut = setTimeout(function() {
                fadeOutSound(soundObj, amount, ms_delay)
            }, ms_delay);
        }
    }
    window.is_fade_enabled = function() {
        if (currentPlayerObj[0].readyState !== 3) {
            return false;
        }
        if (!ua_info.is_ios && !ua_info.is_android && !ua_info.is_playbook && window.playList['tracks'].length > 1 && !is_spy_page()) {
            return true;
        } else {
            return false;
        }
    }
    window.is_html5_history_compat = function() {
        return !!(window.history && history.pushState);
    }
    window.togglePlayByItemid = function(itemid, evt) {
        log("togglePlayByItemid(" + itemid + ", " + evt + ") called");
        for (var i = 0; i < window.displayList['tracks'].length; i++) {
            log(' checking ' + window.displayList['tracks'][i]['id'] + ' vs ' + itemid);
            if (window.displayList['tracks'][i]['id'] == itemid) {
                togglePlay(i, evt);
                return false;
            }
        }
    }
    window.is_spy_page = function() {
        if (document.location.href.match(/\/spy$/) || document.location.href.match(/\/obsessedspy$/) || document.location.href.match(/\/lovespy$/)) {
            return true;
        } else {
            return false;
        }
    }
    window.is_shuffle_page = function() {
        if (document.location.href.match(/\/shuffle/)) {
            return true;
        } else {
            return false;
        }
    }
    window.togglePlaySimple = function() {
        log('togglePlaySimple called');
        if (playerStatus == 'PAUSED') {
            if (window.playList['tracks'][currentTrack].type == 'normal') {
                currentPlayerObj[0].resume();
            }
        } else if (playerStatus == 'PLAYING') {
            if (window.playList['tracks'][currentTrack].type == 'normal') {
                currentPlayerObj[0].pause();
                if (currentPlayerObj[1]) {
                    currentPlayerObj[1].pause();
                    nextTrack();
                }
            }
        } else if (playerStatus === '') {
            togglePlay();
        }
    }
    window.togglePlay = function(id, evt) {
        log("togglePlay(" + id + ", " + evt + ") called");
        if ((playerStatus == 'PAUSED' || playerStatus == 'PLAYING') && (typeof id === 'undefined' || (id === currentTrack && activeList === get_current_rel_url() && !window.playList.gc))) {
            togglePlaySimple();
        } else {
            if (activeList != get_current_rel_url() || typeof window.playList === 'undefined' || window.playList.gc) {
                log('deleting playList');
                try {
                    delete window.playList;
                } catch (e) {
                    window.playList = undefined;
                }
                log('cloning playList');
                window.playList = jQuery.extend(true, {}, window.displayList);
                activeList = get_current_rel_url();
                player_elements.page.css('visibility', 'hidden');
            }
            stopTrack();
            if (evt && evt.shiftKey) {
                var queueItems = get_site_queue();
                if (typeof queueItems == 'undefined' || queueItems == null) {
                    queueItems = Array();
                }
                for (var i = 0; i < queueItems.length; ) {
                    if (this[i] === window.displayList['tracks'][id].id) {
                        queueItems.splice(i, 1);
                        set_site_queue(queueItems);
                        return false;
                    } else {
                        ++i;
                    }
                }
                if (typeof id == 'undefined')
                    id = 0;
                queueItems.push(window.displayList['tracks'][id].id);
                set_site_queue(queueItems);
            } else {
                if (typeof id == 'undefined')
                    id = 0;
                currentTrack = id;
                playback_manual = 1;
                playTrack();
            }
        }
        return false;
    }
    window.stopTrack = function() {
        log("stopTrack called");
        setPlaying(false);
        if (typeof currentTrack === 'undefined' || typeof window.playList === 'undefined' || typeof window.playList['tracks'] === 'undefined') {
            return false;
        }
        if (activeList == get_current_rel_url()) {
            if (window.playList['tracks'][currentTrack]) {
                $('.section-track').eq(currentTrack).find('.play-ctrl').removeClass('play-state');
                set_track_bg(currentTrack, '');
            }
        }
        disable_playback_check();
        if (currentPlayerObj[0] && currentPlayerObj[0].playState != 0) {
            debug("executing .stop();");
            currentPlayerObj[0].stop();
        }
        document.title = $("<div>" + window.displayList.title + "</div>").text();
    }
    window.playTrack = function(skip_prompts) {
        log("playTrack called");
        if (!playback_allowed) {
            show_lightbox("listen");
            return false;
        } else if (!window.playList || !window.playList['tracks']) {
            return false;
        }
        $('#player-loading').show();
        if (window.playList['tracks'].length > 1) {
            player_elements.prev.removeClass("playerPrev-inactive").addClass("playerPrev-active").removeClass("playerNext-inactive").addClass("playerNext-active");
            playback_event_count = 0;
        }
        playback_event_count = 0;
        enable_playback_check();
        show_player_bar();
        set_now_playing_info(window.playList['tracks'][currentTrack]);
        if (activeList == get_current_rel_url()) {
            $('.section-track').eq(currentTrack).find('.play-ctrl').addClass('play-state');
            log($('.section-track').eq(currentTrack).find('.play-ctrl'));
            setPlaying();
            set_track_bg(currentTrack, "green");
        }
        if (is_spy_page()) {
            disable_spy_check();
            var clicked = $('.section-track').eq(currentTrack);
            var previousAll = clicked.prevAll('.section-track');
            if (previousAll.length > 0) {
                disable_spy_check();
                var idx = clicked.index('.section-track');
                window.displayList['tracks'].splice(0, 0, window.displayList['tracks'].splice(idx, 1)[0]);
                window.playList['tracks'].splice(0, 0, window.playList['tracks'].splice(idx, 1)[0]);
                currentTrack = 0;
                var top = $(previousAll[previousAll.length - 1]);
                var previous = $(previousAll[0]);
                var moveUp = clicked.offset().top - top.offset().top;
                var moveDown = (clicked.offset().top + clicked.outerHeight()) - (previous.offset().top + previous.outerHeight());
                $('html, body').animate({'scrollTop': 0}, 'slow');
                previousAll.animate({'top': moveDown}, 'slow', function() {
                    previousAll.css({'top': 0})
                });
                clicked.addClass('sliding').animate({'top': -moveUp}, 'slow', function() {
                    clicked.removeClass('sliding').css({'top': 0}).insertBefore(top);
                });
            }
            enable_spy_check();
        }
        if (window.playList['tracks'][currentTrack].artist) {
            track_string = window.playList['tracks'][currentTrack].artist + ' - ' + window.playList['tracks'][currentTrack].song;
        } else {
            track_string = window.playList['tracks'][currentTrack].song;
        }
        now_playing_title = "\u25BA " + track_string + ' / The Hype Machine';
        document.title = $("<div>" + now_playing_title + "</div>").text();
        check_toast_permission(function() {
            if (document.webkitHidden) {
                thumb = '//static-ak.hypem.net/thumbs/' + window.playList['tracks'][currentTrack].postid % 10 + '/' + window.playList['tracks'][currentTrack].postid + '.png';
                $.ajax({url: thumb,type: 'HEAD',error: function() {
                        show_toast('//static-ak.hypem.net/images/albumart0.gif', 'Now playing on Hype Machine', track_string);
                    },success: function() {
                        show_toast(thumb, 'Now playing on the Hype Machine', track_string);
                    }});
            }
        });
        loadTrack(0, 0, 0);
    }
    window.loadTrack = function(skip, retry, is_fade) {
        log("loadTrack called skip: " + skip + " retry: " + retry + " is_fade:" + is_fade);
        var autoplay;
        var volume;
        var attempt = 0;
        if (retry) {
            var prev_url_data = {};
            prev_url_data.url = currentPlayerObj[(currentPlayerObj.length - 1)].url;
            prev_url_data.readyState = currentPlayerObj[(currentPlayerObj.length - 1)].readyState;
            prev_url_data.bytesLoaded = currentPlayerObj[(currentPlayerObj.length - 1)].bytesLoaded;
            prev_url_data.duration = currentPlayerObj[(currentPlayerObj.length - 1)].duration;
            attempt = currentPlayerObj[(currentPlayerObj.length - 1)].attempt + 1;
            is_fade = currentPlayerObj[(currentPlayerObj.length - 1)].is_fade;
            log(" * autoconfigured is_fade to: " + is_fade + " based on prev. obj");
            var req_data = 'retry=1&attempt=' + attempt + '&readyState=' + prev_url_data.readyState + '&bytesLoaded=' + prev_url_data.bytesLoaded + '&duration=' + prev_url_data.duration + '&prev_url=' + Base64.encode(prev_url_data.url);
            currentPlayerObj[(currentPlayerObj.length - 1)].destruct();
            tmp = currentPlayerObj.pop();
        } else {
            var req_data = '';
        }
        if (!skip && is_fade) {
            skip = 1;
            var track = currentTrack + skip;
        } else if (skip && is_fade) {
            var track = currentTrack + skip;
        } else if (!skip && !is_fade) {
            skip = 0;
            var track = currentTrack;
        } else {
            var track = currentTrack + skip;
        }
        if (!window.playList['tracks'][track] || (window.playList['tracks'][track].type != 'normal' && window.playList['tracks'][track].type != 'sc_special')) {
            loadTrack(skip + 1, 0, is_fade);
            return false;
        }
        var req_url = '/serve/source/' + window.playList['tracks'][track].id + '/' + window.playList['tracks'][track].key;
        if (!is_fade && currentPlayerObj[1] && currentPlayerObj[1].sID == "currentPlayer" + currentTrack) {
            currentPlayerObj[0].destruct();
            currentPlayerObj.shift();
            if (window.playList['tracks'][currentTrack].time) {
                player_duration = window.playList['tracks'][currentTrack].time;
                player_elements.time_total.html(sec_to_str(player_duration));
            }
            if (playerStatus == "COMPLETED") {
                playerStatus = "PLAYING";
            }
            log("faded track is already loaded... exiting");
            return false;
        } else if (!is_fade) {
            while (currentPlayerObj.length) {
                currentPlayerObj[0].destruct();
                currentPlayerObj.shift();
            }
        }
        if (is_fade) {
            autoplay = false;
            volume = 0;
        } else {
            autoplay = true;
            volume = player_volume;
        }
        var source_data;
        var r = $.ajax({url: req_url,data: req_data,type: 'get',async: false,cache: false,dataType: 'json',error: function() {
                log('playTrack /source/ request FAILED');
                nextTrack();
                _gaq.push(['_trackEvent', 'Error', 'source_json_failure']);
                return false;
            }});
        try {
            response = jQuery.parseJSON(r.responseText);
        } catch (err) {
            log("FAILED to parse JSON data");
            nextTrack();
            return false;
        }
        log("Creating SM object with, autoPlay:" + autoplay + " volume: " + volume);
        try {
            currentPlayerObj.push(soundManager.createSound({id: 'currentPlayer' + track,url: response.url,autoLoad: true,autoPlay: autoplay,onload: sm_onload,onplay: sm_onplay,onresume: sm_onresume,onpause: sm_onpause,whileplaying: sm_whileplaying,whileloading: sm_whileloading,onfinish: sm_onfinish,onstop: sm_onstop,volume: volume}));
            currentPlayerObj[(currentPlayerObj.length - 1)]['final'] = response['final'];
            currentPlayerObj[(currentPlayerObj.length - 1)]['attempt'] = attempt;
            currentPlayerObj[(currentPlayerObj.length - 1)]['is_fade'] = is_fade;
            if (!is_fade) {
                if (typeof (window.playList['tracks'][currentTrack].time) !== "undefined") {
                    player_duration = window.playList['tracks'][currentTrack].time;
                    player_elements.time_total.html(sec_to_str(window.playList['tracks'][currentTrack].time));
                }
            }
        } catch (err) {
            log('wtf, cant create sound! :(', err);
            return false;
        }
        if ($('player-volume-mute').className == 'player-volume-mute-active') {
            currentPlayerObj[0].mute();
        }
    }
    window.nextTrack = function() {
        stopTrack();
        playback_manual = (typeof this != window) ? 1 : 0;
        while ((window.playList['tracks'].length - 1) > currentTrack && typeof window.playList['tracks'][(currentTrack + 1)] != "undefined") {
            if (window.playList['tracks'][(currentTrack + 1)].type != '') {
                currentTrack++;
                log('Current track: ' + currentTrack);
                playTrack();
                return false;
            } else {
                currentTrack++;
            }
        }
        return false;
    }
    window.prevTrack = function() {
        log("PREV from: " + this);
        playback_manual = (this !== window) ? 1 : 0;
        while (currentTrack > 0 && typeof window.playList['tracks'][(currentTrack - 1)] != "undefined") {
            if (window.playList['tracks'][(currentTrack - 1)].type != '') {
                stopTrack();
                currentTrack--;
                playTrack();
                return false;
            } else {
                currentTrack--;
            }
        }
        return false;
    }
    window.set_track_bg = function(i, color) {
        log("set_track_bg(" + i + "," + color + ") called");
        var track_obj = $('.section-track').eq(i);
        if (color == "green" && typeof track_obj != 'undefined') {
            track_obj.addClass('active-playing-green');
            return true;
        } else if (typeof track_obj != 'undefined') {
            track_obj.removeClass("active-playing-green");
            return true;
        } else {
            log("Could not find section-track [" + i + "]");
            return false;
        }
    }
    window.set_now_playing_info = function(item) {
        log("set_now_playing_info() called");
        if (typeof (player_elements) === "undefined") {
            init_selectors();
        }
        player_elements.time_total.html(sec_to_str(item.time));
        var song = $('<a/>', {href: '/track/' + item.id,text: item.song}).click(function() {
            e.preventDefault();
            load_url($(this).attr('href'), 'player_bar');
        });
        if (item.artist) {
            var artist = $('<a/>', {href: '/artist/' + escape(item.artist) + '/1/',text: item.artist}).click(function(e) {
                e.preventDefault();
                load_url($(this).attr('href'), 'player_bar');
            });
            now_playing = artist.add('<span> - </span>').add(song);
        } else {
            now_playing = song;
        }
        $('#playerFav').remove();
        var fav_link = $('<a href="#" id="playerFav" class="fav_item_' + item.id + '"></a>');
        $('#playerPlay').after(fav_link);
        fav_link.click(function(e) {
            e.preventDefault();
            toggle_favorite('item', item.id);
        });
        if (item.fav == 1) {
            fav_link.addClass('fav-on');
        } else {
            fav_link.addClass('fav-off');
        }
        var blog_link = $('<a/>', {"class": 'store read',href: item.posturl,html: 'Read Post &raquo;'}).bind('click', function(e) {
            e.preventDefault();
            load_url($(this).attr('href'), 'player_bar');
        });
        player_elements.nowplaying.empty().append(now_playing.add(blog_link));
        player_elements.links.hide();
        page_playing_txt = '<a href="' + activeList.replace('/#!', '') + '" onclick="load_url(this.href, \'player_bar_prev_elt\');return false;"><img src="/images/songplaying.gif" alt="song playing" /></a>';
        player_elements.page.empty().append($(page_playing_txt));
    }
    window.toggle_favorite = function(type, id, gray, skip_prompt) {
        log("toggle_favorite(" + type + ", " + id + ", " + gray + ", " + skip_prompt + ") called");
        if (is_logged_in == 0 && !get_cookie("dontprompt") && !skip_prompt) {
            show_lightbox('listen', '/inc/lb_signup_info.php?type=' + type + '&val=' + id);
            return false;
        }
        if (type == "query") {
            id_enc = Base64.encode(id);
            id_enc = id_enc.replace(/=+/, '');
        }
        var via_data = '';
        if (typeof window.displayList !== 'undefined') {
            if (window.displayList['page_name'] == "profile" && (window.displayList['page_mode'] == "loved" || window.displayList['page_mode'] == "history") && logged_in_username != window.displayList['profile_user']) {
                via_data = "&via_user=" + window.displayList['profile_user'];
            } else if (window.displayList['page_name'] == "profile" && window.displayList['page_mode'] == "feed" && logged_in_username != window.displayList['profile_user']) {
                via_data = "&via_user=" + window.displayList["item_" + id]['via_user'];
            }
        }
        var request = $.ajax({url: '/inc/user_action.php',data: 'act=toggle_favorite&ts=' + get_unix_time() + '&ts_offset=' + (get_unix_time() - initial_ts) + '&initial_ref=' + initial_ref + '&session=' + get_visitorid() + '&type=' + type + '&val=' + encodeURIComponent(id) + via_data + '&current_page_name=' + window.displayList['page_name'] + '&current_page_mode=' + window.displayList['page_mode'] + '&current_profile_user=' + window.displayList['profile_user'] + '&current_url=' + Base64.encode(location.href),type: 'post',async: true,cache: false,success: function(response) {
                var response = response || "no response text";
            },error: function() {
                _gaq.push(['_trackEvent', 'Error', 'add_favorite']);
                alert('We couldn\'t add your favorite! :( One of our systems is unavailable and will return shortly.');
            }});
        activeclass = (type == "item") ? "fav-on" : "unfollow";
        inactiveclass = (type == "item") ? "fav-off" : "follow";
        activecolor = '';
        inactivecolor = '#ccc';
        activeclass_favcount = "favcount-on";
        active_favcount_change = 1;
        inactiveclass_favcount = "favcount-off";
        inactive_favcount_change = -1;
        active_text = '<em></em> <span>Unfollow</span>';
        inactive_text = '<em></em> <span>Follow</span>';
        if (type == "query") {
            $(".fav_" + type + "_" + id_enc).each(function(index, elt) {
                if ($(elt).hasClass(activeclass)) {
                    $(elt).removeClass(activeclass).addClass(inactiveclass);
                    $(elt).html(inactive_text);
                } else {
                    $(elt).removeClass(inactiveclass).addClass(activeclass);
                    $(elt).html(active_text);
                    _gaq.push(['_trackEvent', 'Search', 'Favorite', window.displayList['page_name']]);
                }
            });
        } else {
            var hasUpdated = false;
            $('.fav_' + type + "_" + id).each(function(index, elt) {
                if ($(elt).hasClass(activeclass)) {
                    $(elt).removeClass(activeclass).addClass(inactiveclass);
                    if (type != 'item') {
                        $(elt).html(inactive_text);
                    }
                } else {
                    $(elt).removeClass(inactiveclass).addClass(activeclass);
                    if (type != 'item') {
                        $(elt).html(active_text);
                    }
                    if (!hasUpdated && typeof FB !== "undefined") {
                        FB.api('/me/hype-machine:favorite', 'post', {track: 'http://hypem.com/track/' + id + '/'}, function(response) {
                            if (!response || response.error) {
                                log('there was a problem updating with facebook :(')
                                log(response.error);
                                _gaq.push(['_trackEvent', 'Error', 'Facebook', window.displayList['page_name']]);
                            } else {
                                log('track favorite posted to facebook!')
                                _gaq.push(['_trackEvent', 'Facebook', 'Favorite', displayList['page_name']]);
                            }
                        });
                        if (type == "item") {
                            _gaq.push(['_trackEvent', 'Track', 'Favorite', window.displayList['page_name']]);
                        } else if (type == "site") {
                            _gaq.push(['_trackEvent', 'Blog', 'Favorite', window.displayList['page_name']]);
                        } else if (type == "user") {
                            _gaq.push(['_trackEvent', 'Friend', 'Favorite', window.displayList['page_name']]);
                        }
                        hasUpdated = true;
                    }
                }
            });
            $('.favcount_' + id).each(function(index, elt) {
                if ($(elt).hasClass(activeclass_favcount)) {
                    $(elt).removeClass(activeclass_favcount).addClass(inactiveclass_favcount);
                    if (type == 'item' && is_int($(elt).html())) {
                        $(elt).html((parseInt($.trim($(elt).html())) + inactive_favcount_change));
                    }
                } else {
                    $(elt).removeClass(inactiveclass_favcount).addClass(activeclass_favcount);
                    if (type == 'item' && is_int($(elt).html())) {
                        $(elt).html((parseInt($.trim($(elt).html())) + active_favcount_change));
                    }
                }
            });
            if (type == "item" && (currentUrl.match(/popular/) || currentUrl.match(/search/) || currentUrl.match(/artist/) || currentUrl.match(/spy/) || currentUrl.match(/list/))) {
                toggle_item_activity('favorites', $(".section-track[data-itemid='" + id + "']"), 0, 20);
            }
        }
    }
    window.show_all_tracks = function(elt) {
        var elementList = elt.parent().children();
        elt.parent().children().each(function(index, elti) {
            if ($(elti).attr('class') !== '' && $(elti).attr('class').match(/same-post/)) {
                $(elti).hide();
            }
        });
    }
    window.show_buy = function(pos) {
        $('#buy' + pos).slideDown(250);
        if ($('#meta' + pos))
            $('#meta' + pos).hide();
    }
    window.expand_hyped = function(list_parent) {
        var elementList = $('#list_parent').find('li');
        var tmp = '';
        var i = 0;
        $.each(elementList, function(index, elt) {
            if ($(elt).hasClass('hyped-6') && $(elt).css('display', 'none')) {
                $(elt).hide();
            } else if ($(elt).hasClass('hyped-6') && $(elt).css('display', '')) {
                $(elt).show();
            }
        });
        if ($('#mbx').length) {
            if ($('#mbx').html().indexOf("EXPAND") !== -1) {
                $('#mbx').html("HIDE &uarr;");
            } else {
                $('#mbx').html("EXPAND &darr;");
            }
        }
    }
    window.enable_spy_check = function() {
        log("enable_spy_check() called");
        if (spyTimeout != 0) {
            log("Disabling autoupdater in enable_spy_check");
            clearInterval(spyTimeout);
            spyTimeout = 0;
        }
        if (typeof window.displayList['tracks'] !== 'undefined') {
            if (typeof window.displayList['tracks'][0].ts !== 'undefined') {
                log("Enabling autoupdater for: " + document.location.href);
                spyTimeout = window.setInterval(check_spy, 2000);
            }
            $('#spy-status').html('LIVE');
            $('#spy-status').removeClass().addClass('spy-live');
        }
        return true;
    }
    window.check_spy = function() {
        var insert_offset;
        if (playerStatus == 'PAUSED' || playerStatus == 'PLAYING') {
            insert_offset = 1;
        } else {
            insert_offset = 0;
        }
        var request = $.ajax({url: '/inc/serve_spy.php',data: 'page=' + escape(document.location.href) + '&latest=' + window.displayList['tracks'][insert_offset].ts + '&ts=' + get_unix_time(),type: 'get',async: true,success: function(response) {
                if (!response) {
                    return false;
                }
                $(response).insertBefore($('.section-track').eq(insert_offset)).slideDown(500);
                if ($('div.section-track').eq(insert_offset + 1).attr('class').match(/even/)) {
                    $('.section-track').eq(insert_offset).removeClass('even').addClass('odd');
                }
                window.displayList.tracks.splice(insert_offset, 0, JSON.parse($('#displayList-data').remove().html()))
                if (activeList == get_current_rel_url() && typeof playList !== 'undefined' && !playList.gc) {
                    log('cloning playList');
                    window.playList = jQuery.extend(true, {}, window.displayList);
                }
                if (window.displayList['tracks'].length > 25) {
                    $('div.section-track:last').remove();
                    window.playList['tracks'].pop();
                    if (activeList == get_current_rel_url()) {
                        window.displayList['tracks'].pop();
                    }
                }
            }});
    }
    window.disable_spy_check = function() {
        if (spyTimeout !== 0) {
            log("Disabling autoupdater in disable_spy_check");
            clearInterval(spyTimeout);
            spyTimeout = 0;
            $('#spy-status').html('PAUSED');
            $('#spy-status').removeClass().addClass('spy-paused');
            return true;
        } else {
            return false;
        }
    }
    window.enable_notification_check = function() {
        log("enable_notification_check() called");
        if (notificationTimeout != 0) {
            log("Disabling autoupdater in enable_notification_check");
            clearInterval(notificationTimeout);
            notificationTimeout = 0;
        }
        if (window.displayList !== 'undefined') {
            setTimeout(function() {
                if (typeof window.displayList['tracks'][0].ts !== 'undefined') {
                    log("Enabling autoupdater for: " + document.location.href);
                    notificationTimeout = window.setInterval(check_notification, 180000);
                }
            }, 180000);
        } else {
            return false;
        }
    }
    window.check_notification = function() {
        if (is_spy_page()) {
            var request = $.ajax({url: '/inc/serve_track_notification.php',data: 'page=' + escape(document.location.href) + '&latest=' + window.displayList['tracks'][0].ts + '&ts=' + get_unix_time(),type: 'get',async: true,success: function(response) {
                    $(response).insertBefore($('.section-track')[0]);
                    if ($('div.section-track').eq(1).attr('class').match(/even/)) {
                        $('.section-track').eq(0).removeClass('even').addClass('odd');
                    }
                }});
        }
    }
    window.disable_notification_check = function() {
        if (notificationTimeout !== 0) {
            log("Disabling autoupdater in disable_notification_check");
            clearInterval(notificationTimeout);
            notificationTimeout = 0;
            if (is_spy_page()) {
                $('#spy-status').html('PAUSED');
                $('#spy-status').removeClass().addClass('spy-paused');
            }
            return true;
        } else {
            return false;
        }
    }
    window.check_section_updated = function(page_name, latest) {
        log('check_section_updated(' + page_name + ',' + latest + ') called');
        var request = $.ajax({url: '/inc/serve_track_notification.php',data: 'page_name=' + escape(page_name) + '&latest=' + latest + '&ts=' + get_unix_time(),type: 'get',dataType: 'json',async: true,success: function(response) {
                if (response.count > 0) {
                    if (response.page_name == "feed") {
                        jQuery('#submenu li.my-feed').append("<span class='count'>" + response.count + "</span>");
                    }
                }
            }});
    }
    window.enable_playback_check = function() {
        if (playback_event_timeout !== 0) {
            log("playback_check already running!");
            return false;
        } else {
            playback_event_timeout = window.setInterval(playback_check, 5000);
            return true;
        }
    }
    function playback_check() {
        if (playerStatus == "PLAYING") {
            log("playback_check: " + player_position + "/" + player_duration);
            var via_data = '';
            if (window.playList['tracks']['page_name'] == "profile" && (window.playList['tracks']['page_mode'] == "loved" || window.playList['tracks']['page_mode'] == "history") && logged_in_username != window.playList['tracks']['profile_user']) {
                via_data = "&via_user=" + window.playList['tracks']['profile_user'];
            }
            if (playback_event_count == 0 && player_position >= 30 && player_position < 45) {
                _gaq.push(['_trackEvent', 'Track', 'Play_30', window.playList['page_name']]);
                var request = $.ajax({url: '/inc/user_action.php',data: 'act=log_action&type=listen&ts=' + get_unix_time() + '&ts_offset=' + (get_unix_time() - initial_ts) + '&initial_ref=' + initial_ref + '&session=' + get_visitorid() + '&val=' + window.playList['tracks'][currentTrack].id + '&pos=' + player_position + '&rem=' + (player_duration - player_position) + '&vendor=' + playerDisplayed + '&playback_manual=' + playback_manual + via_data + '&current_page_name=' + window.playList['tracks']['page_name'] + '&current_page_mode=' + window.playList['tracks']['page_mode'] + '&current_url=' + Base64.encode(activeList),type: 'get',success: function(response) {
                        if (response == 'NOPLAYS') {
                            playback_allowed = 0;
                        } else if (response == 'BUYPROMPT') {
                            window.playList['tracks'][currentTrack].buy_prompt = 1;
                        }
                    }});
                playback_event_count++;
            } else if (playback_event_count == 1 && (player_position / player_duration) > (2 / 3)) {
                _gaq.push(['_trackEvent', 'Track', 'Play_Full', window.playList['page_name']]);
                var request = $.ajax({url: '/inc/user_action.php',data: 'act=log_action&type=listen&ts=' + get_unix_time() + '&ts_offset=' + (get_unix_time() - initial_ts) + '&initial_ref=' + initial_ref + '&session=' + get_visitorid() + '&val=' + window.playList['tracks'][currentTrack].id + '&pos=' + player_position + '&rem=' + (player_duration - player_position) + '&vendor=' + playerDisplayed + '&playback_manual=' + playback_manual + via_data + '&current_page_name=' + window.playList['tracks']['page_name'] + '&current_page_mode=' + window.playList['tracks']['page_mode'] + '&current_url=' + Base64.encode(activeList),type: 'get'});
                playback_event_count++;
            }
        } else {
            log('playback_check() called, playerStatus=' + playerStatus);
        }
    }
    window.disable_playback_check = function() {
        if (playback_event_timeout != undefined) {
            clearInterval(playback_event_timeout);
            playback_event_timeout = 0;
            return true;
        } else {
            return false;
        }
    }
    window.toggle_item_activity = function(type, item, skip, limit) {
        log.call(this, 'toggle_item_activity', arguments);
        var itemid = item.attr('data-itemid');
        var elt_name;
        if (type == 'favorites' || type == "favorites-friends") {
            elt_name = 'favcountlist';
        } else if (type == 'reposts') {
            elt_name = 'reposts';
        }
        var act_info = $('#section-track-' + item.attr('data-itemid') + ' .act_info')
        if (act_info.css('display') !== "none" && $(act_info.children()[0]).hasClass(elt_name)) {
            act_info.slideUp(300);
        } else if (act_info.html() !== '' && $(act_info.children()[0]).hasClass(elt_name)) {
            act_info.slideDown(300);
        } else {
            load_item_activity(type, item, skip, limit);
        }
    }
    window.update_item_activity = function(type, itemid, skip, limit) {
        debug.call(this, 'update_item_activity', arguments);
        item = $(".section-track[data-itemid='" + itemid + "']");
        load_item_activity(type, item, skip, limit);
    }
    window.load_item_activity = function(type, item, skip, limit) {
        debug.call(this, 'load_item_activity', arguments);
        $('#player-loading').show();
        var extra_args;
        var target_elt;
        var itemid = item.attr('data-itemid');
        if (type == 'reposts') {
            extra_args = {"skip_post": window.displayList['tracks'][item.closest('.section-track').index('.section-track')].postid};
        }
        if (type == 'tweets') {
            target_elt = $('#section-track-' + itemid + ' .act_info_tweets');
        } else {
            target_elt = $('#section-track-' + itemid + ' .act_info');
        }
        var request = $.ajax({url: '/inc/serve_activity_info.php',data: 'type=' + type + '&id=' + itemid + '&skip=' + skip + '&limit=' + limit + '&extra_args=' + JSON.stringify(extra_args) + '&ts=' + get_unix_time(),type: 'get',async: true,success: function(response) {
                target_elt.html(response);
                target_elt.show();
                $('#player-loading').hide();
            }});
    }
    window.toggle_item_graph = function(id, force) {
        log('toggle_item_graph() called');
        var target_elt = $('#flot-' + id + '-container');
        if (!$('#flot-' + id).length && !force) {
            load_item_graph(id);
            log('a');
        } else {
            if (target_elt.css('display') == 'none') {
                target_elt.slideDown(200);
            } else {
                target_elt.slideUp(200);
            }
        }
    }
    window.load_item_graph = function(id) {
        $('#player-loading').show();
        var target_elt = $('#flot-' + id + '-container');
        var request = $.ajax({url: '/inc/serve_track_graph.php',data: '&id=' + id,type: 'get',async: true,success: function(response) {
                target_elt.html(response);
                target_elt.show();
                $('#player-loading').hide();
            }});
    }
    window.show_sidebar_info = function(uid, method, section) {
        var element = $('#' + section);
        if (element.css('display') != "none") {
            element.slideUp(300, function() {
                element.hide();
            });
            $('#show_' + method).html('Show all &darr;');
        } else {
            var request = $.ajax({url: '../inc/serve_sidebar_profile_items.php',data: 'uid=' + uid + '&method=' + method,type: 'get',async: true,success: function(response) {
                    element.html(response);
                    $('#loading_' + method).hide();
                    $('#show_' + method).html('Collapse &uarr;');
                    element.slideDown(200);
                }});
            $('#loading_' + method).show();
        }
    }
    window.set_nav_item_active = function(eltid) {
        var eltm = $('#' + eltid);
        var menu = $('#menu-item-username');
        $.each($("li.active"), function(index, elt) {
            $(elt).removeClass('active');
        });
        if (menu.length) {
            menu.removeClass('active');
        }
        if (eltm.length) {
            eltm.addClass('active');
        }
    }
    window.setup_player_bar = function() {
        if (typeof (player_elements) == "undefined") {
            init_selectors();
        }
        var playableCount = 0;
        if (typeof (window.displayList['tracks']) != 'undefined') {
            for (var i = 0; typeof (window.displayList['tracks'][i]) != 'undefined'; i++) {
                if (window.displayList['tracks'][i].type != '') {
                    playableCount++;
                }
            }
        }
        var window_el = $(window);
        if (player_elements.container.hasClass('mobile')) {
            window_el.bind('scroll', function() {
                player_elements.container.css('top', window.pageYOffset + window.innerHeight - 34);
            });
        }
        log("setup_player_bar() detected " + playableCount + " tracks on this page: " + get_current_rel_url());
        var end = /\/[0-9]+$/g;
        var same_page = activeList.replace(end, '') == get_current_rel_url().replace(end, '');
        if (playerStatus != "PLAYING" && playerStatus != "PAUSED") {
            if (typeof window.displayList === 'undefined' || typeof window.displayList['tracks'] === 'undefined' || playableCount == 0) {
                hide_player_bar();
                return false;
            } else {
                show_player_bar();
            }
            try {
                delete window.playList;
            } catch (e) {
                window.playList = undefined;
            }
            currentTrack = 0;
            set_now_playing_info(window.displayList['tracks'][0]);
            player_elements.volume_ctrl.width(player_volume + "%");
            document.title = $("<div>" + window.displayList.title + "</div>").text();
            player_elements.page.css('visibility', 'hidden');
            if (window.displayList['tracks'].length == 1) {
                player_elements.prev.removeClass("playerPrev-active").addClass("playerPrev-inactive");
                player_elements.next.removeClass("playerNext-active").addClass("playerNext-inactive");
            } else if (window.displayList['tracks'].length > 1) {
                player_elements.prev.removeClass("playerPrev-inactive").addClass("playerPrev-active");
                player_elements.next.removeClass("playerNext-inactive").addClass("playerNext-active");
            }
        } else if (playerStatus == "PLAYING") {
            if (same_page) {
                if (window.playList['tracks'][currentTrack].id == window.displayList['tracks'][currentTrack].id && !is_shuffle_page()) {
                    $('.section-track').eq(currentTrack).find('.play-ctrl').addClass('play-state');
                    set_track_bg(currentTrack, "green");
                } else if (!is_spy_page()) {
                    for (var i = 0; typeof (window.displayList['tracks'][i]) != 'undefined'; i++) {
                        if (window.displayList['tracks'][i].id == window.playList['tracks'][currentTrack].id) {
                            $('.section-track').eq(currentTrack).find('.play-ctrl').addClass('play-state');
                            set_track_bg(i, "green");
                        }
                    }
                }
                player_elements.page.css('visibility', "hidden");
            } else {
                player_elements.page.css('visibility', 'visible');
            }
            setPlaying();
        }
        player_elements.player_inner.show();
    }
    window.hide_player_bar = function() {
        log('hide_player_bar() called');
        if (typeof (player_elements) == "undefined") {
            init_selectors();
        }
        player_elements.player_controls.addClass("disabled");
    }
    window.show_player_bar = function() {
        log('show_player_bar() called');
        if (typeof (player_elements) == "undefined") {
            init_selectors();
        }
        player_elements.player_controls.removeClass("disabled");
    }
    window.blog_search = function(auto) {
        clearInterval(window.autosearch_blogs);
        if ((auto && $('#blog-directory-search').val().length >= 3) || !auto) {
            var request = $.ajax({url: '/inc/serve_sites.php',data: 'q=' + $('#blog-directory-search').val(),type: 'get',async: true,success: function(response) {
                    $('#directory-blogs').html(response);
                }});
        }
    }
    window.infinite_blog_scroll = function(pages, expand_el, GET) {
        window.blog_scroll = {infinite: false,pages: pages,page: 1,loading: false,GET: GET,complete: false};
        var w_el = $(window);
        var dir_el = $('#directory');
        expand_el.bind('click', function(e) {
            window.blog_scroll.infinite = true;
            loadNextBlogPage(true);
            $(this).remove();
        });
        w_el.bind('scroll', function(e) {
            var offset = $('#directory-blogs-inject').offset();
            if (offset) {
                var point = offset.top - (320 * 2);
                if (w_el.scrollTop() >= point) {
                    loadNextBlogPage();
                }
            }
            fadeInImages();
        });
        function fadeInImages(force) {
            if (typeof force == 'undefined')
                force = false;
            $('.fadein').each(function(i, el) {
                var el = $(el);
                var offset = el.offset();
                if (force || w_el.scrollTop() + w_el.height() >= offset.top) {
                    el.removeClass('fadein');
                    el.hide().fadeIn(500);
                }
            });
        }
        function loadNextBlogPage(force) {
            if (typeof force == 'undefined')
                force = false;
            if (!window.blog_scroll.loading && window.blog_scroll.infinite && !window.blog_scroll.complete) {
                window.blog_scroll.loading = true;
                window.blog_scroll.GET.page++;
                $('#player-loading').show();
                $.ajax({url: '/inc/serve_sites.php',data: window.blog_scroll.GET,aync: true,type: 'get',dataType: 'html',success: function(response) {
                        if (response == '') {
                            window.blog_scroll.complete = true;
                        }
                        $('#directory-blogs-inject').before(response);
                        $('#player-loading').hide();
                        window.blog_scroll.loading = false;
                        $('.fadein').each(function(i, el) {
                            $(el).hide();
                        });
                        fadeInImages(force);
                    }});
            }
        }
    }
    window.insert_next_page = function(callback) {
        log("insert_next_page() called");
        var current_page = window.displayList.page_num;
        url = window.displayList.page_next;
        url = url.replace(/\?ax=1&frag=1&ts=\d+/, '');
        url = url.replace(/\&/, '?');
        url = url.replace(/^#!?/, '');
        if (!skip_update_page_contents && (window.displayList.loaded_page_next != url || url.match(/shuffle/i))) {
            window.displayList.loaded_page_next = url;
            log("insert_next_page() going to process " + url);
            $('#player-loading').show();
            if (is_html5_history_compat()) {
                skip_update_page_contents = 1;
                history.replaceState(null, null, document.location.protocol + '//' + document.location.host + url);
                activeList = get_current_rel_url();
            }
            function post_update_page_contents() {
                $('#player-loading').hide();
                n_displayList = JSON.parse($('#displayList-data').remove().html());
                n_displayList['tracks'] = window.displayList['tracks'].concat(n_displayList['tracks']);
                window.displayList = n_displayList;
                if (activeList == get_current_rel_url() && typeof playList !== 'undefined' && !playList.gc) {
                    log('cloning playList');
                    window.playList = jQuery.extend(true, {}, window.displayList);
                }
                if (window.displayList['show_favorites_friends']) {
                    $('.section-track').each(function(i, el) {
                        load_item_activity('favorites-friends', $(el), 0, 20);
                    });
                }
                if (document.location.href.match(/popular\/?\d?/)) {
                    setTimeout(function() {
                        render_popular_sparklines();
                    }, 1000);
                }
                set_ad_vars();
                draw_ads();
                ga_pageview();
            }
            page_updater = $.ajax({url: url,data: 'ax=1&frag=1&ts=' + get_unix_time(),dataType: 'html',type: 'get',async: true,success: function(content) {
                    $('#content-wrapper').append(content);
                    post_update_page_contents();
                    if (typeof callback == 'function') {
                        callback(content);
                    } else {
                        log('insert_next_page callback not a function');
                    }
                    if (window.displayList.page_next == url && !infinite_page_scroll_status.hit_end && !(window.displayList.page_name == 'profile' && window.displayList.page_mode == 'shuffle')) {
                        log("Detected end of infinite scroll");
                        insert_end_content();
                        disable_infinite_page_scroll();
                        infinite_page_scroll_status.hit_end = true;
                    }
                    r = setTimeout(function() {
                        skip_update_page_contents = 0;
                    }, 500);
                },error: function() {
                    $('#player-loading').hide();
                    setup_player_bar();
                    _gaq.push(['_trackEvent', 'Error', 'page_updater']);
                }});
        } else {
            log(" * ignored due to timing or end of pages");
        }
    }
    window.insert_end_content = function() {
        log('insert_end_content() called');
        page_updater = $.ajax({url: '/inc/serve_end_of_infinity.php?direct=1',dataType: 'html',type: 'get',async: true,success: function(content) {
                $('#content-wrapper').append(content);
            }});
    }
    window.enable_infinite_page_scroll = function() {
        log("enable_infinite_page_scroll() called");
        var w_el = $(window);
        more_el = $('#infinite-tracks-button');
        if (more_el.length) {
            infinite_page_scroll_status.view_more_exists = true;
            more_el.bind('click', function() {
                more_el.parent().slideUp(250);
                infinite_page_scroll_status.view_more_clicked = true;
                insert_next_page(function() {
                    more_el.hide();
                });
                w_el.bind('scroll', infinite_page_scroll);
                return false;
            });
        } else {
            w_el.bind('scroll', infinite_page_scroll);
            log("Automatic scroll event bound...");
        }
    }
    window.disable_infinite_page_scroll = function() {
        log("disable_infinite_page_scroll() called");
        var w_el = $(window);
        w_el.unbind('scroll', infinite_page_scroll);
        infinite_page_scroll_status.view_more_clicked = false;
        infinite_page_scroll_status.view_more_exists = false;
        infinite_page_scroll_status.hit_end = false;
    }
    window.infinite_page_scroll_status = {jump_ahead: 200,view_more_exists: false,view_more_clicked: false,hit_end: false};
    window.infinite_page_scroll = function(e) {
        var cw = $('#content-wrapper');
        var offset = cw.offset();
        if (offset) {
            var point = offset.top + cw.height();
            if ($(window).scrollTop() + window.innerHeight + infinite_page_scroll_status.jump_ahead >= point) {
                insert_next_page();
            }
        }
    }
    window.blog_search_keyup = function(e) {
        clearInterval(window.autosearch_blogs);
        window.autosearch_blogs = setTimeout(function() {
            blog_search(true);
        }, 500);
    }
    window.next_review = function(pos) {
        if (album_r_curr[pos] + 1 > (album_rs[pos].length - 1)) {
            album_r_curr[pos] = 0;
            show_review(pos);
        } else if (album_r_curr[pos] === 0 || album_r_curr[pos] === undefined) {
            album_r_curr[pos] = 1;
            show_review(pos);
        } else {
            album_r_curr[pos]++;
            show_review(pos);
        }
    }
    window.prev_review = function(pos) {
        if (album_r_curr[pos] - 1 < 0 || album_r_curr[pos] === undefined) {
            album_r_curr[pos] = album_rs[pos].length - 1;
            show_review(pos);
        } else {
            album_r_curr[pos]--;
            show_review(pos);
        }
    }
    window.show_review = function(pos) {
        $('#album-review' + pos).hide()
        $('#album-review-text' + pos).html(album_rs[pos][album_r_curr[pos]].rbody);
        $('#album-review' + pos).fadeIn(500);
    }
    window.load_rdio = function(pos, c_id) {
        new_iframe = $('<iframe/>', {id: "rdio_" + pos,width: "360",height: "360",scrolling: "no",frameborder: "no",src: 'http://rd.io/i/' + c_id + '?maxwidth=360&maxheight=360&autoplay=1&linkshare_id=DkrYZ0xe5n0&linkshare_subid=&linkshare_offerid=221756.1&linkshare_type=10&linkshare_tmpid=8599'});
        $('#album-player' + pos).append(new_iframe);
        $('#album-art' + pos).hide();
        $('#album-player-link' + pos).hide();
    }
    window.updateUrl = function(value) {
        if ($('#username_url').length) {
            $('#username_url').html(value);
        }
    }
    window.watchInput = function(el, options) {
        var duration = options.duration || 1000;
        var callback = (typeof options.callback == 'function') ? options.callback : function() {
            log('No callback defined for ' + el.attr('id') + '_interval');
        };
        var bind = (typeof options.onblur != 'undefined') ? 'blur' : 'keyup';
        if (bind == 'blur')
            duration = 0;
        el.bind(bind, function() {
            clearInterval(window[el.attr('id') + '_interval']);
            window[el.attr('id') + '_interval'] = setTimeout(function() {
                if (typeof options.url != 'undefined') {
                    $.ajax({url: '/inc/user_action.php',async: true,type: 'post',data: options.url + encodeURIComponent(el.val()),dataType: 'json',success: function(response) {
                            callback(response);
                        }});
                } else {
                    callback();
                }
            }, duration);
        });
    }
    window.process_lightbox_input = function(form_name) {
        log("process_lightbox_input(" + form_name + ") called");
        var form_data = $("#defaultform").serialize();
        var extra_data = "";
        if (form_name == "create_account" && $('#form_type').val() == "normal") {
            var recaptcha_challenge;
            var recaptcha_response;
            if (typeof Recaptcha != 'undefined') {
                recaptcha_challenge = Recaptcha.get_challenge();
                recaptcha_response = Recaptcha.get_response();
            }
            extra_data = '&recaptcha_challenge=' + recaptcha_challenge + '&recaptcha_response=' + recaptcha_response;
        }
        function on_response_failure(response, form_name) {
            log("process_lightbox_input->on_response_failure(%o, %o, %o) called", arguments[0], arguments[1], arguments[2]);
            if (!response || !response.error_msg) {
                $("#formmsg").html("Sorry! :(<br/>An unexpected error occurred: '" + response + "'<br/>Please <a href='/contact'>write to us</a> if this persists.");
                _gaq.push(['_trackEvent', 'Error', 'process_lightbox_input', 'unexpected']);
            } else {
                $('#formmsg').html(response.error_msg);
                _gaq.push(['_trackEvent', 'Error', 'process_lightbox_input', response.error_msg]);
            }
            $("#formmsg").removeClass('bad good').addClass('bad').show();
            $("#submitlogin").attr('disabled', false);
            if (form_name == "login") {
                $("#submitlogin").val("Sign in");
            } else if (form_name == "forgot") {
                $("#submitlogin").val("Reset Password");
            } else if (form_name == "create_account") {
                $("#submitlogin").val("Create Account");
            } else if (form_name == "account" || form_name == "sharing" || form_name == "change_password" || form_name == "change_username" || form_name == "change_email") {
                $("#submitlogin").val("Save");
            }
        }
        function on_response_success(response, form_name) {
            log("process_lightbox_input->on_response_success(%o, %o, %o) called", arguments[0], arguments[1], arguments[2]);
            if (response.status === "ok") {
                if (form_name == "login" || form_name == "create_account") {
                    if ($('#form_type').val() == "mobile") {
                        document.location.href = '/special/mobile_signup_success';
                    }
                    if (typeof ($('#favorite_type').val()) != "undefined" && typeof ($('#favorite_value').val()) != "undefined") {
                        if (is_ssl_parent()) {
                            post_login($('#favorite_type').val(), $('#favorite_value').val());
                        } 
                        else {
                            document.location.href = 'http://' + document.location.host + '/inc/lb_login.php?post_login=1&type=' + $('#favorite_type').val() + '&val=' + $('#favorite_value').val();
                        }
                    } else {
                        if (is_ssl_parent()) {
                            post_login();
                        } 
                        else {
                            document.location.href = 'http://' + document.location.host + '/inc/lb_login.php?post_login=1';
                        }
                    }
                } else if (form_name == "forgot" || form_name == "account" || form_name == "sharing" || form_name == "change_password" || form_name == "change_username" || form_name == "change_email") {
                    if (form_name == "account") {
                        $('#user_avatar').hide();
                    }
                    $("#defaultform").hide();
                    $("#formmsg").removeClass('bad good').addClass('good').html(response.msg).show();
                }
            } else if (response.status === "error") {
                on_response_failure(response, form_name);
                _gaq.push(['_trackEvent', 'Error', 'process_lightbox_input', 'success']);
            } else {
                on_response_failure(response, form_name);
            }
        }
        var request = $.ajax({url: '/inc/user_action.php',data: 'act=' + form_name + '&session=' + get_visitorid() + '&' + form_data + extra_data,type: 'post',dataType: 'json',success: function(data, response_txt, xhr_obj) {
                on_response_success(data, form_name);
            },error: function(xhr_obj, error_txt, http_error) {
                on_response_failure(error_txt, form_name);
                _gaq.push(['_trackEvent', 'Error', 'process_lightbox_input', 'user_action']);
            }});
        return false;
    }
    window.process_user_action = function(action_type) {
        log("process_user_action(" + action_type + ") called");
        function on_response_failure(response, action_type) {
            log("process_lightbox_input->on_response_failure(%o, %o, %o) called", arguments[0], arguments[1], arguments[2]);
            if (!response || !response.error_msg) {
                if (action_type == "user_logout") {
                    alert("We encountered an error logging you out.  It was: '" + response + "'.  Please try again later");
                    _gaq.push(['_trackEvent', 'Error', 'process_user_action', 'undefined']);
                } else {
                    $("#formmsg").html("Sorry! :(<br/>An unexpected error occurred: '" + response + "'<br/>Please <a href='/contact'>write to us</a> if this persists.");
                }
            } else {
                if (action_type == "user_logout") {
                    alert(response.error_msg);
                } else {
                    $('#formmsg').html(response.error_msg);
                }
                _gaq.push(['_trackEvent', 'Error', 'process_user_action', response.error_msg]);
            }
            $("#formmsg").removeClass('bad good').addClass('bad').show();
            $("#submitlogin").attr('disabled', false);
        }
        function on_response_success(response, action_type) {
            log("process_lightbox_input->on_response_success(%o, %o, %o) called", arguments[0], arguments[1], arguments[2]);
            if (response.status === "ok") {
                if (action_type == "user_logout") {
                    load_user_menu();
                    update_page_contents();
                } else {
                    $("#defaultform").hide();
                    $("#formmsg").removeClass('bad good').addClass('good').html(response.msg).show();
                }
            } else if (response.status === "error") {
                on_response_failure(response, action_type);
                _gaq.push(['_trackEvent', 'Error', 'on_response_success']);
            } else {
                on_response_failure(response, action_type);
            }
        }
        var request = $.ajax({url: '/inc/user_action.php',data: 'act=' + action_type + '&session=' + get_visitorid(),type: 'post',dataType: 'json',success: function(data, response_txt, xhr_obj) {
                on_response_success(data, action_type);
            },error: function(xhr_obj, error_txt, http_error) {
                _gaq.push(['_trackEvent', 'Error', 'user_action', error_txt]);
                on_response_failure(error_txt, action_type);
            }});
        return false;
    }
    window.post_login = function(type, id) {
        log("post_login(" + type + "," + id + ") called");
        load_user_menu();
        update_page_contents();
        Lightbox.hideBox();
        if (type && id && type != "listen") {
            setTimeout(function() {
                toggle_favorite(type, id);
            }, 1500);
        }
    }
    window.post_logout = function() {
        log("post_logout() called");
        load_user_menu();
    }
    window.post_username_change = function() {
        log("post_username_change() called");
        load_url('/popular');
        load_user_menu();
        Lightbox.hideBox();
    }
    window.cancel_iframe_dialog = function(redir_to) {
        log('cancel_iframe_dialog(' + redir_to + ') called');
        if (!/lb_/.test(document.location.href)) {
            if (typeof (redir_to) != 'undefined' && redir_to != '') {
                load_url(redir_to);
            } else if (document.location.href.match(/forgot=1/)) {
                load_url("/");
            } else 
            {
                update_page_contents();
            }
            load_user_menu();
            Lightbox.hideBox();
        } else {
            document.location.href = 'http://' + document.location.host + '/inc/lb_cancel.php';
        }
    }
    window.lightbox_close_handler = function(lightbox_url) {
        log('lightbox_close_handler() called');
        if (lightbox_url.match(/lb_account/)) {
            load_user_menu();
            if (window.displayList && window.displayList['page_name'] == 'profile') {
                update_page_contents();
            }
        }
        Lightbox.hideBox();
    }
    window.is_ssl_parent = function() {
        if (!/lb_/.test(document.location)) {
            return true;
        } 
        else {
            return false;
        }
    }
    window.switch_lightbox = function(type, url, reload) {
        log('lightbox_close_handler(' + type + ',' + url + ') called');
        if (is_ssl_parent()) {
            show_lightbox(type, url);
        } 
        else if (!is_ssl_parent() && reload) {
            document.location.href = 'http://' + document.location.host + '/inc/lb_cancel.php?switch_lightbox=1&type=' + type + '&url=' + url;
        } else {
            document.location.href = url + "?ts=" + get_unix_time();
        }
    }
    window.load_url_from_lightbox = function(url) {
        if (is_ssl_parent()) {
            load_url(url);
            cancel_iframe_dialog();
        } 
        else {
            document.location.href = 'http://' + document.location.host + '/inc/lb_cancel.php?redir_to=' + url;
        }
    }
    window.show_lightbox = function(type, url, arg1) {
        if (!$('#box').length) {
            Lightbox.init();
        }
        if (typeof (url) === "undefined") {
            url = false;
        }
        if (type == 'login') {
            if (url) {
                Lightbox.showBoxByAJAX(url, 370, 290);
            } else {
                Lightbox.showBoxByAJAX('/inc/lb_login.php', 370, 290);
            }
        } else if (type == 'signup') {
            if (url) {
                Lightbox.showBoxByAJAX(url, 340, 540);
            } else {
                Lightbox.showBoxByAJAX('/inc/lb_signup.php', 340, 540);
            }
        } else if (type == 'listen') {
            if (url) {
                Lightbox.showBoxByAJAX(url, 360, 210);
            } else {
                Lightbox.showBoxByAJAX('/inc/lb_signup_info.php?type=listen&val=' + window.playList['tracks'][currentTrack].id, 360, 210);
            }
        } else if (type == 'confirm_new_twitter') {
            Lightbox.showBoxByAJAX('/inc/lb_confirm_new.php?service=twitter', 360, 210);
        } else if (type == 'confirm_new_facebook') {
            Lightbox.showBoxByAJAX('/inc/lb_confirm_new.php?service=facebook', 360, 210);
        } else if (type == 'forgot') {
            Lightbox.showBoxByAJAX('/inc/lb_forgot.php', 370, 180);
        } else if (type == 'reset') {
            Lightbox.showBoxByAJAX(url, 350, 250);
        } else if (type == 'change_password') {
            Lightbox.showBoxByAJAX('/inc/lb_change_pw.php', 340, 520);
        } else if (type == 'change_username') {
            Lightbox.showBoxByAJAX('/inc/lb_change_username.php', 340, 520);
        } else if (type == 'change_email') {
            Lightbox.showBoxByAJAX('/inc/lb_change_email.php', 340, 520);
        } else if (type == 'account') {
            Lightbox.showBoxByAJAX('/inc/lb_account.php', 340, 620);
        } else if (type == 'sharing') {
            Lightbox.showBoxByAJAX('/inc/lb_sharing.php', 340, 520);
        } else if (type == 'postcard') {
            if (url) {
                Lightbox.showBoxByAJAX(url, 700, 450);
            }
        }
        $('#overlay').bind('click', function() {
            lightbox_close_handler(Lightbox.lightboxURL);
            return false;
        });
        return false;
    }
    window.checkPw = function() {
        if ($("#user_password_confirmation").val() !== $("#user_password").val() || $("#user_password").val() == '' || $("#user_password_confirmation").val() == '') {
            $("#nomatch").css('display', "inline");
            $("#submitlogin").attr('disabled', true);
            return 1;
        } else {
            $("#nomatch").css('display', "none");
            $("#submitlogin").attr('disabled', false);
            return false;
        }
    }
    window.display_twitter_score = function() {
        $('#twitter_score_calculate').val('Calculating...');
        $('#twitter_score_name').attr('disabled', true);
        $('#twitter_score_calculate').attr('disabled', true);
        var request = $.ajax({url: '/inc/user_action.php',data: 'act=check_twitter_score&session=' + get_visitorid() + '&arg=' + escape($('#twitter_score_name').val()),type: 'get',dataType: 'json',success: function(response) {
                resp_data = response;
                if (resp_data != '') {
                    $('#twitter_score_dyn').html('<b>' + Math.round(resp_data.tweet_points) + ' points</b> will be added to a song\'s score when <a href="http://twitter.com/' + resp_data.twitter_name + '">' + resp_data.twitter_name_long + '</a> links to it on Twitter.');
                    $('#twitter_score_dyn').show();
                    $('#twitter_score_calculate').val('Calculate!');
                    $('#twitter_score_calculate').attr('disabled', false);
                    $('#twitter_score_name').attr('disabled', false);
                } else {
                    $('#twitter_score_dyn').html('<b>N/A</b> points / tweet');
                    $('#twitter_score_dyn').show();
                    $('#twitter_score_calculate').val('Calculate!');
                    $('#twitter_score_calculate').attr('disabled', false);
                    $('#twitter_score_name').attr('disabled', false);
                }
            },error: function(transport) {
                _gaq.push(['_trackEvent', 'Error', 'twitter_score']);
                $('#twitter_score_dyn').html('<b>N/A</b> points / tweet');
                $('#twitter_score_dyn').show();
                $('#twitter_score_calculate').val('Calculate!');
                $('#twitter_score_calculate').attr('disabled', false);
                $('#twitter_score_name').attr('disabled', false);
            }});
    }
    window.UploadToS3 = function() {
        if ($('#picture_upload').val()) {
            $("#s3_upload_form").attr('target', "fileframe");
            $("#s3_upload_form").attr('action', "/inc/img_upload.php");
            $('#current_img_elt').hide();
            $("#img_frame").show();
            $("#s3_upload_form").submit();
        } else {
            return false;
        }
    }
    window.contact_show_tips = function() {
        if ($('#contact_subject').val() === "All blog questions") {
            var request = $.ajax({url: 'inc/serve_all_blogs_listbox.php',success: function(response) {
                    $('#blogs_list').html(response);
                }});
            $('#blog_name').show();
            $('#blogs_list').html('<option>Please wait...</option>');
            $('#contact_tips').html('<p><strong>Recommending a new blog?</strong><br />Use the <a href=\"/add\">add blog form</a>; otherwise, pick the blog you are contacting us about below.  If tracks you posted are not appearing correctly, please also provide the link to the missing/incorrect post.</p>');
            $('#contact_tips').show();
        } else if ($('#contact_subject').val() === "Ad feedback") {
            $('#contact_tips').html("<p><strong>Reporting an ad problem?</strong><br />Clicking on the ad and giving us the URL it sends you to, will help greatly!</p>");
            $('#contact_tips').show();
            $('#blog_name').hide();
        } else if ($('#contact_subject').val() === "Business stuff") {
            $('#contact_tips').html("<p>Please be brief about how you see us working together.</p>");
            $('#contact_tips').show();
            $('#blog_name').hide();
        } else if ($('#contact_subject').val() === "Report bugs") {
            $('#contact_tips').html('<p>Please include what were you doing when the error happened, what browser you use, etc.<br /><br />If you use AdBlock, Flashblock, NoScript, or any other ad-blocking tools or Internet security software, please try disabling them if you encounter problems using the site</p>');
            $('#contact_tips').show();
            $('#blog_name').hide();
        } else if ($('#contact_subject').val() === "Everything else") {
            $('#contact_tips').html("<p><a href='http://hypem.com/about'>Our FAQ</a> can answer most common questions, <a href='http://hypem.com/about'>please look there first</a> - you'll get your answer faster!</p>");
            $('#contact_tips').show();
            $('#blog_name').hide();
        } else {
            $('#blog_name').hide();
            $('#contact_tips').html('');
            $('#contact_tips').show();
        }
    }
    window.render_popular_sparklines = function() {
        log("render_popular_sparklines() called");
        var sparkDivs = $('.sparkdiv');
        Array.max = function(array) {
            return Math.max.apply(Math, array);
        };
        var maxes = new Array();
        sparkDivs.each(function(i) {
            var id = $(this).data('postId');
            if (day_points[id] != undefined) {
                maxes[i] = Array.max(day_points[id]);
            }
        });
        var max_page = Array.max(maxes);
        sparkDivs.each(function(i) {
            var id = $(this).data('postId');
            if (day_points[id] != undefined) {
                $("#sparkline-" + id).sparkline(day_points[id], {type: 'bar',barWidth: 3,barColor: '#63902F',barSpacing: 1,width: '36px',height: '20px',spotColor: false,minSpotColor: false,maxSpotColor: false,chartRangeMax: max_page});
            }
        });
    }
    window.positionMap = function(artist) {
        if (ua_info.is_mobile) {
            return false;
        }
        map.setZoom(4);
        loc = positions[artist.toLowerCase()];
        map.setCenter(new google.maps.LatLng((loc.lat() + 9), loc.lng()));
    }
    window.init_soundmanager = function() {
        if (document.location.href.match(/lb_/)) {
            return false;
        }
        if (typeof (soundManager) != 'undefined') {
            if (ua_info.is_ios || ua_info.is_android || ua_info.is_ie9 || ua_info.is_playbook) {
                soundManager.useHTML5Audio = true;
            } else if (!ua_info.is_android_old_flash) {
                soundManager.flashVersion = 9;
            }
            soundManager.debugMode = false;
            soundManager.url = '/js/soundmanagerv297a-20120624/swf/';
            soundManager.flashLoadTimeout = 5000;
            log("soundManager settings loaded OK");
            soundManager.onready(function() {
                if (soundManager.supported()) {
                    isReady = 1;
                    var group = $('#player-progress-loading, #player-progress-playing, #player-volume-outer');
                    group.bind('mousedown', sm_start_drag).bind('mouseup', sm_end_drag);
                    player_elements.volume_mute.live('click', sm_toggle_mute);
                } else {
                    _gaq.push(['_trackEvent', 'Error', 'soundmanager_unsupported']);
                    log("soundManager reported it's unsupported?");
                }
            });
        } else {
            log("Attempted to set some soundManager logic, but SM not loaded yet");
            setTimeout(function() {
                init_soundmanager();
            }, 1000);
        }
    }
    window.check_toast_permission = function(callback) {
        log('toast -- checking permissions');
        var prompt = $('#toast-prompt');
        if (window.webkitNotifications) {
            log('toast -- browser is supported');
            if (window.webkitNotifications.checkPermission()) {
                log('toast -- checking permission');
                if (!toast_prompt_shown) {
                    log('toast -- showing prompt');
                    prompt.fadeIn();
                }
                window.webkitNotifications.requestPermission(function() {
                    log('toast -- hiding prompt');
                    prompt.fadeOut();
                    callback();
                });
                toast_prompt_shown = true;
            } else {
                log('toast -- already have permission');
                callback();
            }
        }
    }
    window.show_toast = function(image, title, body) {
        try {
            log('toast -- trying to show toast');
            var toast = window.webkitNotifications.createNotification(image, title, body);
            toast.show();
            _gaq.push(['_trackEvent', 'Toast', 'Show']);
            toast.onclick = function(x) {
                log('toast -- clicked');
                _gaq.push(['_trackEvent', 'Toast', 'Clicked']);
                window.focus();
                this.cancel();
            };
            setTimeout(function() {
                toast.cancel();
            }, 7500);
        } catch (Err) {
            _gaq.push(['_trackEvent', 'Error', 'Toast', Err.message]);
            log('toast -- error!');
            log(Err.message);
        }
    }
    window.fire_share_modal = function() {
        var center = {x: ($(window).width() / 2) - (680 / 2),y: ($(window).height() / 2) - (420 / 2)};
        var new_window = window.open($(this).data('href'), $(this).attr('rel'), 'height=420,width=680,location=false,toolbar=false,menubar=false,screenX=' + center.x + ',screenY=' + center.y + ',left' + center.x + ',top' + center.y);
        return false;
    };
    window.handle_click = function(event) {
        log('handle_click(' + event + ') called');
        if (event.which == 2) {
            event.stopPropagation();
            return true;
        }
        if ($(this).attr('href')) {
            t_elt = event.target || event.srcElement;
            if (t_elt.tagName != 'A') {
                while (t_elt.tagName != 'A') {
                    t_elt = t_elt.parentNode;
                }
                url = t_elt.href;
            } else {
                url = t_elt.href;
            }
            if (url.match(/random$/)) {
                load_random_track();
            } else if (url.match(/random_search$/)) {
                load_random_search();
            } else {
                load_url(url, null, event);
            }
            return false;
        }
    }
    window.attach_clicks = function() {
        $('#content-wrapper').on('click', '.play-ctrl', function(e) {
            togglePlay($(this).closest('.section-track').index('.section-track'), e);
            return false;
        });
        $('#content-wrapper').on('click', '.toggle-reposts', function(e) {
            toggle_item_activity('reposts', $(this).closest('.section-track'), 0, 20);
            return false;
        });
        $('#content-wrapper').on('click', '.toggle-favorites', function(e) {
            toggle_item_activity('favorites', $(this).closest('.section-track'), 0, 20);
            return false;
        });
        $('#playerNext').on('click', nextTrack);
        $('#playerPrev').on('click', prevTrack);
        $('#content-wrapper').on('click', '.facebook-share', fire_share_modal);
        $('#content-wrapper').on('click', '.twitter-share', fire_share_modal);
        $('#header, #content-wrapper, #footer').on('click', 'a', handle_click);
    }
    $(document).ready(function() {
        log("document.ready has fired");
        $('#player-loading').show();
        activeList = get_current_rel_url();
        init_selectors();
        load_user_menu();
        init_soundmanager();
        attach_clicks();
        $(window).mousemove(function(e) {
            if (!$('#takeover-link').length)
                return false;
            var p = {x: e.pageX - $('#header-inner').offset().left,y: e.pageY - $('#header-inner').offset().top};
            if ((p.x < 0 || p.x > 1056) & p.y > 74) {
                p.x += $('#header-inner').offset().left;
                p.y += $('#header-inner').offset().top;
            } else {
                p = {x: 0,y: 0};
            }
            $('#takeover-link').css({'left': p.x - 5,'top': p.y - 5});
        });
    });
})(jQuery);

